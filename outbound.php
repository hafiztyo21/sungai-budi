<?php
session_start();
#session_destroy();
//print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('outbound.html');
$tablename = 'tbl_dax_deal_info';
$tablename1 = 'tbl_dax_customer';






if ($_POST['btn_update_outbound'])
{

$sql= "UPDATE tbl_dax_customer SET quality='".$_POST['txt_quality']."',tour_type='".$_POST['txt_tour_type']."'
		 where pk_id='".$_GET['id']."'";
  # $data->showsql($sql);
	if (!$data->inpQueryReturnBool($sql))
	{	throw new Exception($data->err_report('s02'));	}

$sql_2= "UPDATE tbl_dax_sales_assign SET memo='".$_POST['txt_memo']."',quality='".$_POST['txt_quality']."',tour_type='".$_POST['txt_tour_type']."'
		 where pk_id='".$_POST['txt_assign_id']."'";
  # 	$data->showsql($sql_2);
	if (!$data->inpQueryReturnBool($sql_2))
	{	throw new Exception($data->err_report('s02'));	}	
	
	if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('s01')."');window.parent.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('s02')."');</script>";	}


}



if ($_GET['edit']==1)
{   

$server_date = date("F j, Y H:i:s");
$server_time = date("H:i:s");
#$data->auth('09030101',$_SESSION['user_id']);

	if ($_SESSION['jobid']!='SADM'){
		$filter = " and  tbl_dax_customer.outlet_code='".$_SESSION['outletcode']."'";
	}

  $rows_outbound = $data->selectQuery("SELECT tbl_dax_customer.pk_id as pk_id, tbl_dax_customer.first_name, tbl_dax_customer.last_name, tbl_dax_customer.outlet_code,
										tbl_dax_customer.mobile,tbl_dax_customer.card_id,tbl_dax_customer.age,tbl_dax_customer.occupation,
										tbl_dax_customer.phone,tbl_dax_customer.address,tbl_dax_customer.income,assign.quality,
										assign.tour_type,
										tbl_dax_spouse_particulars.fk_customer,tbl_dax_spouse_particulars.first_name AS spouse_name,
										tbl_dax_status.name AS
										STATUS_CUSTOMER_TERAKHIR , tbl_dax_customer.status as status_no,
										tbl_dax_outlet.name as outlet,
										type_data.data_contact as data_source,
										CONCAT(tbl_dax_employee.first_name,' ', tbl_dax_employee.last_name) as ASSIGN_TO_SALES,
										assign.assign_id,assign.dateday, assign.session_name, assign.session_time, assign.venue, assign.memo, assign.fk_schedule_detail
										
										FROM tbl_dax_customer
										LEFT JOIN tbl_dax_status ON tbl_dax_customer.status = tbl_dax_status.pk_id
										LEFT JOIN tbl_dax_spouse_particulars ON tbl_dax_customer.pk_id = tbl_dax_spouse_particulars.fk_customer
										
										LEFT JOIN tbl_dax_outlet ON tbl_dax_customer.outlet_code = tbl_dax_outlet.code
										LEFT JOIN  (select tbl_dax_typedata.*,tbl_dax_datasource_from.name from tbl_dax_typedata,tbl_dax_datasource_from  where tbl_dax_typedata.fk_datasource=tbl_dax_datasource_from.pk_id)type_data  ON tbl_dax_customer.data_source_id = type_data.pk_id  
										LEFT JOIN tbl_dax_employee ON tbl_dax_customer.sales_id = tbl_dax_employee.pk_id
										LEFT JOIN 
											(   SELECT tbl_dax_sales_assign.pk_id as assign_id,tbl_dax_schedule.dateday,tbl_dax_schedule_detail.session_name,tbl_dax_schedule_detail.session_time,tbl_dax_sales_assign.venue,tbl_dax_sales_assign.pk_id,tbl_dax_sales_assign.memo,tbl_dax_sales_assign.fk_schedule_detail,tbl_dax_sales_assign.quality,tbl_dax_sales_assign.tour_type
												 FROM tbl_dax_schedule,tbl_dax_schedule_detail,tbl_dax_sales_assign
												WHERE tbl_dax_schedule.pk_id=tbl_dax_schedule_detail.schedule_id
												AND  tbl_dax_schedule_detail.pk_id=tbl_dax_sales_assign.fk_schedule_detail)as assign ON tbl_dax_customer.sales_assign_id=assign.pk_id
										
										WHERE tbl_dax_customer.status = 8 
										$filter and tbl_dax_customer.pk_id ='".$_GET['id']."'");
			$rows_assign = $data->selectQuery("SELECT * FROM tbl_dax_sales_assign where pk_id='".$rows_outbound[assign_id]."'");
			


         $dataRows = array (
				'TEXT' =>  array('Customer Name','Date Outbound','NRIC','Current Age','Occupation','Phone','Mobile','Address',
								'Spouse Name','Income Category','Counsellor','Quality','Tour Type','Status','Remarks','Sales Team',
								'Venue','HC'),
				'DOT'  => array (':',':',':',':',':',':',':',':'),
				'FIELD' => array ($rows_outbound[first_name],
				$rows_outbound[dateday], 
				$rows_outbound[card_id], 
				$rows_outbound[age], 
				$rows_outbound[occupation],
				$rows_outbound[phone],
				$rows_outbound[mobile],
				$rows_outbound[address],
				$rows_outbound[spouse_name],
				$rows_outbound[income],
				$rows_outbound[ASSIGN_TO_SALES],
				$data->cb_quality('txt_quality',$rows_assign[quality]),
				$data->cb_tour_type('txt_tour_type',$rows_assign[tour_type]),
				$rows_outbound[STATUS_CUSTOMER_TERAKHIR],
				"<textarea name='txt_memo' id='txt_memo'>".$rows_assign[memo]."</textarea>"."<input type='hidden' name='txt_assign_id' value='".$rows_outbound[assign_id]."' readonly>",
				$rows_outbound[sales_team],
				$rows_outbound[venue],
				$rows_outbound[ASSIGN_TO_SALES])
				#"<a href='?edit=1'>edit</a>"
				
				);

    $tittle = "OUTBOUND EDIT";

    $button = array ('SUBMIT' => "<input type=submit name=btn_update_outbound value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
	
	$editlink = array ('EDIT' => ""
					 
					);				
}



if ($_GET['outbound']==1)
{

	if ($_SESSION['jobid']!='SADM'){
		$filter = " and  tbl_dax_customer.outlet_code='".$_SESSION['outletcode']."'";
	}

  $rows_outbound = $data->selectQuery("SELECT tbl_dax_customer.pk_id as pk_id, tbl_dax_customer.first_name, tbl_dax_customer.last_name, tbl_dax_customer.outlet_code,
										tbl_dax_customer.mobile,tbl_dax_customer.card_id,tbl_dax_customer.age,tbl_dax_customer.occupation,
										tbl_dax_customer.phone,tbl_dax_customer.address,tbl_dax_customer.income,assign.quality,
										assign.tour_type,
										tbl_dax_spouse_particulars.fk_customer,tbl_dax_spouse_particulars.first_name AS spouse_name,
										tbl_dax_status.name AS
										STATUS_CUSTOMER_TERAKHIR , tbl_dax_customer.status as status_no,
										tbl_dax_outlet.name as outlet,
										type_data.data_contact as data_source,
										CONCAT(tbl_dax_employee.first_name,' ', tbl_dax_employee.last_name) as ASSIGN_TO_SALES,
										assign.assign_id,assign.dateday, assign.session_name, assign.session_time, assign.venue, assign.memo, assign.fk_schedule_detail
										
										FROM tbl_dax_customer
										LEFT JOIN tbl_dax_status ON tbl_dax_customer.status = tbl_dax_status.pk_id
										LEFT JOIN tbl_dax_spouse_particulars ON tbl_dax_customer.pk_id = tbl_dax_spouse_particulars.fk_customer
										
										LEFT JOIN tbl_dax_outlet ON tbl_dax_customer.outlet_code = tbl_dax_outlet.code
										LEFT JOIN  (select tbl_dax_typedata.*,tbl_dax_datasource_from.name from tbl_dax_typedata,tbl_dax_datasource_from  where tbl_dax_typedata.fk_datasource=tbl_dax_datasource_from.pk_id)type_data  ON tbl_dax_customer.data_source_id = type_data.pk_id  
										LEFT JOIN tbl_dax_employee ON tbl_dax_customer.sales_id = tbl_dax_employee.pk_id
										LEFT JOIN 
											(   SELECT tbl_dax_sales_assign.pk_id as assign_id,tbl_dax_schedule.dateday,tbl_dax_schedule_detail.session_name,tbl_dax_schedule_detail.session_time,tbl_dax_sales_assign.venue,tbl_dax_sales_assign.pk_id,tbl_dax_sales_assign.memo,tbl_dax_sales_assign.fk_schedule_detail,tbl_dax_sales_assign.quality,tbl_dax_sales_assign.tour_type
												 FROM tbl_dax_schedule,tbl_dax_schedule_detail,tbl_dax_sales_assign
												WHERE tbl_dax_schedule.pk_id=tbl_dax_schedule_detail.schedule_id
												AND  tbl_dax_schedule_detail.pk_id=tbl_dax_sales_assign.fk_schedule_detail)as assign ON tbl_dax_customer.sales_assign_id=assign.pk_id
										
										WHERE tbl_dax_customer.status = 8 
										$filter and tbl_dax_customer.pk_id ='".$_GET['id']."'");
			$rows_assign = $data->selectQuery("SELECT * FROM tbl_dax_sales_assign where pk_id='".$rows_outbound[assign_id]."'");


         $dataRows = array (
				'TEXT' =>  array('Customer Name','Date Outbound','NRIC','Current Age','Occupation','Phone','Mobile','Address',
								'Spouse Name','Income Category','Counsellor','Quality','Tour Type','Status','Remarks','Sales Team',
								'Venue','HC'),
				'DOT'  => array (':',':',':',':',':',':',':',':'),
				'FIELD' => array ($rows_outbound[first_name],
				$rows_outbound[dateday], 
				$rows_outbound[card_id], 
				$rows_outbound[age], 
				$rows_outbound[occupation],
				$rows_outbound[phone],
				$rows_outbound[mobile],
				$rows_outbound[address],
				$rows_outbound[spouse_name],
				$rows_outbound[income],
				$rows_outbound[ASSIGN_TO_SALES],
				$rows_assign[quality],
				$rows_assign[tour_type],
				$rows_outbound[STATUS_CUSTOMER_TERAKHIR],
				$rows_assign[memo]."<input type='hidden' name='txt_schedule_detail' value='".$rows_outbound[fk_schedule_detail]."' readonly>",
				$rows_outbound[sales_team],
				$rows_outbound[venue],
				$rows_outbound[ASSIGN_TO_SALES])
				#"<a href='?edit=1'>edit</a>"
				
				);
				
				
				
				
	#$data->showsql($sql);
			#print_r($dataRows);
			$tittle = "OUTBOUND DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
					
	$editlink = array ('EDIT' => "<img src='image/page_edit.png' width='12' height='12'>&nbsp;<a href='?edit=1&id=".$_GET['id']."' >edit</a>"
					 
					);
}
#$editlink = "<img src='image/page_edit.png' width='12' height='12'>&nbsp;<a href='?edit=1' >edit</a>";


$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addRows('loopData',$package );
$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);

$tmpl->addVars('edit',$editlink);
$tmpl->displayParsedTemplate('page');
?>