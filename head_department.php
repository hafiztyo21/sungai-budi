<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'employee.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new employee;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('head_department.html');


if($_GET['del']==1){

	$sql = "delete  from tbl_dax_department_head where pk_id='".$_GET['id']."'";
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');window.parent.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}
}

#$addLink = "<a href='user.php' onclick=show_modal('TEL.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
#$cekLink = "<a href='user.php' onclick=show_modal('user_detail.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('detail')." </a>";

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='name_employee';//default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
$link = 'head_department_add.php';
if($data->auth_boolean(131910,$_SESSION['pk_id'])){ 
$addLink = "<a href='head_department.php' onclick=popup('".$link."?add=1','HeadDepartment')>ADD</a>";
$tmpl->addVar('page','add',$addLink);
}

	if ($_SESSION['pajak'] =='P'){
		$filter_pajak = "where tbl_dax_employee.tax_status='".$_SESSION['pajak']."'";
	}else{
		$filter_pajak = "where tbl_dax_employee.tax_status in ('P','NP','')";
	}



if ($_POST['btn_search'] )
{
	#$id_serach =  $_POST['cb_search'];
	#$q_serach =  $_POST['txt_search'];

	if(($_POST[txt_department]=='0') || ($_POST[txt_department]=='')){
		$filter_department = "";
	}else{
		$filter_department = " and  tbl_dax_department_head.fk_department='".$_POST[txt_department]."'  ";
	}
	if(($_POST[txt_location]=='0') || ($_POST[txt_location]=='')){
		$filter_location = "";
	}else{
		$filter_location = " and  tbl_dax_department_head.fk_location='".$_POST[txt_location]."'  ";
	}
	if(($_POST[txt_name]=='0') || ($_POST[txt_name]=='')){
		$filter_name = "";
	}else{
		$filter_name = " and tbl_dax_employee.pk_id = '".$_POST[txt_name]."'  ";
	}
	
	
	$sql = "SELECT tbl_dax_department_head.pk_id,tbl_dax_department_head.fk_department,tbl_dax_department_head.fk_location,tbl_dax_department_head.fk_employee,
			tbl_dax_employee.pk_id as id_employee,tbl_dax_employee.full_name as name_employee,tbl_dax_employee.email as email,tbl_dax_employee.tax_status,tbl_dax_department.pk_id as id_department,tbl_dax_department.name as name_department
			,tbl_dax_location.pk_id as id_location,tbl_dax_location.name as name_location
			FROM tbl_dax_department_head
			LEFT JOIN tbl_dax_employee ON tbl_dax_employee.pk_id = tbl_dax_department_head.fk_employee
			LEFT JOIN tbl_dax_department ON tbl_dax_department.pk_id = tbl_dax_department_head.fk_department
			LEFT JOIN tbl_dax_location ON tbl_dax_location.pk_id = tbl_dax_department_head.fk_location
 			$filter_pajak $filter_name $filter_department $filter_location order by  $order_by $sort_order";

	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else
{
	$_SESSION['sql']='';
	$sql = "SELECT tbl_dax_department_head.pk_id,tbl_dax_department_head.fk_department,tbl_dax_department_head.fk_location,tbl_dax_department_head.fk_employee,
			tbl_dax_employee.pk_id as id_employee,tbl_dax_employee.full_name as name_employee,tbl_dax_employee.email as email,tbl_dax_employee.tax_status,tbl_dax_department.pk_id as id_department,tbl_dax_department.name as name_department
			,tbl_dax_location.pk_id as id_location,tbl_dax_location.name as name_location
			FROM tbl_dax_department_head
			LEFT JOIN tbl_dax_employee ON tbl_dax_employee.pk_id = tbl_dax_department_head.fk_employee
			LEFT JOIN tbl_dax_department ON tbl_dax_department.pk_id = tbl_dax_department_head.fk_department
			LEFT JOIN tbl_dax_location ON tbl_dax_location.pk_id = tbl_dax_department_head.fk_location
 			 $filter_pajak order by  $order_by $sort_order";
}

$arrFields = array(
		'tbl_dax_employee.full_name'=>'NAME',
		'tbl_dax_department.name'=>'DEPARTMENT'
);
#print_r($sql);
#$data->showsql($sql);

$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridHeadDepartment($sql,'pk_id','id_employee',$data->ResultsPerPage,$pg,'view',$link,'tambah',$link,'edit',$link,'delete',$link);
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
        # $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		$paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }


###############################################################################################

$tmpl->addVar('page','name',$data->cb_employee_all_number('txt_name',$_POST[txt_name]));
$tmpl->addVar('page', 'department',$data->cb_department_search('txt_department',$_POST[txt_department]));
$tmpl->addVar('page', 'location',$data->cb_location_search('txt_location',$_POST[txt_location]));

$tmpl->addRows('loopData',$DG);

$tmpl->addVar('page','add',$addLink);

$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);


//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>
