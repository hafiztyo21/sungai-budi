<?php
ob_start();
session_start();
include_once 'global.inc.php';
//require_once 'include/class/global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'setting.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
$data = new setting;
$data->auth_boolean('10',$_SESSION['pk_id']);

?>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="include/css/normal.css" />
<head>
<script language="javascript">
function drawtime(){
	arr_month=Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	odate=new Date();
	year=odate.getYear();
	if(odate.getHours()<10){
		c_hour="0"+odate.getHours()
	}else{
		c_hour=odate.getHours();
	}
	if(odate.getMinutes()<10){
		c_minute="0"+odate.getMinutes()
	}else{
	
		c_minute=odate.getMinutes();
	}
	if(odate.getSeconds()<10){
		c_sec="0"+odate.getSeconds();
	}else{
		c_sec=odate.getSeconds();
	}
	span_time.innerHTML=odate.getDate()+" "+arr_month[odate.getMonth()]+" "+(1900+year)+" "+c_hour+":"+c_minute+":"+c_sec;
}

function fLoad(){
//alert('asf');
	drawtime()
	oTimer=setInterval(drawtime,1000);
	
}
</script>
</head>
<body onLoad="fLoad();" topmargin="0px" rightmargin="0px" leftmargin="0px" bottommargin="0px">

<table width="100%" border="0" cellspacing="0" cellpadding="0" height="114" background="image/header.jpg">
  <tr>
    <td width="33%" align="left" valign="top" background="image/bg_image.jpg" bgcolor="#c7d8f6" ><img src="image/header.jpg"></td>
	<td width="67%" align="center" valign="top" background="image/bg_image.jpg" bgcolor="#c7d8f6">
	
	
<script type="text/javascript">

/***********************************************
* Scrollable Menu Links- ?Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/

//configure path for left and right arrows
//var goleftimage='./image/arrow1.gif';
//var gorightimage='./image/arrow2.gif';
//configure menu width (in px):
var menuwidth=600
//configure menu height (in px):
var menuheight=50
//Specify scroll buttons directions ("normal" or "reverse"):
var scrolldir="normal"
//configure scroll speed (1-10), where larger is faster
var scrollspeed=20
//specify menu content
var menucontents=''
menucontents += '<nobr>';
<?php if($data->auth_boolean(10,$_SESSION['pk_id'])){ ?>
menucontents += '<a href="outlet.php" target="mainFrame"><img src="image/btn_outlet.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(11,$_SESSION['pk_id'])){ ?>
menucontents += '<a href="customer_all.php" target="mainFrame"><img src="image/btn_customer.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(12,$_SESSION['pk_id'])){ ?>
menucontents += '<a href="employee_all.php" target="mainFrame"><img src="image/btn_HR.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(13,$_SESSION['pk_id'])){ ?>
menucontents += '<a href="telemarketer_all.php" target="mainFrame"><img src="image/btn_telemarketing.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(14,$_SESSION['pk_id'])){ ?>
menucontents += '<a href="sales_all.php" target="mainFrame"><img src="image/btn_sales.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(15,$_SESSION['pk_id'])){ ?>
menucontents += '<a href="marketing_all.php" target="mainFrame"><img src="image/btn_marketing.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(16,$_SESSION['pk_id'])){ ?>
menucontents +='<a href="schedule_all.php" target="mainFrame"><img src="image/btn_schedule.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(17,$_SESSION['pk_id'])){ ?>
menucontents +='<a href="accounting_all.php" target="mainFrame"><img src="image/btn_accounting.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(18,$_SESSION['pk_id'])){ ?>
menucontents +='<a href="report_all.php" target="mainFrame"><img src="image/btn_report.gif"></a>'
<? } ?>
<?php if($data->auth_boolean(19,$_SESSION['pk_id'])){ ?>
menucontents +='<a href="admin_all.php" target="mainFrame"><img src="image/btn_admin.gif"></a></nobr>'
<? } ?>
//alert(menucontents);
////NO NEED TO EDIT BELOW THIS LINE////////////

var iedom=document.all||document.getElementById
var leftdircode='onMouseover="moveleft()" onMouseout="clearTimeout(lefttime)"'
var rightdircode='onMouseover="moveright()" onMouseout="clearTimeout(righttime)"'
if (scrolldir=="reverse"){
var tempswap=leftdircode
leftdircode=rightdircode
rightdircode=tempswap
}
if (iedom)
document.write('<span id="temp" style="visibility:hidden;position:absolute;top:-100;left:-5000">'+menucontents+'</span>')
var actualwidth=''
var cross_scroll, ns_scroll
var loadedyes=0
function fillup(){
if (iedom){
cross_scroll=document.getElementById? document.getElementById("test2") : document.all.test2
cross_scroll.innerHTML=menucontents
actualwidth=document.all? cross_scroll.offsetWidth : document.getElementById("temp").offsetWidth
}
else if (document.layers){
ns_scroll=document.ns_scrollmenu.document.ns_scrollmenu2
ns_scroll.document.write(menucontents)
ns_scroll.document.close()
actualwidth=ns_scroll.document.width
}
loadedyes=1
}
window.onload=fillup

function moveleft(){
if (loadedyes){
if (iedom&&parseInt(cross_scroll.style.left)>(menuwidth-actualwidth)){
cross_scroll.style.left=parseInt(cross_scroll.style.left)-scrollspeed+"px"
}
else if (document.layers&&ns_scroll.left>(menuwidth-actualwidth))
ns_scroll.left-=scrollspeed
}
lefttime=setTimeout("moveleft()",50)
}

function moveright(){
if (loadedyes){
if (iedom&&parseInt(cross_scroll.style.left)<0)
cross_scroll.style.left=parseInt(cross_scroll.style.left)+scrollspeed+"px"
else if (document.layers&&ns_scroll.left<0)
ns_scroll.left+=scrollspeed
}
righttime=setTimeout("moveright()",50)
}


if (iedom||document.layers){
with (document){
write('<table border="0" cellspacing="0" cellpadding="0" align="right">')
write('<td valign="top" align="right"><a href="#" '+rightdircode+'><img src="image/btn_left.gif" border="0"></a> </td>')
write('<td width="'+menuwidth+'px" valign="top">')
if (iedom){
write('<div style="position:relative;width:'+menuwidth+'px;height:'+menuheight+'px;overflow:hidden;">')
write('<div id="test2" style="position:absolute;left:0;top:0">')
write('</div></div>')
}
else if (document.layers){
write('<ilayer width='+menuwidth+' height='+menuheight+' name="ns_scrollmenu">')
write('<layer name="ns_scrollmenu2" left=0 top=0></layer></ilayer>')
}
write('</td>')
write('<td valign="top"> <a href="#" '+leftdircode+'>')
write('<img src="image/btn_right.gif" border="0"></a>')
write('</td></table>')
}
}

</script>	</td>
  </tr>
  <tr>
    <td background="image/backheader1.PNG"></td>
    <td height="20px" bgcolor="#f9921c" valign="bottom" align="right" background="image/backheader1.PNG"><font color="#FFFFFF"><b> Welcome,&nbsp;<?php  print_r($_SESSION['username']." [".$_SESSION['jobname']."]"); print_r(", "); ?> <span id="span_time"></span>&nbsp;&nbsp;<a href="logout.php">logout</a> </b></font></td>
  </tr>
</table>
</body>
</html>