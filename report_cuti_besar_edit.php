<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'report.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new report;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('report_cuti_tahunan_edit.html');
$tablename = 'tbl_dax_job';

if ($_POST['btn_save'])
{
#print_r($_POST);
	$sql = "INSERT INTO tbl_dax_overwrite (leave_type,fk_employee,qty,day_date,memo,date_created,user_created)
		VALUES ('CB','".$_GET[fk_employee]."','".$_POST['txt_qty']."','".$_POST['txt_date']."','".$_POST['txt_memo']."',now(),'".$_SESSION['pk_id']."')";
   # $data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	
	$sql_log = "insert into tbl_dax_update_log (date_changed,changed_by,fk_employee,day_date,status,referral,memo)
					values(now(),'".$_SESSION[pk_id]."','".$_GET[fk_employee]."','".$_POST['txt_date']."','',
					'Cuti Besar -> Edit -> Save','Date:".$_POST['txt_date'].",Days:".$_POST['txt_qty'].",Memo:".$_POST['txt_memo']."')";
	$data->inpQueryReturnBool($sql_log);
	echo "<script>alert('".$data->err_report('s01')."');window.close();</script>";	
	}
	else
	{	echo "<script>alert('".$data->err_report('s02')."');</script>";	}

}


if ($_GET['fk_employee'])
{   #$data->auth('09030101',$_SESSION['user_id']);
	$row=$data->get_row("select * from tbl_dax_employee where pk_id='".$_GET[fk_employee]."'");
	#$new_id = $value_kode+1;
	$dataRows = array (
				'TEXT' => array('NAME','ID','DATE','NUMBER OF DAYS','MEMO'),
				'DOT'  => array (':',':',':',':'),
				'FIELD' => array (  $row[full_name],
									$row[pk_id],
									$data->datePicker('txt_date',$_POST[txt_date]),
                                    "<input type='text' name='txt_qty' value='".$_POST[txt_qty]."'>",
									"<textarea name='txt_memo'>".$_POST[txt_memo]."</textarea>"
									)
          			  );

    $tittle = "OVERWRITE ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.close();\">"
					);
}

if($_GET[del]==1){
	  $sql2 = "select * from tbl_dax_overwrite where pk_id='".$_GET[id]."'";
	  $overwrite = $data->get_rows($sql2);
	  foreach($overwrite as $field=>$value){
	  $sql_log = "insert into tbl_dax_update_log (date_changed,changed_by,fk_employee,day_date,status,referral,memo)
						values(now(),'".$_SESSION[pk_id]."','".$_GET[fk_employee]."','".$value[day_date]."','',
						'Cuti Besar -> Edit -> Delete','Date:".$value['day_date'].",Days:".$value['qty'].",Memo:".$value['memo']."')";
	  $data->inpQueryReturnBool($sql_log);
	  #echo $sql_log."<hr>";
	  }


      $sql = "delete from tbl_dax_overwrite where pk_id='".$_GET[id]."'";
    #$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}

}

$sql = "select tbl_dax_employee.full_name, tbl_dax_employee.pk_id,tbl_dax_overwrite.*,
			DATE_FORMAT(tbl_dax_overwrite.day_date,'%d-%M-%Y') as vdate  from tbl_dax_overwrite,tbl_dax_employee
		 where      tbl_dax_overwrite.fk_employee= tbl_dax_employee.pk_id and
		 fk_employee='".$_GET[fk_employee]."' and leave_type='CB' order by day_date desc";
$data->ResultsPerPage = 10000;
$DG= $data->dataGridCTedit($sql,$data->ResultsPerPage,$pg,'pk_id',$link);

 $tmpl->addRows('loopData',$DG);
$path = array
 		(
			  'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
			  'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
			  'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
			  'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
			  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);


$tmpl->addVars('row',$dataRows );
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>