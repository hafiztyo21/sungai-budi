<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'employee.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'warehouse.class.php';

$data = new employee;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>
</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="1500" cellpadding="0" cellspacing="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<? if($data->auth_boolean(1310,$_SESSION['pk_id'])){  ?>
<li> <a href="employee.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Employee</a></li>
 <? } ?>

 <? if($data->auth_boolean(1311,$_SESSION['pk_id'])){  ?>
<li> <a href="employee_active.php" target="contentTabFrame" onClick="activeLink(this);">Employee Active</a></li>
 <? } ?>
 <? if($data->auth_boolean(1312,$_SESSION['pk_id'])){  ?>
<li> <a href="employee_nonactive.php" target="contentTabFrame" onClick="activeLink(this);">Employee Non Active</a></li>
 <? } ?>
 <? if($data->auth_boolean(1313,$_SESSION['pk_id'])){  ?>
<li> <a href="employee_male.php" target="contentTabFrame" onClick="activeLink(this);">Employee Male</a></li>
 <? } ?>
 <? if($data->auth_boolean(1314,$_SESSION['pk_id'])){  ?>
<li> <a href="employee_female.php" target="contentTabFrame" onClick="activeLink(this);">Employee Female</a></li>
 <? } ?>

<? if($data->auth_boolean(1315,$_SESSION['pk_id'])){  ?>
<li> <a href="job_possition.php" target="contentTabFrame" onClick="activeLink(this);">Job Position</a></li>
 <? } ?>

<? if($data->auth_boolean(1316,$_SESSION['pk_id'])){  ?>
<li> <a href="department.php" target="contentTabFrame" onClick="activeLink(this);">Department</a></li>
 <? } ?>

<? if($data->auth_boolean(1317,$_SESSION['pk_id'])){  ?>
<li> <a href="location.php" target="contentTabFrame" onClick="activeLink(this);">Location</a></li>
 <? } ?>

<? if($data->auth_boolean(1318,$_SESSION['pk_id'])){  ?>
<li> <a href="shift.php" target="contentTabFrame" onClick="activeLink(this);">Shift</a></li>
 <? } ?>

<? if($data->auth_boolean(1319,$_SESSION['pk_id'])){  ?>
<li> <a href="head_department.php" target="contentTabFrame" onClick="activeLink(this);">Head Department</a></li>
 <? } ?>

<? if($data->auth_boolean(1320,$_SESSION['pk_id'])){  ?>
<li> <a href="head_office.php" target="contentTabFrame" onClick="activeLink(this);">Head Office</a></li>
 <? } ?>

<? if($data->auth_boolean(1321,$_SESSION['pk_id'])){  ?>
<li> <a href="head_hrd.php" target="contentTabFrame" onClick="activeLink(this);">Head HRD</a></li>
 <? } ?>

<? if($data->auth_boolean(1322,$_SESSION['pk_id'])){  ?>
<li> <a href="multiple_location.php" target="contentTabFrame" onClick="activeLink(this);">Multiple Location</a></li>
 <? } ?>


</ul>
</td>
  <!--<tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container">&nbsp;</td>
  </tr>-->
</tr>
</table>
</body>
</html>