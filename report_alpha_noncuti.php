<?php
session_start();
#session_destroy();
#print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'report.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new report;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('report_alpha_noncuti.html');


#$addLink = "<a href='user.php' onclick=show_modal('TEL.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
#$cekLink = "<a href='user.php' onclick=show_modal('user_detail.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('detail')." </a>";

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='employee.emp_name';//default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
$link = 'batch_add.php';
if($data->auth_boolean(121110,$_SESSION['pk_id'])){ 
$addLink = "<a href='batch.php?page=".$_GET[page]."' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:600px;dialogHeight:500px')>ADD</a>";
$tmpl->addVar('page','add',$addLink);
}


if($_SESSION[pajak]=='P'){
	$filter_status = "   (tbl_dax_attendance_status.status in ('A-N-CT','IAP')) and emp_tax_status='P' ";
}else{
	$filter_status = "   (tbl_dax_attendance_status.status in ('A-N-CT','IAP')) ";
}


		if($_POST){
			$date = " and day_date between '".$_POST[txt_from]."' and '".$_POST[txt_to]."'";
			$date_print = $_POST[txt_from]."<>".$_POST[txt_to];
		}else{
			$date = " and day_date='".date("Y-m-d")."'";
			$date_print = date("Y-m-d");
		}
		
if (($_POST['btn_search']) or ($_POST))
{
	#$id_serach =  $_POST['cb_search'];
	#$q_serach =  $_POST['txt_search'];
	
	
	if($_POST[txt_department]=='0'){
		$filter_department = "";
	}else{
		$filter_department = " and  employee.fk_department='".$_POST[txt_department]."'  ";
	}
	if($_POST[txt_location]=='0'){
		$filter_location = "";
	}else{
		$filter_location = " and  employee.fk_location='".$_POST[txt_location]."'  ";
	}
	$sql = "SELECT tbl_dax_attendance_status.*,
			DATE_FORMAT(tbl_dax_attendance_status.day_date,'%d-%M-%Y') as vday_date, 
			DATE_FORMAT(tbl_dax_attendance_status.day_date,'%a') as vday_name,
			time(tbl_dax_attendance_status.date_in) as vdate_in,
			time(tbl_dax_attendance_status.date_out) as vdate_out,
			emp_pk_id,employee.emp_name,employee.department,employee.fk_department,emp_status,emp_shift,emp_tax_status FROM tbl_dax_attendance_status
			inner join
			(select tbl_dax_employee.fk_location,tbl_dax_location.name as location,tbl_dax_employee.pk_id,
			 tbl_dax_employee.full_name as emp_name, tbl_dax_department.name as department,tbl_dax_employee.fk_department, 
			tbl_dax_employee.tax_status as emp_tax_status,tbl_dax_employee.status as emp_status,tbl_dax_employee.pk_id as emp_pk_id,
			tbl_dax_employee.fk_shift as emp_shift
			from tbl_dax_employee,tbl_dax_department,tbl_dax_location where tbl_dax_employee.fk_department = tbl_dax_department.pk_id 
			and tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			)employee on tbl_dax_attendance_status.fk_employee = employee.pk_id
			WHERE 
			employee.pk_id<90000 and emp_status=1 and emp_shift in('1','2') and  $filter_status 
			$date
			$filter_department
			$filter_location
			order by  $order_by $sort_order"; 

	$_SESSION['sql']=$sql;
	$_SESSION['txt_from']=$_POST[txt_from];
	$_SESSION['txt_to']  =$_POST[txt_to];
}
else if (($_SESSION['sql']) and ($_GET))
{
  
	$sql = $_SESSION['sql'];
	$_POST['txt_from']=$_SESSION[txt_from];
	$_POST['txt_to']  =$_SESSION[txt_to];

}else{
	$_SESSION['sql']='';
  	$sql = "SELECT tbl_dax_attendance_status.*,
			DATE_FORMAT(tbl_dax_attendance_status.day_date,'%d-%M-%Y') as vday_date, 
			DATE_FORMAT(tbl_dax_attendance_status.day_date,'%a') as vday_name,
			time(tbl_dax_attendance_status.date_in) as vdate_in,
			time(tbl_dax_attendance_status.date_out) as vdate_out,
			emp_pk_id,employee.emp_name,employee.department,employee.fk_department,emp_status,emp_shift,emp_tax_status FROM tbl_dax_attendance_status
			inner join
			(select tbl_dax_employee.fk_location,tbl_dax_location.name as location,tbl_dax_employee.pk_id,tbl_dax_employee.status as emp_status,
			 tbl_dax_employee.full_name as emp_name, tbl_dax_department.name as department,tbl_dax_employee.pk_id as emp_pk_id,
			tbl_dax_employee.tax_status as emp_tax_status,tbl_dax_employee.fk_department ,tbl_dax_employee.fk_shift as emp_shift
			from tbl_dax_employee,tbl_dax_department,tbl_dax_location where tbl_dax_employee.fk_department = tbl_dax_department.pk_id
			and tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			)employee on tbl_dax_attendance_status.fk_employee = employee.pk_id
			where employee.pk_id<90000 and emp_status=1 and emp_shift in('1','2') and $filter_status $date
			 order by  $order_by $sort_order"; 
}
#print_r($sql);
$pg = ($_POST['btn_search'])? 1 : $_GET['page'];
$DG= $data->DGAlphaNonCuti($sql,'pk_id',$data->ResultsPerPage,$pg,'view',$link,'tambah',$link,'edit',$link,'delete',$link);
  #print_r ($DG);  

if($data->auth_boolean(121910,$_SESSION['pk_id'])){ 
	$print = "<input type='button' name='btprint' value='Print Alpha Non-Cuti Report' onclick=\"AlphaNonCutiPrint('".$_POST[txt_department]."','".$_POST[txt_location]."','".$date_print."')\">";
}


$tmpl->addVar('page', 'print',$print);

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
        # $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		$paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }


###############################################################################################
$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);		

$arrFields = array('0' => ' - All - ','A'=>'Alpha (A)','H'=>'Hadir (H)','HE'=>'Hadir hasil edit (HE)',
			'CB'=>'Cuti Besar (CB)','CT'=>'Cuti Tahunan (CT)','CB'=>'Cuti Dasar (CB)',
			'CH'=>'Cuti Haid (CH)','T'=>'Tugas (T)', 
			'TLK'=>'Tugas Luar Kota (TLK)','IAC' =>'Ijin Akumulasi Cuti (IAC)',
			'IAS' =>'Ijin Akumulasi Potong 50% (IAC)','IAF' =>'Ijin Akumulasi Potong 100% (IAF)',
				'HT1' =>'Hadir Terlambat < 30 menit (HT1)',
				'HT2' =>'Hadir Terlambat > 30 menit (HT2)',
				'HT3' =>'Hadir, Pulang Cepat (HT3)',
				'S' =>'Sakit (S)',
				'TLK' =>'Tugas Luar Kota (TLK)'
			);

$tmpl->addVar('page','cb_location',$data->cb_location_search('txt_location',$_POST[txt_location]));
$tmpl->addVar('page','cb_department',$data->cb_department_search('txt_department',$_POST[txt_department]));
$tmpl->addVar('page','cb_status',$data->cb_select('txt_status',$arrFields,$_POST[txt_status]));
$tmpl->addVar('page','from',$data->datePicker('txt_from',$_POST[txt_from]));
$tmpl->addVar('page','to',$data->datePicker('txt_to',$_POST[txt_to]));

$tmpl->addRows('loopData',$DG);
$tmpl->addVar('page','add',$addLink);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

$tmpl->displayParsedTemplate('page');
?>
