<?php
session_start();
#session_destroy();
//print_r($_SESSION);
//print_r($_GET);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('outlet_add.html');
$tablename = 'tbl_dax_outlet';


if ($_POST['btn_save']=='save')
{
	$country_name = explode(';',$_POST[txt_country]);
	$country=$country_name[0];

	$sql = "INSERT INTO tbl_dax_outlet (code,name,address,city,province,country,phone,fax,currency_id,status,state_code ) VALUES ('".$_POST['txt_code']."', '".$_POST['txt_name']."','".$_POST['txt_address']."','".$_POST['txt_city']."','".$_POST['txt_province']."','".$country."','".$_POST['txt_phone']."','".$_POST['txt_fax']."','".$_POST['txt_currency_id']."', '1','".$_POST['txt_state']."')";
    #$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('s01')."');window.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('s02').", Outlet Code must be unique ');</script>";	}

}



if ($_GET['detail']==1)
{
    $rows = $data->get_row("select * from tbl_dax_outlet where pk_id='".$_GET['id']."'");
#print_r($rows);
	$currency = $data->get_value("select currency_name from tbl_dax_currency where pk_id='".$rows[currency_id]."'");
         $dataRows = array (
				'TEXT' =>  array('Code','Name','Address','City','Currency'),
			
				'FIELD' => array ('&nbsp;'.$rows[code],'&nbsp;'.$rows[name],'&nbsp;'.$rows[address],'&nbsp;'.$rows[city],'&nbsp;'.$currency),
				'TEXT1' =>  array('Province','Country','State Code','Phone','fax'),
			
				'FIELD1' => array ('&nbsp;'.$rows[province],'&nbsp;'.$rows[country],'&nbsp;'.$rows[state_code],'&nbsp;'.$rows[phone],'&nbsp;'.$rows[fax])
				
			);

			$tittle = "OUTLET DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}

if ($_GET['add'] == 1)
{   #$data->auth('09030101',$_SESSION['user_id']);
	$dataRows = array (
				'TEXT' =>  array('Code','Name','Address','City','Currency'),
				'DOT'  => array (':',':',':',':',':'),
				'FIELD' =>  array('<input type=text name=txt_code id=txt_code>',
				'<input type=text name=txt_name id=txt_name>',
				//'<input type=text name=txt_address id=txt_address>',
				'<textarea rows=3 cols=16 name=txt_address id=txt_address></textarea>',
				'<input type=text name=txt_city id=txt_city>',
				$data->cb_dax_currency('txt_currency_id'),
				),
				
				'TEXT1' =>  array('Province','Country','State Code','Phone','Fax'),
				'DOT1'  => array (':',':',':',':'),
				'FIELD1' => array ('<input type=text name=txt_province id=txt_province>',
				$data->cb_country('txt_country'),
				'<input type=text name=txt_state id=txt_state>',
				'<input type=text name=txt_phone id=txt_phone>',
				'<input type=text name=txt_fax id=txt_fax>',
									)
						  );

    $tittle = "OUTLET ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.close();\">"
					);
}

if ($_POST['btn_save_edit'])
{
	$country_name = explode(';',$_POST[txt_country]);
	$country=$country_name[0];
	
	$sql= "UPDATE ".$tablename." SET name = '".$_POST['txt_name']."',address = '".$_POST['txt_address']."',city = '".$_POST['txt_city']."', province= '".$_POST['txt_province']."', country = '".$country."',phone = '".$_POST['txt_phone']."',fax = '".$_POST['txt_fax']."',currency_id = '".$_POST['txt_currency_id']."',state_code='".$_POST['txt_state']."'
			WHERE pk_id = '".$_GET['id']."'";
    #$data->showsql($sql);
	if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('s01')."');window.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('s02')."');</script>";	}
}

if ($_GET['edit'] == 1)
{   

#$data->auth('09030102',$_SESSION['user_id']);
	$rows = $data->get_row("select * from ".$tablename." where pk_id='".$_GET['id']."'");
    # print_r($rows);
	$dataRows = array (
				'TEXT' =>  array('Code','Name','Address','City','Currency'),
				'TEXT1' =>  array('Province','Country','State Code','Phone','Fax'),
			
				'FIELD' =>  array('&nbsp;'.$rows[code]."<input type=hidden name=txt_name value='".$rows[code]."' >","<input type=text name=txt_name value='".$rows[name]."'>",/*"<input type=text name=txt_address value='".$rows[address]."'>"*/ "<textarea rows=3 cols=16 name=txt_address>".$rows[address]."</textarea>","<input type=text name=txt_city value='".$rows[city]."'>",$data->cb_dax_currency('txt_currency_id',$rows['currency_id'])),
				'FIELD1' => array ("<input type=text name=txt_province value='".$rows[province]."'>",$data->cb_country('txt_country',$rows[country]),"<input type=text name=txt_state id=txt_state value='".$rows[state_code]."'>","<input type=text name=txt_phone value='".$rows[phone]."'>","<input type=text name=txt_fax value='".$rows[fax]."'>"

)
         			   );
    $tittle = "OUTLET EDIT";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.close();\">"
					
					);
}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>