<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'employee.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new employee;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('employee_log.html');


if($_GET['del']==1){

	$sql = "DELETE FROM tbl_dax_log where pk_id='".$_GET['id']."'";
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');window.parent.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}
}
#####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='first_name';//default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################


#$link = 'user_customer_add.php';
#$addLink = "<a href='user_customer_log.php' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>ADD</a>";
//$addLink = "<a href='user.php' onclick=show_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
$job_id = $data->get_value("select job_id from tbl_dax_employee where pk_id='".$_GET[id]."'");	
if($job_id=='SLSMGR'){
	$job_id = 'tbl_dax_log.fk_sales_manager';

}else if($job_id=='TLMMGR'){
	$job_id = 'tbl_dax_log.fk_tm_manager';
	
}else{
$job_id = 'tbl_dax_log.employee_id';
	
}
	

if ($_POST['btn_search'] )
{
	$id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];

	$sql = "SELECT tbl_dax_log.pk_id,tbl_dax_log.date_create,
			concat(tbl_dax_customer.first_name,' ',tbl_dax_customer.last_name) AS customer,
			tbl_dax_log_status.name AS STATUS,
			tbl_dax_job.name AS JOB_ID
			FROM tbl_dax_log
			LEFT JOIN tbl_dax_customer ON tbl_dax_log.customer_id=tbl_dax_customer.pk_id
			LEFT JOIN tbl_dax_status ON tbl_dax_log.status=tbl_dax_log_status.pk_id
			LEFT JOIN tbl_dax_job ON tbl_dax_log.job_id=tbl_dax_job.code
			 WHERE $job_id='".$_GET['id']."'
			upper($id_serach) like upper('%$q_serach%')" ;
		
	#$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else

{
	$_SESSION['sql']='';
	
	$sql = "SELECT tbl_dax_log.pk_id,tbl_dax_log.date_create,
			concat(tbl_dax_customer.first_name,' ',tbl_dax_customer.last_name) AS customer,
			tbl_dax_log_status.name AS STATUS,
			tbl_dax_job.name AS JOB_ID
			FROM tbl_dax_log
			RIGHT JOIN tbl_dax_customer ON tbl_dax_log.customer_id=tbl_dax_customer.pk_id
			LEFT JOIN tbl_dax_log_status ON tbl_dax_log.status=tbl_dax_log_status.pk_id
			LEFT JOIN tbl_dax_job ON tbl_dax_log.job_id=tbl_dax_job.code
			 WHERE $job_id='".$_GET['id']."'
			 order by $order_by $sort_order";
			 #$data->showsql($sql);
			
}

#print_r($sql);
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridEmployee($sql,'pk_id','card_id',$data->ResultsPerPage,$pg,'view',$linkDetail,'menu',$link,'edit',$link,'delete',$link);
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?id=".$_GET[id]."&page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?id=".$_GET[id]."page=1" . $InfoArray["PREV_PAGE"] . "'<img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
      } else {
         $paging_no .= "<a href='?id=".$_GET[id]."&page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?id=".$_GET[id]."page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?id=".$_GET[id]."page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);

$tmpl->addVar('page','add',$addLink);

$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>
