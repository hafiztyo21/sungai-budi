<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'user.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new user;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('refund.html');


if($_GET['del']==1){

	$sql = "DELETE FROM tbl_dax_payment where pk_id='".$_GET['id']."'";
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');window.parent.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}
}
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='desc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$link = 'payment_refund.php';
$addLink = "<a href='refund.php' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:800px')>ADD</a>";
//$addLink = "<a href='user.php' onclick=show_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";

if ($_POST['btn_search'] )
{
	$id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];

	$sql = "SELECT tbl_dax_refund.pk_id,tbl_dax_refund.memo,tbl_dax_refund.cancellation_fee,concat(tbl_dax_refund.reason_code,' - ',tbl_dax_refund.reason_number) as refund_id ,  tbl_dax_deal_info.pk_id as deal_id, tbl_dax_deal_info.customer_name as customer_name, tbl_dax_refund.date_refund as date_refund, 
	tbl_dax_refund.value_refund as value_refund FROM tbl_dax_refund LEFT JOIN tbl_dax_deal_info ON tbl_dax_deal_info.pk_id = tbl_dax_refund.fk_deal_info  WHERE upper($id_serach) like upper('%$q_serach%')" ;

	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else

{
	$_SESSION['sql']='';
	$sql = "SELECT tbl_dax_refund.pk_id,tbl_dax_refund.memo,tbl_dax_refund.cancellation_fee,concat(tbl_dax_refund.reason_code,' - ',tbl_dax_refund.reason_number) as refund_id ,  tbl_dax_deal_info.pk_id as deal_id, tbl_dax_deal_info.customer_name as customer_name, tbl_dax_refund.date_refund as date_refund, 
	tbl_dax_refund.value_refund as value_refund FROM tbl_dax_refund LEFT JOIN tbl_dax_deal_info ON tbl_dax_deal_info.pk_id = tbl_dax_refund.fk_deal_info 
	order by $order_by $sort_order";
		
	#print_r($sql);
#	
}
$arrFields = array(
		'tbl_dax_deal_info.pk_id'=>'DEAL ID',
		'tbl_dax_deal_info.customer_name '=>'CUSTOMER NAME',
		'tbl_dax_refund.date_refund '=>'DATE REFUND',
		
);
  #$data->showsql($sql);
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridUser($sql,'pk_id','user_name',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link);
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         #$paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		 $paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);

$tmpl->addVar('page','add',$addLink);

$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>