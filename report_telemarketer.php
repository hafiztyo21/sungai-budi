<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'report.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new report;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('report_telemarketer.html');


####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='date_refund'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='desc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

#print_r($_POST);

if ($_POST['bt_go'] == 'Go')
{

		$sql = "SELECT tbl_dax_log.pk_id,tbl_dax_log.date_create,
			tbl_dax_customer.first_name AS CUSTOMER,
			tbl_dax_log_status.name AS STATUS,
			tbl_dax_job.name AS JOB_ID
			FROM tbl_dax_log
			RIGHT JOIN tbl_dax_customer ON tbl_dax_log.customer_id=tbl_dax_customer.pk_id
			LEFT JOIN tbl_dax_log_status ON tbl_dax_log.status=tbl_dax_log_status.pk_id
			LEFT JOIN tbl_dax_job ON tbl_dax_log.job_id=tbl_dax_job.code
			 WHERE date_create BETWEEN '".$_POST['txt_datefrom']."' AND '".$_POST['txt_dateto']."' 
			 AND tbl_dax_log_status.name='".$_POST['txt_status_telamarketer']."' AND tbl_dax_log.employee_id='".$_POST['txt_telamarketer']."'";
			 # $data->showsql($sql);
		
		
				
	$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
	$DG= $data->dataGridReport($sql,'pk_id','user_name',100000,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link);
	#print_r($sql);
	#print_r($DG);
	$tmpl->addVar('page','print',"<input type='button' name='btprint' value='Print' onclick=\"show_modal('report_refund_print.php?from=".$_POST[txt_datefrom]."&to=".$_POST[txt_dateto]."','status:no;help:no;dialogWidth:800px;dialogHeight:800px')\">");
}
	


#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################
$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);		


$tmpl->addRows('loopData',$DG);



$tmpl->addVar('page','add',$addLink);
$tmpl->addVar('grand','grandtotal1',$data->convert_to_money($tot,''));
$tmpl->addVar('date','datefrom', $data->datePicker('txt_datefrom',$_POST['txt_datefrom']) );
$tmpl->addVar('date','dateto',$data->datePicker('txt_dateto',$_POST['txt_dateto']) );
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);
$tmpl->addVar('date','cb_telemarketer',$data->cb_telemarketer('txt_telamarketer',$_POST['txt_telamarketer']));
$tmpl->addVar('date','cb_status_telemarketer',$data->cb_status_telemarketer('txt_status_telamarketer',$_POST['txt_status_telamarketer']));


//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>