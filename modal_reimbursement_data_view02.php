<?php
session_start();
#session_destroy();
#print_r($_SESSION);
#print_r($_GET);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'reimburse.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new reimburse;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('modal_reimbursement_data_view02.html');


if($_GET['del']==1){

	$sql = "DELETE FROM tbl_dax_reimbursement_data where pk_id='".$_GET['id']."'";
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');window.location='modal_reimbursement_data_view.php?detail=1&mode=".$_GET[mode]."';</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}
}
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
if($_GET['mode']=='pengobatan'){
	$title = "VIEW DATA PENGOBATAN";
	$type_remburse = 1;
}elseif($_GET['mode']=='lahir_normal'){
	$title = "VIEW DATA BERSALIN NORMAL";
	$type_remburse = 2;
}elseif($_GET['mode']=='lahir_operasi'){
	$title = "VIEW DATA BERSALIN OPERASI";
	$type_remburse = 3;
}elseif($_GET['mode']=='lensa'){
	$title = "VIEW DATA PEMBUATAN LENSA KACAMATA";
	$type_remburse = 4;
}elseif($_GET['mode']=='rangka'){
	$title = "VIEW DATA PEMBUATAN RANGKA KACAMATA";
	$type_remburse = 5;
}else{
	$title = "";
}
if($_GET[id]!=''){
	$id_employee = $_GET[id];
	$filter_employee = " and tbl_dax_reimbursement_data.fk_employee=$id_employee ";
	$filter_status =  "";
}else{
	$filter_employee = "";
	$filter_status =  " and tbl_dax_reimbursement_data.status_reimburse=0";
}


#$linkEdit='user_edit.php';
$link_view = "modal_reimbursement_data_view.php?detail=1&id=".$_GET[id]."&mode=".$_GET[mode]."";
$link = 'modal_reimbursement_data_add2.php';

if($data->auth_boolean(161010,$_SESSION['pk_id'])){
	$addLink = "<a href='".$link_view."' onclick=popup('".$link."?add=1&id=".$_GET[id]."','ReimbursementDataVIew')>ADD</a>";
	//$addLink = "<a href='user.php' onclick=show_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
	#$tmpl->addVar('page','add',$addLink);

}



	$sql  = "select tbl_dax_reimbursement_data.*,DATE_FORMAT(tbl_dax_reimbursement_data.date,'%d-%M-%Y')as vdate,
			full_name,tbl_dax_employee.pk_id as emp_id
			 from tbl_dax_reimbursement_data
			left join
			tbl_dax_employee on tbl_dax_reimbursement_data.fk_employee=tbl_dax_employee.pk_id
			where 1 $filter_employee and tbl_dax_reimbursement_data.fk_type_reimburse = $type_remburse
			and year(now())=year(tbl_dax_reimbursement_data.date) $filter_status
			order by $order_by $sort_order";

#echo $sql;

$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridReimburseDataView($sql,'pk_id','fk_employee',$_GET[mode],$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link,$link_view);
  #print_r ($DG);
#$data->listData();
$button = array ('SUBMIT' => "",
					 'RESET'  => "<input type=button name=cancel value=back onclick=\"opener.document.form1.submit();window.parent.close();\">"
					);
					
$tmpl->addVars('button',$button);
#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?detail=1&mode=pengobatan&page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?detail=1&mode=pengobatan&page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         #$paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		 $paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?detail=1&mode=pengobatan&page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?detail=1&mode=pengobatan&page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?detail=1&mode=pengobatan&page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################
#print_r($_GET);
$tmpl->addRows('loopData',$DG);

$tmpl->addVar('page', 'title',$title);
#$tmpl->addVar('page', 'button',"<input type=button name=cancel value=cancel onclick=\"window.parent.close();\">");
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>