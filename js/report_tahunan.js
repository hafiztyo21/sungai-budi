var app = angular.module('SungaiBudi.Controller', ['ui.grid','ui.grid.resizeColumns','ui.grid.cellNav','ui.grid.autoResize','ui.grid.exporter', 'ngAnimate']);

app.controller('mainCtrl', ['$scope', '$http', '$interval',  'uiGridConstants', '$location', '$uibModal',
 function($scope, $http, $interval,  uiGridConstants, $location, $uibModal) {
	$scope.orders=[];
	$scope.listEmployee=[];
	$scope.listDepartment=[];
	$scope.listLocation=[];
	$scope.listJobPosition=[];
	
	$scope.data={
		JobPositionID:0,
		DepartmentID:0,
		LocationID:0,
		EmployeeID:0
	};
	$scope.gridIsLoading=false;
	$scope.gridOrdersOptions = {
        data : $scope.orders,  
		onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
        enableSorting: true,
        enableGridMenu: true,
        enableFiltering: false,
		showColumnFooter: true,
        enableColumnResizing: true,
		rowTemplate:'rowTemplate.html',
        columnDefs:[
            { field: 'name', displayName : 'Name', width:200, aggregationType: uiGridConstants.aggregationTypes.count, footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:right">{{col.getAggregationValue() }}</div>' },
            { field: 'id', displayName : 'ID', width:80 },
			{ field: 'start', displayName : 'Start', width:130 },
			{ field: 'begin', displayName : 'Begin Period', width:130 },
			{ field: 'end', displayName : 'End Period', width:130 },
			{ field: 'total', displayName : 'Total', width:80,  cellClass: 'grid-alignright', type:'number' },
			{ field: 'masal', displayName : 'Cuti Masal dipakai', width:130,  cellClass: 'grid-alignright', type:'number' },
			{ field: 'used', displayName : 'Dipakai', width:80,  cellClass: 'grid-alignright', type:'number' },			
			{ field: 'available', displayName : 'Available', width:80,  cellClass: 'grid-alignright', type:'number' },
           
			{ name: 'Actio', enableFiltering:false, field:'name', width:150, cellTemplate:
				'<button class="btn btn-sm btn-info" ng-click="grid.appScope.pod(row.entity)" >View</button> &nbsp; <button class="btn btn-sm btn-warning" ng-click="grid.appScope.edit(row.entity)" >Edit</button>'
			}	
        ],
    };
	$scope.pod = function(data){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/view_detailattendance.html?v=10',
            controller: 'viewDetailCtrl',
            size: 'lg',
            resolve: {
                dataOrder: function () {
                    return data;
                }
            },
            scope: $scope
        });
    }
	
	$scope.edit = function(data){
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/edit_attendanceyearly.html?v=6',
            controller: 'editAttendanceYearlyCtrl',
            size: 'lg',
            resolve: {
                dataOrder: function () {
                    return data;
                }
            },
            scope: $scope
        });
		modalInstance.result.then(function (returnValue) {
				if( returnValue=="OK" ) {
					$scope.getCutiTahunan();
				}
		}, function () {
				console.log('Modal dismissed at: ' + new Date());
		});
    }
	
	$scope.getDepartment = function()
	{
		$scope.gridIsLoading = true;
		$http({
            method: "POST",
            url: 'http://'+$location.$$host+'/sungaibudi/webservices/getDepartment.php',
            data: { 'data': '' },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
            $scope.gridIsLoading = false;
            for(var i=0, length=response.data.records.length;i<length;i++){
                for ( var temp in response.data.records[i] )
                {
					response.data.records[i][temp] = decodeURIComponent(response.data.records[i][temp]);
                }
			}		
			$scope.listDepartment=response.data.records;
			$scope.data.DepartmentID=0;
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });	
		
	}
	$scope.getEmployee = function()
	{
		$scope.gridIsLoading = true;
		$http({
            method: "POST",
            url: 'http://'+$location.$$host+'/sungaibudi/webservices/getEmployee.php',
            data: { 'data': '' },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
            $scope.gridIsLoading = false;
            for(var i=0, length=response.data.records.length;i<length;i++){
                for ( var temp in response.data.records[i] )
                {
					response.data.records[i][temp] = decodeURIComponent(response.data.records[i][temp]);
                }
			}
			$scope.data.EmployeeID=0;
			$scope.listEmployee=response.data.records;
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });	
		
	}
	$scope.getLocation = function()
	{
		$scope.gridIsLoading = true;
		$http({
            method: "POST",
            url: 'http://'+$location.$$host+'/sungaibudi/webservices/getLocation.php',
            data: { 'data': '' },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
            $scope.gridIsLoading = false;
            for(var i=0, length=response.data.records.length;i<length;i++){
                for ( var temp in response.data.records[i] )
                {
					response.data.records[i][temp] = decodeURIComponent(response.data.records[i][temp]);
                }
			}	
			$scope.data.LocationID=0;			
			$scope.listLocation=response.data.records;
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });	
	}
	$scope.getJobPosition = function()
	{
		$scope.gridIsLoading = true;
		$http({
            method: "POST",
            url: 'http://'+$location.$$host+'/sungaibudi/webservices/getJobPosition.php',
            data: { 'data': '' },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
            $scope.gridIsLoading = false;
            for(var i=0, length=response.data.records.length;i<length;i++){
                for ( var temp in response.data.records[i] )
                {
					response.data.records[i][temp] = decodeURIComponent(response.data.records[i][temp]);
                }
			}	
			$scope.data.JobPositionID=0;			
			$scope.listJobPosition=response.data.records;
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });	
	}
	$scope.getCutiTahunan = function(){
		
		$scope.gridIsLoading = true;
		$http({
            method: "POST",
            url: 'http://'+$location.$$host+'/sungaibudi/webservices/getCutiTahunan.php',
            data: { 'data': JSON.stringify($scope.data) },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
            $scope.gridIsLoading = false;
			
            for(var i=0, length=response.data.records.length;i<length;i++){
                for ( var temp in response.data.records[i] )
                {
					response.data.records[i][temp] = decodeURIComponent(response.data.records[i][temp]);
                }
			}
			
			$scope.orders=response.data.records;
			$scope.gridOrdersOptions.data= response.data.records;
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });	
    };
	
	$scope.getHeight = function(){
		return window.innerHeight - 125;
	};
	
	$scope.refresh = function()
	{
		$scope.getCutiTahunan();
	};
	$scope.getJobPosition();
	$scope.getEmployee();
	$scope.getDepartment();
	$scope.getLocation();
	
}])

app.controller('editAttendanceYearlyCtrl', ['$uibModalInstance','$scope', 'dataOrder', '$location', '$http', 'uiGridConstants', 
function ($uibModalInstance, $scope, dataOrder, $location, $http, uiGridConstants) {
	$scope.data={};
	console.log(dataOrder);
	$scope.input={
		id:dataOrder.id,
		begin: dataOrder.begin,
		end: dataOrder.end,
		extenddate:0,
		extendday:0,
		prev:0,
		notes:''
	};
	$scope.data.id=dataOrder.id;
	$scope.data.name=dataOrder.name;
	$scope.data.begin = dataOrder.begin;
	$scope.data.end = dataOrder.end;
	$scope.data.totals = dataOrder.total;
	$scope.data.masal = dataOrder.masal;
	$scope.data.used = dataOrder.used;
	
	$scope.save = function(){
		console.log("OK");
		if( true==confirm("Are you sure want to submit ?"))
		{
			if( $scope.input.extenddate.length==0 ) $scope.input.extenddate=0;
			if( $scope.input.extendday.length==0 ) $scope.input.extendday=0;
			if( $scope.input.prev.length==0 ) $scope.input.prev=0;
			$http({
				method: "POST",
				url: 'http://'+$location.$$host+'/sungaibudi/webservices/saveAttendanceYearly.php',
				data: { 'data': JSON.stringify($scope.input) },
				headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
			}).then(function mySuccess(response) {
				alert(decodeURIComponent(response.data.message));
				if( decodeURIComponent(response.data.message)=="OK" ){
					$uibModalInstance.close("OK");
				}
			}, function myError(response) {
				console.log(response.status);
			});	
		}        
    }
	$scope.init = function(){
        $http({
            method: "POST",
            url: 'http://'+$location.$$host+'/sungaibudi/webservices/getAttendanceYearly.php',
            data: { 'data': JSON.stringify($scope.data) },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
			$scope.input.extenddate = Number(response.data.extenddate);
			$scope.input.extendday = Number(response.data.extendday);
			$scope.input.prev = Number(response.data.prev);
			$scope.input.notes = decodeURIComponent(response.data.notes);
        }, function myError(response) {
            console.log(response.status);
        });
    }
	
	$scope.init();
	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	}
	
}])

app.controller('viewDetailCtrl', ['$uibModalInstance','$scope', 'dataOrder', '$location', '$http', 'uiGridConstants', 
function ($uibModalInstance, $scope, dataOrder, $location, $http, uiGridConstants) {
	$scope.data={};
	console.log(dataOrder);
	
	$scope.data.id=dataOrder.id;
	$scope.data.name=dataOrder.name;
	$scope.data.begin = dataOrder.begin;
	$scope.data.end = dataOrder.end;
	$scope.gridIsLoading=false;
	
	$scope.orders=[];
	$scope.gridOrdersOptions = {
        data : $scope.orders,  
		onRegisterApi :function(gridApi){ $scope.gridApi = gridApi; },
        enableSorting: true,
        enableGridMenu: true,
        enableFiltering: true,
		showColumnFooter: true,
        enableColumnResizing: true,
		rowTemplate:'rowTemplate.html',
        columnDefs:[
            { field: 'daydate', displayName : 'Date', width:120, aggregationType: uiGridConstants.aggregationTypes.count, footerCellTemplate: '<div class="ui-grid-cell-contents" style="text-align:right">{{col.getAggregationValue() }}</div>' },
            { field: 'datein', displayName : 'In', width:150 },
			{ field: 'dateout', displayName : 'Out', width:150 },
			{ field: 'status', displayName : 'Status', width:130 }
        ],
    };
	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	}
	$scope.init = function(){
        $scope.gridIsLoading = true;
        $http({
            method: "POST",
            url: 'http://'+$location.$$host+'/sungaibudi/webservices/getViewDetailAttendance.php',
            data: { 'data': JSON.stringify($scope.data) },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' }
        }).then(function mySuccess(response) {
            $scope.gridIsLoading = false;			
            for(var i=0, length=response.data.records.length;i<length;i++){
                for ( var temp in response.data.records[i] )
                {
					response.data.records[i][temp] = decodeURIComponent(response.data.records[i][temp]);
                }
			}
			$scope.orders=response.data.records;
			$scope.gridOrdersOptions.data= response.data.records;
        }, function myError(response) {
            $scope.gridIsLoading = false;
            console.log(response.status);
        });
    }
	
	$scope.init();
}]);
