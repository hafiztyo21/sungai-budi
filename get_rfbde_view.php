<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'report.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new report;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('get_rfbde_view.html');

$get = "&id_batch=".$_GET[id_batch]."&id_dept=".$_GET[id_dept];
$row = $data->get_row("select * from tbl_dax_batch where pk_id='".$_GET['id_batch']."'");
#$addLink = "<a href='user.php' onclick=show_modal('TEL.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
#$cekLink = "<a href='user.php' onclick=show_modal('user_detail.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('detail')." </a>";

####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_dax_employee.full_name';//default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
if($data->auth_boolean(121110,$_SESSION['pk_id'])){ 
#$link = "batch_payroll_edit.php?id=".$data_work[pk_id]."&fk_employee=".$_GET[id]."&day_date=".$date_work."";
$addLink = "<a href='batch.php?page=".$_GET[page]."' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:600px;dialogHeight:500px')>ADD</a>";
$tmpl->addVar('page','add',$addLink);
}

if($_SESSION[pajak]=='P'){
		$filter = " and  tbl_dax_employee.tax_status='P'";
	}else{
		$filter = " ";
	}	

	$date = $_GET[day_date];
	$d =  $data->get_row("select * from tbl_dax_batch where pk_id='".$_GET[id_dept]."'");
	$sql ="select tbl_dax_employee.*,tbl_dax_machine_m.*,DATE_FORMAT(tbl_dax_machine_m.day_date,'%d-%M-%Y') as v_day_date,
				TIME(tbl_dax_machine_m.checktime) as time_in,'' as time_out,tbl_dax_machine_m.status as status_code,
			tbl_dax_employee.pk_id as emp_id,tbl_dax_machine_m.pk_id as m_id
			 from tbl_dax_machine_m inner join tbl_dax_employee
		   where tbl_dax_machine_m.badgenumber=tbl_dax_employee.pk_id
		  and  tbl_dax_machine_m.day_date ='".$date."' and tbl_dax_machine_m.status like '".$_GET[id_status]."%'
          and tbl_dax_employee.fk_department='".$_GET[id_dept]."' and tbl_dax_employee.status=1 and tbl_dax_employee.fk_shift in('1','2') and tbl_dax_employee.pk_id<90000
	  $filter order by tbl_dax_employee.full_name ASC";



#print_r($_GET[id_batch]);
#$data->showsql($sql);
#print_r($sql);

$searchCB = $data->cb_department('txt_department',$_POST[txt_department])."&nbsp;<input type='submit' name='btn_search' value='Search'>";
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$get="id_dept=".$_GET[id_dept]."&day_date=$date&id_status=".$_GET[id_status]."&page=$pg";
$DG= $data->dataGridRFBDE($get,$sql,'tbl_dax_machine_m.pk_id',$_GET[id_batch],$data->ResultsPerPage,$pg,'view',$link,'tambah',$link,'edit',$link,'delete',$link);
 #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "$get'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
        # $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		$paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "$get'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "$get'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }


###############################################################################################


$tmpl->addRows('loopData',$DG);
$tmpl->addVar('page','add',$addLink);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);


//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>
