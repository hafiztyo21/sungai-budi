<?php
/***********************************************
* Snippet Name : KonversiRupiah *
* Email : [EMAIL PROTECTED] *
* License : GPL (General Public License) *
***********************************************/
// START KONVERSI RUPIAH CLASS
//class intToRupiah {
class terbilang {
        var $numb = Array();
        var $tail;
        var $number;
        var $currency;
        var $min;
        //function intToRupiah () {
		function terbilang () {
        $this->numb = Array ("",
        "SATU",
        "DUA",
        "TIGA",
        "EMPAT",
        "LIMA",
        "ENAM",
        "TUJUH",
        "DELAPAN",
        "SEMBILAN");
}
function mod($a,$b) {
        return $a-$b*floor($a/$b);
}
function setTail($str) {
        $this->tail = $str;
}
function setNumber($int) {
        $int = trim($int);
        if (is_int(strpos($int,"-"))) {
                $this->number = substr($int,strpos($int,"-")+1,strlen($int));
                $this->currency = "MINUS";
        } else {
                $this->number = $int;
        }
        $this->setAsCurrency();
}
function getCurrency() {
        return $this->currency;
}
/*
function printCurrency() {
        print ucfirst(strtoupper(trim($this->currency)));
}


*/
function printCurrency() {
        return ucfirst(strtolower(trim($this->currency)));
}


function setAsCurrency() {
        $xpos = strpos($this->number,".");
        if (is_int($xpos)) {
                $pecahan = round(substr($this->number,$xpos,strlen($this->number)),2);
                $last = substr($this->number,0,$xpos);
        } else {
                $pecahan = "";
                $last = $this->number;
        }
        if ($last==0 || $this->number==0) {
                $this->currency .= "NOL ".$this->tail;
        } else {
                $triliun = floor($last/pow(10,12));
                $last = $this->mod($last,1000000000000);

                $miliar = floor($last/pow(10,9));
                $last = $this->mod($last,1000000000);

                $juta = floor($last/pow(10,6));
                $last = $this->mod($last,1000000);

                $ribu = floor($last/pow(10,3));
                $last = $this->mod($last,1000);

                $kata = $this->ThreeDigit($triliun, "TRILIUN");
                $kata .= $this->ThreeDigit($miliar, "MILIAR");
                $kata .= $this->ThreeDigit($juta, "JUTA");
                $kata .= $this->ThreeDigit($ribu, "RIBU");
                $kata .= $this->ThreeDigit($last,"");
                $kata .= " ".$this->tail;
        }
        if ($pecahan>0) {
                $kata .= " DAN". $this->ThreeDigit(round($pecahan*100),"SEN");
        }
        $this->currency .= strtoupper($kata);
}
function ThreeDigit($amount, $suffix="") {
        $last = (int) $amount;
        $kata = "";
        if ($last == 1 && $suffix=="RIBU") {
                $kata = " SE".$suffix;
                return $kata;
        }
        if ($last < 20 && $last > 10) {
                if ($last==11) {
                        $kata = " SEBELAS";
                } else {
                        $kata = " ".$this->numb[$last-10]." BELAS";
                }
                if ($suffix != "") {
                        $kata .= " ".$suffix;
                }
                return $kata;
        }
        $ratus = floor($last/100);
        if ($ratus <= 0) {
                $kata .= "";
        } elseif ($ratus == 1) {
                $kata .= " SERATUS";
        } else {
                $kata .= " ".$this->numb[$ratus]." RATUS";
        }
        $last = $this->mod($last,100);
        if ($last < 20 && $last > 10) {
                if ($last == 11) {
                        $kata .= " SEBELAS ". $suffix;
                } else {
                        $kata .= " ".$this->numb[$last-10]." BELAS ". $suffix;
                }
                return $kata;
        }
        $puluh = floor($last/10);
        if ($puluh == 0) {
                $kata .= "";
        } elseif ($puluh == 1) {
                $kata .= " SEPULUH";
        } else {
                $kata .= " ".$this->numb[$puluh]." PULUH";
        }
        $last = $this->mod($last,10);
        if ($last>0&&$last<=9) {
                $kata .= " ".$this->numb[$last];
        }
        if ($amount>0&&$amount<=1000) {
                $kata .= " ".$suffix;
        }
        return $kata;
        }
}
// END KONVERSI RUPIAH CLASS

/* THIS LINE BELOW SHOW YOU HOW TO RUN IT
$int = $_GET["int"];
if (!isset($int)) $int = "1";
if (!isset($_GET["int"])) {
print "Jika anda tidak memasukkan sebuah query string, maka defaultnya
adalah angka '1' <br>";
print "Untuk memasukkan query string, panggilah alamat di atas dengan
tambahan huruf '?int=n'<br>";
print "dimana 'n' adalah angka yang anda inginkan.<br><br>";
print "Maximum angka adalah <a
href=\"?int=999999999999999.99\">999999999999999.99</a><br>";
print "Minimum angka adalah <a
href=\"?int=-999999999999999.99\">-999999999999999.99</a><br>";
}

if ($int>999999999999999.99||$int<-999999999999999.99) {
        print "Out of value";
} else {
        $cc = new intToRupiah;
        $cc->setTail("rupiah");
        $cc->setNumber($int);
        $cc->printCurrency();
}*/
##########################
####### cara pakai #######
/*
require_once $GLOBALS['CLASS'].'terbilang.class.php';
$cc = new terbilang;

    $cc->setNumber($rows['total']);
	$terbilang = $cc->printCurrency();
	echo $terbilang;
*/
##########################
?>