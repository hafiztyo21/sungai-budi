function show_modal(p_url,p_option){
	var l_obj_argument = new Object();
    l_obj_argument.url = p_url;
    var l_str_return_value=window.showModalDialog("modal.html",l_obj_argument,p_option);
	return l_str_return_value;
}

function popup(url,windowname4)
{
 params  = 'width='+screen.width;
 params += ', height='+screen.height;
 params += ', top=0, left=0'
 params += ', fullscreen=yes,menubar=yes,scrollbars=yes';

 newwin=window.open(url,windowname4, params);
 if (window.focus) {newwin.focus()}

 return false;
}
