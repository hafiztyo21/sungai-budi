// JavaScript Documentvar xmlHttp

var xmlHttp

function ajaxDo(str,div) {
	if (str.length==0) {
		document.getElementById(div).innerHTML=""
		return
	}
	var xmlHttp=GetXmlHttpObject()
	if (xmlHttp==null) {
		alert ("Browser does not support HTTP Request")
		return
	}
	var url=""
	url=url+str
	
	xmlHttp.onreadystatechange=function() {stateChanged(div,xmlHttp);}
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
}



function stateChanged(div,xmlHttp) {
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete") {
		document.getElementById(div).innerHTML=xmlHttp.responseText
	}
	else {
		document.getElementById(div).innerHTML = "<font color=white style='font-family:Arial, Helvetica, sans-serif; font-size:10px '>Loading Data...</font>";
	}
}




function GetXmlHttpObject()
{
	var xmlHttp=null;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e) {
		// Internet Explorer
		try
		{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) {
			xmlHttp=new
			ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}



// JavaScript Document