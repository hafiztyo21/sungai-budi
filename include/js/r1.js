		/*******  Menu 0 Add-On Settings *******/
		var a = qmad.qm0 = new Object();

		// Item Bullets (CSS - Imageless) Add On
		a.ibcss_apply_to = "parent";			//parent, non-parent, all
		a.ibcss_main_type = "arrow";			//arrow, arrow-head, arrow-gap, arrow-v, arrow-head-v, arrow-gap-v, square, square-inner, square-raised
		a.ibcss_main_direction = "down";		//left, right, down, up
		a.ibcss_main_size = 5;
		a.ibcss_main_bg_color = "#FFF";
		a.ibcss_main_border_color = "#fff";
		a.ibcss_main_position_x = -8;
		a.ibcss_main_position_y = 3;
		a.ibcss_main_align_x = "right";
		a.ibcss_main_align_y = "middle";

		a.ibcss_sub_type = "arrow";			//arrow, arrow-head, arrow-gap, arrow-v, arrow-head-v, arrow-gap-v, square, square-inner, square-raised
		a.ibcss_sub_direction = "right";		//left, right, down, up
		a.ibcss_sub_size = 5;
		a.ibcss_sub_bg_color = "#FFF";
		a.ibcss_sub_border_color = "#fff";
		a.ibcss_sub_position_x = -16;
		a.ibcss_sub_position_y = 4;
		a.ibcss_sub_align_x = "right";
		a.ibcss_sub_align_y = "middle";

		// Rounded Corners Add On
		a.rcorner_size = 3;
		a.rcorner_opacity = "0.7";
		a.rcorner_border_color = "#bd0775";
		a.rcorner_bg_color = "#bd0775 ";
		a.rcorner_apply_corners = new Array(false,false,true,true);
