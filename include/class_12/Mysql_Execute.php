<?PHP
/**
* Mysql DB Connection
* Copyright by Harfiq Elnanda/Vyxell
* 			   08174124217
*              h_el_n@yahoo.com
* Version 1.0.0 Yogyakarta 2006-04-08
**/
require_once("function.php");
class Mysql_Execute{

var $_table;
var $_field;
var $_where;
var $_limit;
var $_offset;
//var $_count;
var $_orderBy;
var $_groupBy;
	function Mysql_Execute($_table,$_where='',$_limit='',$_offset='0',$_orderBy='',$_field='*',$_groupBy='',$_pageName='pageNum'){
		$_index='1';
		$this->_table      = $_table;		
		$this->_field      = $_field;
		$this->_where      = $_where;
		$this->_limit	    = $_limit;
		$this->_groupBy	    = $_groupBy;
		$this->_pageName   = $_pageName;
		//$this->_count	 = $_count;
//		(isset($_GET['pageNum']) ? $this->_offset = $_GET['pageNum'] : $this->_offset = $_offset);
		(isset($_GET[$this->_pageName]) ? $this->_offset = $_GET[$this->_pageName] : $this->_offset = $_offset);
		(isset($_GET['index'])   ? $this->_index  = $_GET['index'] : $this->_index = $_index);
		$this->_orderBy    = $_orderBy;
		(empty($_indexNav) ?  $this->_indexNav = '10' : $this->_indexNav = $_indexNav );
		$this->data        = array();
		//print_r($this->queryString());
		$this->page		   = $this->queryString();	
		//print_r($this->page);		
		$this->currentPage = $GLOBALS['HTTP_SERVER_VARS']["PHP_SELF"];		
	}
	
	function queryString(){	 
	 $queryString = "";
     	if (!empty($GLOBALS['HTTP_SERVER_VARS']['QUERY_STRING'])) {
          $params = explode("&", $GLOBALS['HTTP_SERVER_VARS']['QUERY_STRING']);
          $newParams = array();
          foreach ($params as $param) {
              if (stristr($param, $this->_pageName) == false &&
			      stristr($param, "index")   == false){ 
                  array_push($newParams, $param);  
              } 
	      }
          if (count($newParams) != 0) {
              $queryString = implode("&", $newParams);
          }
     	}
		else if (!empty($GLOBALS['_SERVER']['QUERY_STRING'])) {
		$params = explode("&", $GLOBALS['_SERVER']['QUERY_STRING']);
          $newParams = array();
          foreach ($params as $param) {
              if (stristr($param, $this->_pageName) == false &&
			      stristr($param, "index")   == false){ 
                  array_push($newParams, $param);  
              } 
	      }
          if (count($newParams) != 0) {
              $queryString = implode("&", $newParams);
          }
     	}
	 	$queryString = sprintf("%s", $queryString).(!empty($queryString) ? '&' : '');
		return $queryString;
	}
	
//query execute
	function execute($sql){
// Connecting, selecting database
		$hostname = "localhost";
		$username = "root";
		$password = "pass";
		$database = "unipex";
		if ($db_connect = @mysql_pconnect($hostname, $username, $password)) {
		} else {
			$db_connect = @mysql_connect($hostname, $username, $password);	
		} 
		@mysql_select_db($database, $db_connect) or die(mysql_error());
		$result = mysql_query($sql) or die('Query failed: ' . mysql_error());
		// Closing connection
		return $result;
		mysql_close($db_connect);
	}
	
//function for save data to mysql db
	function save_for_db($_arr_data){
		$key  =''; $value ='';
		foreach($_arr_data AS $k=>$v){
			$key .= (empty($key) ? '' : ',').''.$k.'';
			$value .= (empty($value) ? '' : "," )."'".$v."'";
		}
		$sql =" INSERT INTO ".$this->_table."(".$key.") VALUES (".$value.")";
		//print_r($sql);
		//print_rr($sql);
		$this->sql = $sql; return $this->execute($this->sql); return $this->sql;
	}

//function for update data to mysql db
	function update_for_db($_arr_data,$where){
		$key_value ='';
		foreach($_arr_data AS $k=>$v){
			$key_value .= (empty($key_value) ? '' : ', ').$k." = '".mysql_escape_string($v)."'";
		}
		$sql =" UPDATE ".$this->_table." SET ".$key_value." WHERE ".$where;
		//print_rr($sql);
		$this->sql = $sql; return $this->execute($this->sql); return $this->sql;
	}
	
	function createLink($file, $field){
		$this->lFile = $file;
		$this->lField = $field;
	}

//function for select data from mysql db
	function select_from_db($typ='',$update='',$detail='',$delete='',$add=''){
		$sql  = "SELECT ".$this->_field." FROM ".$this->_table;
		$sql .= ($this->_where ? ' WHERE ' . $this->_where : '');
		//$sql .= ($this->_count ? 'COUNT'.$this->_count:'');
		$sql .= ($this->_groupBy ? ' GROUP BY '.$this->_groupby : '');
		$sql .= ($this->_orderBy ? ' ORDER BY '.$this->_orderBy : '');
		if(!empty($this->_limit) && empty($this->_offset)){
			$sql .= ' LIMIT '.$this->_limit ;
		}elseif(!empty($this->_limit) && !empty($this->_offset)){
			$this->_offset = $this->_offset*$this->_limit;
			$sql .= ' LIMIT '.$this->_offset.','.$this->_limit ;
		}

		$this->sql = $sql; 	$result = $this->execute($sql);
		$i=0;
		while ($line =mysql_fetch_assoc($result)){
			if($typ=='rows'){
				$this->data[$i] = $line;
				if(isset($this->lFile) && !empty($this->lFile) && isset($this->lField) && !empty($this->lField)){
					if($add!='')
						$this->data[$i]['add'] = '<a href="'.$this->lFile.'?add='.$line[$this->lField].'">'.$update.'</a>';
					if($update!='')
						$this->data[$i]['update'] = '<a href="'.$this->lFile.'?update='.$line[$this->lField].'">'.$update.'</a>';					
					if($delete!='')
						$this->data[$i]['delete'] = '<a href="'.$this->lFile.'?delete='.$line[$this->lField].'" >'.$delete.'</a>';
					if($detail!='')
						$this->data[$i]['detail'] = '<a href="'.$this->lFile.'?detail='.$line[$this->lField].'">'.$detail.'</a>';
				}
			}else{
				foreach($line AS $k=>$v){
					$this->data[$k][$i]=$v;				
				}
			}
			$i++;
		}			
		return $this->data;	return $this->sql;
	}
	
//function for delete data from mysql db
	function delete_from_db(){
		$sql  = "DELETE  FROM ".$this->_table;
		$sql .= ($this->_where ? ' WHERE ' . $this->_where : '');
		$this->sql = $sql; 	$result = $this->execute($sql); return $this->sql;
	}
		
//function get one record
	function detail_from_db(){
		$sql  = "SELECT * FROM ".$this->_table." WHERE ". $this->_where ;
		$this->sql = $sql; 	$result = $this->execute($sql);
		while ($line =mysql_fetch_assoc($result)){
			foreach($line AS $k=>$v){
				$this->data[$k]=$v;				
			}
		}			
		return $this->data;	return $this->sql;
   	}
	
/**
 * Copy FROM Kunang.com
 * PostgreSQLAdmin.php
 * Copyright (C) 2005 Wahyu Kurniawan
 *                    +62 - 293- 5508124
 * Version 1.0.0 2005-12-22
**/
//function navigasi
	function navigasi(){
		$pageNow = (isset($_GET[$this->_pageName]) && !empty($_GET[$this->_pageName]) ? $_GET[$this->_pageName] : 0 );
		$sqlnav  = "SELECT COUNT(*) AS total_data FROM ".$this->_table;
		$sqlnav .= ($this->_where ? ' WHERE ' .$this->_where : '');
		$sqlnav .= ($this->_groupBy ? ' GROUP BY '.$this->_groupBy : '');

		$result = mysql_fetch_object($this->execute($sqlnav));
		$this->total_data = $result->total_data;
		

		($this->_limit ? $this->pageNum = ceil($this->total_data/$this->_limit) : $this->pageNum = 1);
		$dataAwal  = ($this->_offset ? $this->_offset+1 : "1");		
		$dataAkhir = ($this->pageNum == 1 ? $this->total_data : min($dataAwal+$this->_limit-1,$this->total_data));
	
		$segNav = $this->_indexNav-1;
		$max_index = ceil ($this->pageNum / $segNav);		
		$this->_index == 1 ? $shmulai = $this->_index : $shmulai = $this->_index+$segNav-1;				
		$shakhir = min($this->pageNum,$shmulai+$segNav); 		  
		  
		if($this->total_data != 0) {
         // buat info
         $this->info  = "Record ".$dataAwal." - ".$dataAkhir." of ".$this->total_data;

         // buat menu
         $this->menu   ='<nobr>';

         if($dataAwal!=1){
            $this->menu .= "<a href=\"?".$this->page.$this->_pageName."=0\">&lt;&lt; First</a>&nbsp;";
            $this->menu .= "<a href=\"?".$this->page.$this->_pageName."=".($pageNow-1)."\">&lt; Prev</a>&nbsp;&nbsp;";
         }
         else{
            $this->menu .= "&lt;&lt; First &lt; Prev &nbsp;";
         }
         
         if($pageNow > 8) $linkAwal = $pageNow - 7;
         else $linkAwal = 0;

         if($this->pageNum > 15) {
            $xx = $linkAwal;
            $yy = $linkAwal+15;
            $jumElement = 0;
            for($banding=$xx;$banding<$this->pageNum;$banding++) {
               $jumElement++;
            }
            if($jumElement<15) {
               $xx=$this->pageNum-15;
            }
         }
         else {
            $xx = 0;
            $yy = $this->pageNum;
         }

//    $finish1 = time();
//    echo $finish1 - $start1 . ' 3<br />';

         $temp = '';
         for ($i=$xx;$i<$yy;$i++){
            if ($pageNow!=$i) {
               $temp .= "<a href=\"?".$this->page.$this->_pageName."=".$i."\">";
            }
            if ($pageNow==$i) {
               $temp .= "<b><big>";
            }
            $temp .= $i+1;
            if ($pageNow==$i) {
               $temp .= "</big></b>";
            }
            if ($pageNow!=$i) {
               $temp .= "</a>";
            }

            $temp .= ' ';
            if ($i==$this->pageNum-1){break;}
         }

         $this->menu .= $temp;
         
         if ($pageNow!=$this->pageNum-1){
            $this->menu .= "&nbsp;&nbsp;<a href=\"?".$this->page.$this->_pageName."=".($pageNow+1)."\">Next &gt;</a>";
            $this->menu .= "&nbsp;<a href=\"?".$this->page.$this->_pageName."=".($this->pageNum-1)."\">Last &gt;&gt;</a>";
         }else{
            $this->menu .= "&nbsp; Next &gt; Last &gt;&gt;";
         }
         
         $this->menu .= '</nobr>';
		
		 }
	}
}

?>
