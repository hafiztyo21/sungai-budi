<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_GET);
#print_r($_POST);

require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('emp_mutasi_add.html');
$tablename = 'tbl_dax_employee';
$tablename_mutasi = 'tbl_dax_mutasi';


if ($_POST['btn_save'])
{


	$sql = "INSERT INTO tbl_dax_mutasi (date,fk_job,fk_department,fk_location,fk_responsible,memo,fk_employee,datecreated,usercreated,fk_medic)
	 		VALUES ('".$_POST['txt_date']."', '".$_POST['txt_job_id']."',
			'".$_POST['txt_department']."','".$_POST['txt_location']."',
			'".$_POST['txt_responsible_to']."','".$_POST['txt_memo']."','".$_GET['id']."',now(),'".$_SESSION[pk_id]."','".$_POST[fk_medic]."')";
			# $data->showsql($sql);
	if ($data->inpQueryReturnBool($sql)){

		$sql_log = "insert into tbl_dax_update_log (date_changed,changed_by,fk_employee,day_date,status,referral,memo)
					values(now(),'".$_SESSION[pk_id]."','".$_POST['txt_pk_id']."','".$_POST['txt_start_date']."','".$_POST['txt_employee_status']."',
					'Employee -> Mutasi Add','job_id:".$_POST['txt_job_id'].",dept_id:".$_POST['txt_department'].",location_id:".$_POST['txt_location'].",shift:".$_POST['txt_shift'].",".$_POST['txt_start_date'].",".$_POST['txt_end_date']."')";
		$data->inpQueryReturnBool($sql_log);
		echo "<script>alert('".$data->err_report('s01')."');window.parent.close();</script>";
	}else{	
		echo "<script>alert('".$data->err_report('s02')."');</script>";	
	}
#print_r($sql);
}

if ($_POST['btn_save_edit'])
{
	
	$sql_save = "INSERT INTO tbl_dax_mutasi (date,fk_job,fk_department,fk_location,fk_responsible,memo,fk_employee,datecreated,usercreated,fk_medic)
	 		VALUES ('".$_POST['txt_date']."', '".$_POST['txt_job_id']."',
			'".$_POST['txt_department']."','".$_POST['txt_location']."',
			'".$_POST['txt_responsible_to']."','".$_POST['txt_memo']."','".$_GET['id']."',now(),'".$_SESSION[pk_id]."','".$_POST[fk_medic]."')";
			# $data->showsql($sql);
	if ($data->inpQueryReturnBool($sql_save)){

		$sql_save_log = "insert into tbl_dax_update_log (date_changed,changed_by,fk_employee,day_date,status,referral,memo)
					values(now(),'".$_SESSION[pk_id]."','".$_POST['txt_pk_id']."','".$_POST['txt_start_date']."','".$_POST['txt_employee_status']."',
					'Employee -> Mutasi Add','job_id:".$_POST['txt_job_id'].",dept_id:".$_POST['txt_department'].",location_id:".$_POST['txt_location'].",shift:".$_POST['txt_shift'].",".$_POST['txt_start_date'].",".$_POST['txt_end_date']."')";
		$data->inpQueryReturnBool($sql_save_log);
		echo "<script>alert('".$data->err_report('s01')."');window.parent.close();</script>";
	}else{	
		echo "<script>alert('".$data->err_report('s02')."');</script>";	
	}
	
	
	$sql_change = "UPDATE ".$tablename." SET 
	fk_job = '".$_POST['txt_job_id']."',fk_department = '".$_POST['txt_department']."'
	,fk_location='".$_POST['txt_location']."'
	,fk_responsible='".$_POST['txt_responsible_to']."',fk_medic='".$_POST[fk_medic]."'
	WHERE pk_id = '".$_GET['id']."'";
    #$data->showsql($sql);
	if ($data->inpQueryReturnBool($sql_change)){

	    $sql_log = "insert into tbl_dax_update_log (date_changed,changed_by,fk_employee,day_date,status,referral,memo)
					values(now(),'".$_SESSION[pk_id]."','".$_GET['id']."','".$_POST['txt_start_date']."','".$_POST['txt_employee_status']."',
					'Employee -> Mutasi Save Changes','job_id:".$_POST['txt_job_id'].",dept_id:".$_POST['txt_department'].",location_id:".$_POST['txt_location'].",shift:".$_POST['txt_shift'].",".$_POST['txt_start_date'].",".$_POST['txt_end_date']."')";
		$data->inpQueryReturnBool($sql_log);
		echo "<script>alert('".$data->err_report('s01')."');window.parent.close();</script>";

	}else{	
		echo "<script>alert('".$data->err_report('s02')."');</script>";	
	}

}



if ($_GET['add'] == 1)
{   #$data->auth('09030101',$_SESSION['user_id']);

	$rows = $data->selectQuery("select * from tbl_dax_employee where pk_id='".$_GET['id']."'");
	$fk_job = $data->get_value("select name from tbl_dax_job where pk_id='".$rows['fk_job']."'");
	$fk_department = $data->get_value("select name from tbl_dax_department where pk_id='".$rows['fk_department']."'");
	$fk_location = $data->get_value("select name from tbl_dax_location where pk_id='".$rows['fk_location']."'");
	$fk_responsible = $data->get_value("select fk_employee from tbl_dax_department_head where pk_id='".$rows['fk_responsible']."'");
	
	
	if($_POST){	
		$date = $_POST[txt_date];
		$job = $_POST[txt_job_id];
		$department = $_POST['txt_department'];
		$location = $_POST[txt_location];
		$responsible = $_POST[txt_responsible_to];
		$memo = $_POST[txt_memo];
		$txt_medic = $_POST[fk_medic];
	}else{
		$date = $_POST[txt_date];
		$job = $rows[fk_job];
		$department = $rows[fk_department];
		$location = $rows[fk_location];
		$responsible = $rows[fk_responsible];
		$memo = $_POST[txt_memo];
		$txt_medic = $rows[fk_medic];
	}
	
	$dataRows = array (
				'TEXT' =>  array('Date Mutasi','Job Position','Department',''),
				'DOT'  => array (':',':',':',':'),
				'FIELD' => array (
				$data->datePicker('txt_date', $_POST[txt_date]),
				$data->cb_dax_job('txt_job_id',$job),
				$data->cb_department('txt_department',$department,''," onchange='document.formaddType.submit();' ")),
				'',


				'TEXT1' =>  array('Location','Responsible To','Memo','Code Medic'),
				'DOT1'  => array (':',':',':',':'),
				'FIELD1' =>  array(
				$data->cb_location('txt_location',$location,''," onchange='document.formaddType.submit();' "),
				$data->cb_headdepartment('txt_responsible_to',$responsible,'',$department,$location),
				'<textarea rows=3 cols=40 name=txt_memo>'.$_POST[txt_memo].'</textarea>',
				$data->cb_golongan_reimburse('fk_medic',$txt_medic)
				),


						  );

    $tittle = "MUTASI EMPLOYEE ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save><input type=submit name=btn_save_edit value='save changes'>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
}

if ($_GET['detail']==1)
{
    $country_name = explode(';',$_POST[txt_country]);
	$country=$country_name[0];

    $citizen_name = explode(';',$_POST[txt_citizen]);
	$citizen=$citizen_name[0];



	
	$rows_mutasi = $data->selectQuery("select tbl_dax_mutasi.*,DATE_FORMAT(tbl_dax_mutasi.date,'%d-%M-%Y')as date_mutasi from tbl_dax_mutasi where fk_employee='".$_GET['id']."' and pk_id='".$_GET['id_mutasi']."'");
	$rows = $data->selectQuery("select * from tbl_dax_employee where pk_id='".$_GET['id']."'");
	$job = $data->get_value("select name from tbl_dax_job where pk_id='".$rows_mutasi['fk_job']."'");
	$department = $data->get_value("select name from tbl_dax_department where pk_id='".$rows_mutasi['fk_department']."'");
	$location = $data->get_value("select name from tbl_dax_location where pk_id='".$rows_mutasi['fk_location']."'");
	$medical = $data->get_value("select code from tbl_dax_golongan where pk_id='".$rows_mutasi['fk_medic']."'");
	#print_r($default_absence);

	
	#print_r($rows[status]);
         $dataRows = array (
				'TEXT' =>  array('Date Mutasi','Job Position','Department',''),
				'DOT'  => array (':',':',':',':'),
				'FIELD' =>array ('&nbsp;'.$rows_mutasi['date_mutasi'],'&nbsp;'.$job,'&nbsp;'.$department),

				'TEXT1' =>  array('Location','Responsible To','Memo','Code Medic'),
				'DOT1'  => array (':',':',':',':'),
				'FIELD1' => array  ('&nbsp;'.$location,'&nbsp;'.$data->responsible_to($rows[fk_responsible]),'&nbsp;'.$rows_mutasi['memo'],'&nbsp;'.$medical),
			);

			$tittle = "MUTASI EMPLOYEE DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}





$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>