<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<title>.: CRM-DAX :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" type="text/css" href="include/css/style.css" />
<link rel="stylesheet" type="text/css" href="include/css/2leveltab.css" />
<script type="text/javascript" src="include/js/2leveltab.js"></script>


<script type="text/javascript" src="jquery-1.2.2.pack.js"></script>

<script type="text/javascript" src="include/js/ddaccordion.js">

/***********************************************
* Accordion Content script- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/

</script>


<script type="text/javascript">


ddaccordion.init({
	headerclass: "expandable", //Shared CSS class name of headers group that are expandable
	contentclass: "categoryitems", //Shared CSS class name of contents group
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false
	defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc]. [] denotes no content
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: true, //persist state of opened contents within browser session?
	toggleclass: ["", "openheader"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["prefix", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed: "normal", //speed of animation: "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isclicked){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})


</script>

<style type="text/css">

.arrowlistmenu{
width: 180px; /*width of accordion menu*/
}

.arrowlistmenu .menuheader{ /*CSS class for menu headers in general (expanding or not!)*/
font: bold 14px Arial;
color: white;
background: black url(titlebar.png) repeat-x center left;
margin-bottom: 5px; /*bottom spacing between header and rest of content*/
text-transform: uppercase;
padding: 2px 0 4px 10px; /*header text is indented 10px*/
cursor: pointer;
cursor: pointer;
}

.arrowlistmenu .openheader{ /*CSS class to apply to expandable header when it's expanded*/
background-image: url(titlebar-active.png);
}

.arrowlistmenu ul{ /*CSS for UL of each sub menu*/
list-style-type: none;
margin: 0;
padding: 0;
margin-bottom: 8px; /*bottom spacing between each UL and rest of content*/
}

.arrowlistmenu ul li{
padding-bottom: 2px; /*bottom spacing between menu items*/
}

.arrowlistmenu ul li a{
	color: #2175bc;
	background: url(arrowbullet.png) no-repeat center left; /*custom bullet list image*/
	display: block;
	padding: 1px 0;
	padding-left: 19px; /*link text is indented 19px*/
	text-decoration: none;
	font-weight: normal;
	border-bottom: 1px solid #dadada;
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
}

.arrowlistmenu ul li a:visited{
color: #2175bc;
}

.arrowlistmenu ul li a:hover{ /*hover state CSS*/
color: #0066CC;
background-color: #F3F3F3;
}

.style1 {color: #2175bc}


</style>
</head>
<frameset rows="140,*" cols="*" frameborder="NO" border="0" framespacing="0">
  
    <frame src="header.php" name="topFrame" style="border-right:1px #FF6600 solid;
	border-left:1px #FF6600 solid;" border scrolling="no" noresize>
 
 <frameset rows="*" cols="5%,*,5%" framespacing="0" frameborder="NO" border="0">
    <frame src="taxi.php" name="leftFrame" scrolling="No">
    <frame src="telemarketer_view.php" name="mainFrame" style="border-right:1px #FF6600 solid;
	border-left:1px #FF6600 solid;" border scrolling="auto" noresize>
	<frame src="taxi.php" name="rightFrame" scrolling="auto">
  </frameset>
</frameset>
<noframes>
<body marginwidth="0" marginheight="0">
</body></noframes>
</html>
