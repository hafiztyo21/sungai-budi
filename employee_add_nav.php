<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'warehouse.class.php';

$data = new warehouse;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>

</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="100%" cellpadding="0" cellspacing="0"><tr>
<td height="31" >
<ul class="activeVid" id="videoList">
<li> <a href="emp_cuti.php?detail=1&id=<?=$_GET[id];?>" target="bottomFrame" class="activeVid" onClick="activeLink(this);">Cuti</a></li>
<!--<li> <a href="emp_ijin.php?detail=1&id=<?=$_GET[id];?>" target="bottomFrame"  onClick="activeLink(this);">Ijin</a></li>-->
<li> <a href="emp_tugas_luar_kantor.php?detail=1&id=<?=$_GET[id];?>" target="bottomFrame"  onClick="activeLink(this);">Tugas Luar Kantor</a></li>
<li> <a href="emp_tugas_luar_kota.php?detail=1&id=<?=$_GET[id];?>" target="bottomFrame"  onClick="activeLink(this);">Tugas Luar Kota</a></li>
<li> <a href="emp_approval_permit.php?detail=1&id=<?=$_GET[id];?>" target="bottomFrame"  onClick="activeLink(this);">Approval Permit</a></li>
<li> <a href="emp_attendance.php?detail=1&id=<?=$_GET[id];?>" target="bottomFrame"  onClick="activeLink(this);">Attendance</a></li>
<li> <a href="emp_surat_peringatan.php?detail=1&id=<?=$_GET[id];?>" target="bottomFrame"  onClick="activeLink(this);">Surat Peringatan</a></li>
<li> <a href="emp_mutasi.php?detail=1&id=<?=$_GET[id];?>" target="bottomFrame"  onClick="activeLink(this);">Mutasi</a></li>

<!-- bagas edit 20121025 -->
<li> <a href="emp_id_card.php?detail=1&id=<?=$_GET[id];?>" target="topFrame" onClick="activeLink(this);">ID Card(KTP, SIM, PASPORT)</a></li>
<li> <a href="emp_akta_lahir.php?detail=1&id=<?=$_GET[id];?>" target="topFrame"  onClick="activeLink(this);">Kutipan Akta Kelahiran</a></li>
<li> <a href="emp_kk.php?detail=1&id=<?=$_GET[id];?>" target="topFrame"  onClick="activeLink(this);">Kartu Keluarga (KK)</a></li>
<li> <a href="emp_akta_nikah.php?detail=1&id=<?=$_GET[id];?>" target="topFrame"  onClick="activeLink(this);">Kutipan Akta Nikah</a></li>
<li> <a href="emp_personal_salary.php?detail=1&id=<?=$_GET[id];?>" target="topFrame"  onClick="activeLink(this);">Personal Salary</a></li>
<!-- bagas edit 20121025 -->

</ul>
</td>
<tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
</tr>
</table>
</body>
</html>