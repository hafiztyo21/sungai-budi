<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('get_report_cuti.html');
if($_POST){
	$date_from = $_POST['txt_date_from'];
	$date_to = $_POST['txt_date_to'];
	$status = $_POST['txt_status'];
}else{
	$date_from = date("Y-m-d");
	$date_to = date("Y-m-d");
	$status = 0;
}
if($_POST['btprint']){



echo "<script>window.location = 'get_report_cuti_print.php?date_from=".$date_from."&date_to=".$date_to."&status=".$status."&name=".$_POST[txt_name]."&id_depart=".$_POST[txt_department]."&id_locat=".$_POST[txt_location]."';</script>";

}

	    $arrFields = array('' => ' - All - ',
			'CB'=>'Cuti Besar (CB)','CT'=>'Cuti Tahunan (CT)','CD'=>'Cuti Dasar (CD)',
			'CH'=>'Cuti Haid (CH)','CH-AC'=>'Cuti Haid - Akumulatif Cuti Tahunan (CH-AC)',
			
				'CM' =>'Cuti Melahirkan (CM)'
			);
		
		
		$dataRows = array (
				'TEXT' =>  array('Date From','Date To','Name','Location','Department','Description'),
				'DOT'  => array (':',':',':'),
				'FIELD' => array ( 
				$data->datePicker('txt_date_from', $_POST['txt_date_from']),
				$data->datePicker('txt_date_to', $_POST['txt_date_to']),
				$data->cb_employee_all_number('txt_name',$_POST[txt_name]),
				$data->cb_location_search('txt_location',$_POST[txt_location]),
				$data->cb_department_search('txt_department',$_POST[txt_department]), 
				$data->cb_select('txt_status',$arrFields,$_POST[txt_status])
				
				) 
			);
			
	   $tittle = "CUTI PRINT";




    $button = array ('SUBMIT' => "<input type='submit' name='btprint' value='Print Cuti'>",
					 'RESET'  => "<input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
#<input type='submit' name='btprint' value='Print Cuti' onclick=\"getCutiPrint('".$date_from."','".$date_to."','".$status."')\">




$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addRows('loopData',$package );
$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>