<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'employee.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'warehouse.class.php';

$data = new employee;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>
</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="100%" cellpadding="0" cellspacing="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<? if($data->auth_boolean(1410,$_SESSION['pk_id'])){  ?>
<li> <a href="holiday.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Holiday</a></li>
 <? } ?>
<? if($data->auth_boolean(1411,$_SESSION['pk_id'])){  ?>
<li> <a href="cuti_massal.php" target="contentTabFrame" onClick="activeLink(this);">Cuti Massal</a></li>
 <? } ?>
<? if($data->auth_boolean(1412,$_SESSION['pk_id'])){  ?>
<li> <a href="rate.php" target="contentTabFrame" onClick="activeLink(this);">Rate</a></li>
 <? } ?>
<? if($data->auth_boolean(1413,$_SESSION['pk_id'])){  ?>
<li> <a href="run_manual.php" target="contentTabFrame" onClick="activeLink(this);">Run Manual</a></li>
 <? } ?>
<? if($data->auth_boolean(1414,$_SESSION['pk_id'])){  ?>
<li> <a href="mailing_system_log.php" target="contentTabFrame" onClick="activeLink(this);">Mailing System Log</a></li>
 <? } ?>
<? if($data->auth_boolean(1416,$_SESSION['pk_id'])){  ?>
<li> <a href="server_log.php" target="contentTabFrame" onClick="activeLink(this);">Server Log</a></li>
 <? } ?>
<? if($data->auth_boolean(1415,$_SESSION['pk_id'])){  ?>
<li> <a href="log_update.php" target="contentTabFrame" onClick="activeLink(this);">Updated Log</a></li>
 <? } ?>
<? if($data->auth_boolean(1417,$_SESSION['pk_id'])){  ?>
<li> <a href="machine_log.php" target="contentTabFrame" onClick="activeLink(this);">Machine Log</a></li>
 <? } ?>
<? if($data->auth_boolean(1415,$_SESSION['pk_id'])){  ?>
<li> <a href="calculate_manual.php" target="contentTabFrame" onClick="activeLink(this);">Calculate AH & HT3</a></li>
 <? } ?>
<? if($data->auth_boolean(1415,$_SESSION['pk_id'])){  ?>
<li> <a href="calculate_manual_ht1.php" target="contentTabFrame" onClick="activeLink(this);">Calculate HT1 & HT2</a></li>
 <? } ?>

<? if($data->auth_boolean(1415,$_SESSION['pk_id'])){  ?>
<li> <a href="payroll_type.php" target="contentTabFrame" onClick="activeLink(this);">Payroll Type</a></li>
 <? } ?>

 <? if($data->auth_boolean(1415,$_SESSION['pk_id'])){  ?>
<li> <a href="calculate_absance.php" target="contentTabFrame" onClick="activeLink(this);">Calculate Absance</a></li>
 <? } ?>

</ul>
</td>
  <tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
</tr>
</table>
</body>
</html>