<?php
session_start();
#print_r($_SESSION['user_id']);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'chart.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new chart;
$data->auth('031102',$_SESSION['user_id']);

$data->ResultsPerPage = 10;
$data->LinksPerPage = 10;


$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('sales_report_customer.html');

if ($_POST['search'] )
{

	$date_from = $_POST['date_from'];
	$date_to = $_POST['date_to'];

	$printLink='sales_report_customerPrint.php';
	$printLink = "<img src='image/print.gif' height='12' width='12'><a href='#' onclick=openWin1('".$printLink."?print=1&date_from=$date_from&date_to=$date_to')>".$data->lang('print')."</a>";

	if($data->greaterDate($date_from, $date_to))
	{
		$date_from = $_POST['date_to'];
		$date_to = $_POST['date_from'];
	}

	if ($date_from == $date_to)
	{	$periode = "PERIODE: ".date('d M Y', strtotime($date_from));		}
	else
	{	$periode = "PERIODE: ".date('d M Y', strtotime($date_from)).' - '.date('d M Y', strtotime($date_to));	}

	/*$sql = "SELECT customer_id, customer FROM order_list
			WHERE date_faktur BETWEEN '".$date_from."' AND '".$date_to."' AND faktur_status = '2'
			ORDER BY customer_id";*/


	$sql = "SELECT order_list.customer_id, order_list.customer, item_list.item_id, item_list.item_name, sum(sales_order_detail.quantity) as quantity,
				   sum(sales_order_detail.amount) as amount,
				   sum(sales_order_detail.quantity * sales_order_detail.price) as gross_total,
				   
				   sum(sales_order_detail.amount + (  
				   (select distinct tax_percent  from so_faktur_list where order_id=sales_order_detail.order_id) 
				   * sales_order_detail.amount / 100)) as net_total,
				   
				   sum(sales_order_detail.quantity * sales_order_detail.price - sales_order_detail.amount) as total_disc
	 		 FROM order_list, sales_order_detail, item_list
			 WHERE order_list.order_id = sales_order_detail.order_id AND
			 	   order_list.rev = sales_order_detail.rev AND
				   sales_order_detail.item_id = item_list.item_id AND
			 	   order_list.date_faktur BETWEEN '".$date_from."' AND '".$date_to."' AND
			 	   order_list.faktur_status = '2'
		     GROUP BY order_list.customer_id, order_list.customer, item_list.item_id, item_list.item_name
	 		 ORDER BY order_list.customer_id, item_list.item_id";
			 
			 
			 $sql = "SELECT so_faktur_list.customer_id, so_faktur_list.customer, item_list.item_id, item_list.item_name, sum(sales_order_detail.quantity) as quantity,
				   sum(sales_order_detail.amount) as amount,
				   sum(sales_order_detail.quantity * sales_order_detail.price) as gross_total,
				   
				   sum(sales_order_detail.amount + (  so_faktur_list.tax_percent   * sales_order_detail.amount / 100)) as net_total,
				   
				   sum(sales_order_detail.quantity * sales_order_detail.price - sales_order_detail.amount) as total_disc
	 		 FROM so_faktur_list, sales_order_detail, item_list
			 WHERE so_faktur_list.order_id = sales_order_detail.order_id AND
			 	   so_faktur_list.rev = sales_order_detail.rev AND
				   sales_order_detail.item_id = item_list.item_id AND
			 	   so_faktur_list.date_faktur BETWEEN '".$date_from."' AND '".$date_to."' AND
			 	   so_faktur_list.status = '2'
		     GROUP BY so_faktur_list.customer_id, so_faktur_list.customer, item_list.item_id, item_list.item_name
	 		 ORDER BY so_faktur_list.customer_id, item_list.item_id";

	//echo $sql;

    $_SESSION['sql'] = $sql;
	$_SESSION['periode'] = $periode;
	$_SESSION['date_from'] = $date_from;
	$_SESSION['date_akhir'] = $date_to;
}
else if (($_SESSION['sql']) and ($_GET))
{
	$sql = $_SESSION['sql'];
	$periode = $_SESSION['periode'];
	$date_from = $_SESSION['date_from'];
	$date_to = $_SESSION['date_to'];
}
else
{
	$_SESSION['sql'] = '';
	$_SESSION['periode'] = '';
	$_SESSION['date_from'] = '';
	$_SESSION['date_to'] = '';
	$sql = '';
}

#echo $sql;
$date_temp = '';

if (!empty($sql))
{
	$pg = ($_POST['search'] )? 1 : $_GET['page'];
	$DG = $data->dataGridBase($sql, $data->ResultsPerPage, $pg);

	if (!empty($DG))
	{
		$total_type = array('quantity' => 0,
							'gross_total' => 0,
							'total_disc' => 0,
							'amount' => 0,
							'net_total' => 0
							);
		$type_temp = '';

		foreach ($DG as $w => $det)
		{
			$bgcolor = (($w % 2) == 1)? '#e6f0fc': '#ffffff';

			if ($w > 0 && $type_temp != $det['customer_id'])
			{
				$total_type['quantity'] = $det['quantity'];
				$total_type['gross_total'] = $det['gross_total'];
				$total_type['total_disc'] = $det['total_disc'];
				$total_type['amount'] = $det['amount'];
				$total_type['net_total'] = $det['net_total'];
			}
			else
			{
				$total_type['quantity'] += $det['quantity'];
				$total_type['gross_total'] += $det['gross_total'];
				$total_type['total_disc'] += $det['total_disc'];
				$total_type['amount'] += $det['amount'];
				$total_type['net_total'] += $det['net_total'];
			}

			$type_temp = $det['customer_id'];

			$list[]['TABLE_LIST'] .=
						"<tr bgcolor='".$bgcolor."'>
							 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;' align='center'>".($w+1)."</td>
							 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['customer_id']."</td>
            				 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['customer']."</td>
          					 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['item_id']."</td>
            				 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['item_name']."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['quantity'], '', 0)."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['gross_total'], '')."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['total_disc'], '')."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['amount'], '')."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['net_total'], '')."</td>
						 </tr>";

			if ($det['customer_id'] != $DG[$w+1]['customer_id'])
			{
				$list[]['TABLE_LIST'] .=
					"<tr class='tr_footer'>
          				<td style='padding:2;' colspan='5' align='right'>".ucwords(strtolower($data->lang('total_customer')))."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['quantity'], '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['gross_total'], '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['total_disc'], '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['amount'], '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['net_total'], '')."</td>
					</tr>";
			}
		}

		$list[]['TABLE_LIST'] .=
				"<tr class='tr_footer'>
          				<td style='padding:2;' colspan='5' align='right'>Total</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'quantity'), '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'gross_total'), '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'total_disc'), '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'amount'), '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'net_total'), '')."</td>
				</tr>";
	}
}

#echo '<pre>';
//print_r();
#echo '</pre>';

$header = array ('CUSTOMER_ID'=> strtoupper($data->lang('cust_id')),
				 'CUSTOMER' => strtoupper($data->lang('cust_name')),
				 'ITEM_ID'=> strtoupper($data->lang('item_id')),
				 'ITEM_NAME' => strtoupper($data->lang('item_name')),
				 'QTY'=> strtoupper($data->lang('jumlah')),
				 'AMOUNT' => strtoupper($data->lang('total_price')),
				 'DISCOUNT' => strtoupper($data->lang('disc')),
				 'DPP' => strtoupper($data->lang('dpp')),
				 'NETTO'=> strtoupper($data->lang('net_total'))
				 );

$date = array ('from' => $data->lang('from'), 'to' => $data->lang('to'),'SEARCH' => $data->lang('search'));

$top = array ('TITLE' => strtoupper($data->lang('sales_report_customer')));

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);


$include = array
		(
				'INCLUDEAJAXJS' => $GLOBALS['JS'].'ajax.js',
				'INCLUDEJSVALJS' => $GLOBALS['JS'].'jsval.js'
		);


$tmpl->addRows('list', $list);
$tmpl->addVar('page', 'PRINT',$printLink);
$tmpl->addVars('header',$header );
$tmpl->addVars('top',$top );
$tmpl->addVars('date',$date );
$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVar('date', 'DATE_TO', $data->datePicker('date_to', $date_to));
$tmpl->addVar('page', 'PERIODE', $periode);
//$tmpl->addVars('button',$button);
//$tmpl->addVar('paging', 'paging_no',$paging_no);
//$tmpl->addVar('legend', 'page',$page_info);
//$tmpl->addVar('legend', 'result',$result_info);
//$tmpl->addVar('search', 'search',$searchCB);
$tmpl->addRows('row',$DG );
$tmpl->addVars('path',$path);
$tmpl->addVars('include',$include);
$tmpl->displayParsedTemplate('page');

?>