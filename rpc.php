<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'sales.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new sales;

?>


<?php
	$yCur = 0;
	$yCur=date("Y");
	$month = $_POST['month'];
	$year = $_POST['year'];
	#$yDelta = $yCur + 2;
	#print_r($yCur+2);
	$action = $_POST['action'];
	switch($action) {
	
	case 'startCalendar':
		$month = $_POST['month'];
		$year = $_POST['year'];
		
		if(($month == 0) || ($year == 0)) {
			$thisDate = mktime(0, 0, 0, date("m"), date("d"), date("Y"));//this date
		} else {
			$thisDate = mktime(0, 0, 0, $month, 1, $year);
		}
		
		echo '<div style="margin-bottom: 3px;">
					<form name="changeCalendarDate">
						<select id="ccMonth" onChange="startCalendar($F(\'ccMonth\'), $F(\'ccYear\'))">';
						
						for($i=1; $i<=12; $i++)
						{
							$monthMaker = mktime(0, 0, 0, $i, 1, 2006);
							if($month > 0) {
								if($month == $i) {
									$sel = 'selected';
								} else {
									$sel = '';
								}
							} else {
								if(date("m", $thisDate) == $i) {
									$sel = 'selected';
								} else {
									$sel = '';
								}
							}

							echo '<option value="'. $i .'" '. $sel .'>'. date("F", $monthMaker) .'</option>';
						}
						
				echo '</select>
						&nbsp;
						<select id="ccYear" onChange="startCalendar($F(\'ccMonth\'), $F(\'ccYear\'))">';
						
						$yStart = $yCur - 2;
						$yEnd = ($yCur + 2);
						for($i=$yStart; $i<$yEnd; $i++)
						{
							if($year > 0) {
								if($year == $i) {
									$sel = 'selected';
								} else {
									$sel = '';
								}
							} else {
								if(date("Y", $thisDate) == $i) {
									$sel = 'selected';
								} else {
									$sel = '';
								}
							}
							echo '<option value="'. $i .'" '. $sel .'>'. $i .'</option>';
							
						}
						
				echo '</select>
				
					</form>
				</div>';
				//print_r($month);
		$monthkey=$month;
		if($monthkey == 0){ $monthkey=date("m");};
		$monthMakerview = mktime(0, 0, 0, $monthkey, 1, 2006);	
		$monthview = 	date("F", $monthMakerview);
		
		$yearview=$year;
		if ($yearview==0){ $yearview = date("Y");  };
		
		echo '<div align=right> <font size=6 color="#1b4887">'.$monthview.' '.$yearview.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></div>';
		// Display the week days.
		echo '<div class="calendarFloat" style="text-align: center; background-color: #999999"><span style="position: relative; top: 4px;">Mon</span></div>
				<div class="calendarFloat" style="text-align: center; background-color: #f0f2ff;"><span style="position: relative; top: 4px;">Tue</span></div>
				<div class="calendarFloat" style="text-align: center; background-color: #f0f2ff;"><span style="position: relative; top: 4px;">Wed</span></div>
				<div class="calendarFloat" style="text-align: center; background-color: #f0f2ff;"><span style="position: relative; top: 4px;">Thur</span></div>
				<div class="calendarFloat" style="text-align: center; background-color: #f0f2ff;"><span style="position: relative; top: 4px;">Fri</span></div>
				<div class="calendarFloat" style="text-align: center; background-color:#f0f2ff;"><span style="position: relative; top: 4px;">Sat</span></div>
				<div class="calendarFloat" style="text-align: center; background-color: #1b4887;"><span style="position: relative; top: 4px;">Sun</span></div>';
				
		// Show the calendar.
		for($i=0; $i<date("t", $thisDate); $i++)
		{
			$thisDay = ($i + 1);
			if(($month == 0) || ($year == 0)) {
				$finalDate = mktime(0, 0, 0, date("m"), $thisDay, date("Y"));
				$today = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
				$fdf = mktime(0, 0, 0, date("m"), 1, date("Y"));
				$month = date("m");
				$year = date("Y");
			} else {
				$finalDate = mktime(0, 0, 0, $month, $thisDay, $year);
				$fdf = mktime(0, 0, 0, $month, 1, $year);
			}
			
			
			// Skip some cells to take into account for the weekdays.
			if($i == 0) {
				$firstDay = date("w", $fdf);
				$skip = ($firstDay - 1);
				if($skip < 0) { $skip = 6; }
				
				for($s=0; $s<$skip; $s++)
				{
					if ($s==0) {
						echo '<div class="calendarFloat"  style="background-color:#999999;border : 1px solid #FFF;"></div>';
					}else{
						echo '<div class="calendarFloat" style="border" : 1px solid #FFF;"></div>';
					}
				}
			}
							
			// Make the weekends a darker colour.
			if((date("w", $finalDate) == 0) || (date("w", $finalDate) == 7)) 
			{
				$fontcolor='#bababa';
				$bgColor = '#1b4887';
			} 
				else if (date("w", $finalDate) == 1)
			{
				$bgColor = '#999999';
			}
				else  
			{
				$bgColor = '#f0f2ff';
			}
			
			
			
			
			
			
			//<span style="position: relative; top: '. $tTop .'; left: 1px;">'. $thisDay .'</span>
			
			// Display the day.
			#get pk-id
			if($_SESSION['jobid']=='ADM'){
				$sql1 = "select  count(*) as total  from tbl_dax_customer ,tbl_dax_booking
				 where  tbl_dax_customer.telemarketer_booking_id=tbl_dax_booking.pk_id
				 And
				 tbl_dax_booking.date_sign_in=date_format('$year-$month-$thisDay','%Y-%m-%d')
				 and (tbl_dax_customer.status=5 or tbl_dax_customer.status=6)
				 and date_format('".$year."-".$month."-".$thisDay."','%Y-%m-%d') >= CURRENT_DATE()";
				$sql1 = "select  count(*) as total  from tbl_dax_customer ,tbl_dax_booking
				 where  tbl_dax_customer.telemarketer_booking_id=tbl_dax_booking.pk_id
				 And
				 tbl_dax_booking.date_sign_in=date_format('$year-$month-$thisDay','%Y-%m-%d')
				 and (tbl_dax_customer.status=5 or tbl_dax_customer.status=6)";
				 }
				 else
				 {
					 $sql1 = "select  count(*) as total  from tbl_dax_customer ,tbl_dax_booking
					 where  tbl_dax_customer.telemarketer_booking_id=tbl_dax_booking.pk_id
					 And
					 tbl_dax_booking.date_sign_in=date_format('$year-$month-$thisDay','%Y-%m-%d')
					 and (tbl_dax_customer.status=5 or tbl_dax_customer.status=6)
					 and date_format('".$year."-".$month."-".$thisDay."','%Y-%m-%d') >= CURRENT_DATE()and outlet_code='".$_SESSION['outletcode']."'";
					$sql1 = "select  count(*) as total  from tbl_dax_customer ,tbl_dax_booking
					 where  tbl_dax_customer.telemarketer_booking_id=tbl_dax_booking.pk_id
					 And
					 tbl_dax_booking.date_sign_in=date_format('$year-$month-$thisDay','%Y-%m-%d')
					 and (tbl_dax_customer.status=5 or tbl_dax_customer.status=6)and outlet_code='".$_SESSION['outletcode']."'";
				 }
				 
				
			#get customer
			$customer1 = $data->get_value($sql1);
			if ($customer1==0){
				$cs = '';
				echo '<div class="calendarFloat" id="calendarDay_'. $thisDay .'" style="background-color: '. $bgColor .'; cursor: pointer;" 
									onMouseOver="highlightCalendarCell(\'calendarDay_'. $thisDay .'\')"
									onMouseOut="resetCalendarCell(\'calendarDay_'. $thisDay .'\')">
						<span style="position: relative; top: '. $tTop .'; left: 1px;">'. $thisDay .'</span></div>';
			
			}else{
				$cs = $customer1;
				
				echo '<div class="calendarFloat" id="calendarDay_'. $thisDay .'" style="background-color: '. $bgColor .'; cursor: pointer;" 
									onMouseOver="highlightCalendarCell(\'calendarDay_'. $thisDay .'\')"
									onMouseOut="resetCalendarCell(\'calendarDay_'. $thisDay .'\')">
						<span style="position: relative; top: '. $tTop .'; left: 1px;">'. $thisDay .'</span><br><span><font size="1">[ '.$cs.' ] <a href="admin_sales_detail.php?id_date='.$year.'-'.$month.'-'.$thisDay.'">Customer Request</a></font><span></div>';
			
			} 
	
				
			
				
		}
		
			#print_r($_SESSION);
		
		break;
		
		default:
		
			echo 'Whoops.';
			break;
	}

?>