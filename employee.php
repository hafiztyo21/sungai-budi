<?php
session_start();
#session_destroy();
#print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'employee.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new employee;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('employee.html');

if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='full_name';
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

if($_GET['del']==1){

	$sql = "update tbl_dax_employee set status=0 where pk_id='".$_GET['id']."' ";
 	$row = $data->get_row("select * from tbl_dax_employee where pk_id='".$_GET['id']."' ");
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{
 		$sql_log = "insert into tbl_dax_update_log (date_changed,changed_by,fk_employee,day_date,status,referral,memo)
					values(now(),'".$_SESSION[pk_id]."','".$_GET['id']."','".$row[start_date]."','".$row['status']."',
					'Employee -> Delete','!Warning')";
		$data->inpQueryReturnBool($sql_log);
		echo "<script>alert('".$data->msgbox('d01')."');window.parent.close();</script>";

	}
	else
	{	echo "<script>alert('".$data->msgbox('d02')."');</script>";	}
}




$link = 'employee_add.php';
$linkAttach = 'signature_attach.php';

if($data->auth_boolean(131010,$_SESSION['pk_id'])){
//print_r ($kode);


$addLink = "<a href='employee.php' onclick=popup('".$link."?add=1','Employee')>ADD</a>";
//$addLink = "<a href='user.php' onclick=shows_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
$tmpl->addVar('page','add',$addLink);
}

if ($_SESSION['jobid']!='ADM'){
		//$filter = " and  tbl_dax_employee.outlet_id='".$_SESSION['outletcode']."'";
	}

if ($_SESSION['pajak'] =='P'){
		$filter = " tbl_dax_employee.tax_status='".$_SESSION['pajak']."'";
	}else{
		$filter = " tbl_dax_employee.tax_status in ('P','NP','')";
	}

if ($_POST['btn_search'] )
{
#print_r($_POST);
	if($_POST[txt_status]=='-1'){
		$filter_status = "";
	}else{
		$filter_status = " and  tbl_dax_employee.status= '".$_POST[txt_status]."' ";
	}
	if($_POST[txt_department]=='0'){
		$filter_department = "";
	}else{
		$filter_department = " and  tbl_dax_employee.fk_department='".$_POST[txt_department]."'  ";
	}
	if($_POST[txt_location]=='0'){
		$filter_location = "";
	}else{
		$filter_location = " and  tbl_dax_employee.fk_location='".$_POST[txt_location]."'  ";
	}

	if($_POST[txt_job]=='-1'){
		$filter_job = "";
	}else{
		$filter_job = " and  tbl_dax_employee.fk_job='".$_POST[txt_job]."'  ";
	}

	if($_POST[txt_kelamin]=='-1'){
		$filter_kelamin = "";
	}else{
		$filter_kelamin = " and  tbl_dax_employee.sex='".$_POST[txt_kelamin]."'  ";
	}

	if(($_POST[txt_name]=='0') || ($_POST[txt_name]=='')){
		$filter_name = "";
	}else{
		$filter_name = " and tbl_dax_employee.pk_id = '".$_POST[txt_name]."'  ";
	}
	if($_POST[txt_id_emp]==''){
		$filter_id_emp = "";
	}else{
		$filter_id_emp = " and tbl_dax_employee.pk_id ='".$_POST[txt_id_emp]."'  ";
	}
	if($_POST[txt_religion]=='-1'){
		$filter_religion = "";
	}else{
		$filter_religion = " and tbl_dax_employee.fk_religion ='".$_POST[txt_religion]."'  ";
	}

	$sql = "SELECT tbl_dax_employee.pk_id,tbl_dax_employee.sex,tbl_dax_employee.full_name,tbl_dax_employee.birth_date,tbl_dax_employee.marital_status,tbl_dax_employee.jumlah_anak,tbl_dax_employee.email,tbl_dax_employee.fk_religion,tbl_dax_location.name as location,tbl_dax_employee_status.description as emp_status,
			tbl_dax_employee.nickname ,tbl_dax_job.name as job, tbl_dax_department.name as department,
			tbl_dax_golongan.code
			FROM tbl_dax_employee
			LEFT JOIN tbl_dax_job on tbl_dax_employee.fk_job = tbl_dax_job.pk_id
			LEFT JOIN tbl_dax_department on tbl_dax_employee.fk_department = tbl_dax_department.pk_id
			LEFT JOIN tbl_dax_location on tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			LEFT JOIN tbl_dax_employee_status on tbl_dax_employee.status = tbl_dax_employee_status.pk_id
			left join tbl_dax_golongan on tbl_dax_employee.fk_medic = tbl_dax_golongan.pk_id
			where
			1 $filter_name $filter_status  $filter_department $filter_location $filter_job $filter_kelamin
			$filter_id_emp $filter_religion
			 order by $order_by $sort_order " ;
#$data->showsql($sql);
	$_SESSION['sql']=$sql;
}else if (($_SESSION['sql']) and ($_GET)){
    $sql = $_SESSION['sql'];
}else{
	$_SESSION['sql']='';
	$sql = "SELECT tbl_dax_employee.pk_id,tbl_dax_employee.full_name,tbl_dax_employee.birth_date,tbl_dax_employee.marital_status,tbl_dax_employee.jumlah_anak,tbl_dax_employee.email,tbl_dax_location.name as location,tbl_dax_employee_status.description as emp_status,
			tbl_dax_employee.nickname ,tbl_dax_job.name as job, tbl_dax_department.name as department,
			tbl_dax_golongan.code
			FROM tbl_dax_employee
			LEFT JOIN tbl_dax_job on tbl_dax_employee.fk_job = tbl_dax_job.pk_id
			LEFT JOIN tbl_dax_department on tbl_dax_employee.fk_department = tbl_dax_department.pk_id
			LEFT JOIN tbl_dax_location on tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			LEFT JOIN tbl_dax_employee_status on tbl_dax_employee.status = tbl_dax_employee_status.pk_id
			left join tbl_dax_golongan on tbl_dax_employee.fk_medic = tbl_dax_golongan.pk_id
			where
			$filter and tbl_dax_employee.status in(0,1,2)
	 		order by $order_by $sort_order";


}
#print_r($sql);
$arrFields = array(
		'full_name '=>'FULL NAME',
		'nickname '=>'NICKNAME',
		'tbl_dax_job.name'=>'JOB POSSITION',
		'tbl_dax_department.name'=>'DEPARTMENT',
		'tbl_dax_location.name'=>'LOCATION'
);
#print_r($_GET);

$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridEmployee($sql,'pk_id','first_name',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link,'signature',$linkAttach);
  #print_r ($DG);
#$data->listData();

$print = "<input type='button' name='btprint' value='Print Pegawai' onclick=\"EmployeePrint('".$_POST[txt_name]."','".$_POST[txt_department]."','".$_POST[txt_location]."','".$_POST[txt_job]."','".$_POST[txt_kelamin]."','".$_POST[txt_status]."','".$_POST[txt_id_emp]."','".$_POST[txt_religion]."')\">";



#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         #$paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		 $paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);

#$tmpl->addVar('page', 'name',"<input type='text' name='txt_name' value='".$_POST[txt_name]."'>");
$tmpl->addVar('page','name',$data->cb_employee_all_number('txt_name',$_POST[txt_name]));

#$tmpl->addVar('page','txt_id_employee',$data->cb_employee_id_number('txt_id_emp',$_POST[txt_id_emp]));
$tmpl->addVar('page','txt_id_employee',"<input type='text' name='txt_id_emp' value='".$_POST[txt_id_emp]."'>");

$tmpl->addVar('page', 'department',$data->cb_department_search('txt_department',$_POST[txt_department]));
$tmpl->addVar('page', 'location',$data->cb_location_search('txt_location',$_POST[txt_location]));
$tmpl->addVar('page', 'job',$data->cb_dax_job('txt_job',$_POST[txt_job]));

$tmpl->addVar('page', 'religion',$data->cb_religion('txt_religion',$_POST[txt_religion]));

$tmpl->addVar('page', 'kelamin',$data->cb_sex('txt_kelamin',$_POST[txt_kelamin]));



$arrStatus = array ('-1' => '- All -', '0' => 'Not Active' , '1' => 'Active','2' => 'Mutasi');
$_POST[txt_status] = ($_POST[txt_status]=='')? '-1' : $_POST[txt_status];
$tmpl->addVar('page', 'status',$data->cb_select('txt_status',$arrStatus,$_POST[txt_status]));

$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('page', 'total','Total : '.$InfoArray["TOTAL_RESULTS"]);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);
$tmpl->addVar('page', 'print',$print);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>