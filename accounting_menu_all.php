<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'payment_cash.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new payment_cash;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>

</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="100%" cellspacing="0" cellpadding="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<? if($data->auth_boolean(1710,$_SESSION['pk_id'])){  ?>
<li> <a href="payment_info.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Payment</a></li>
<? } ?>
<? if($data->auth_boolean(1711,$_SESSION['pk_id'])){  ?>
<li> <a href="cancel_payment.php" target="contentTabFrame" onClick="activeLink(this);">Cancel Payment</a></li>
<? } ?>
<? if($data->auth_boolean(1712,$_SESSION['pk_id'])){  ?>
<li> <a href="refund.php" target="contentTabFrame" onClick="activeLink(this);">Refund</a></li>
<? } ?>
<? if($data->auth_boolean(1713,$_SESSION['pk_id'])){  ?>
<li> <a href="next_payment.php" target="contentTabFrame" onClick="activeLink(this);">Next Payment</a></li>
<? } ?>
<? if($data->auth_boolean(1714,$_SESSION['pk_id'])){  ?>
<li> <a href="pattycash.php" target="contentTabFrame" onClick="activeLink(this);">Pettycash</a></li>
<? } ?>
</ul>
</td>
</tr>
  <tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
</table>
</body>
</html>