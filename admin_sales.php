<?php
session_start();
$_SESSION['sql']='';
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'sales.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Calendar System </title>

<script src="jscalendar/lib/prototype.js" type="text/javascript"></script>
<script src="jscalendar/src/scriptaculous.js" type="text/javascript"></script>

<style>
	.calendarBox {
		position: relative;
		top: 10px;
		margin: 20px;
		padding: 10px;
		width: 590px;
		border: 1px solid #bababa;
	}

	.calendarFloat {
		float: left;
		width: 75px;
		height: 55px;
		margin: 1px 0px 0px 1px;
		padding: 1px;
		border: 1px solid #bababa;
			
	}
</style>

<script type="text/javascript">
	function highlightCalendarCell(element) {
		$(element).style.border = '1px solid #1b4887';
	}

	function resetCalendarCell(element, color) {
		$(element).style.border = '1px solid #bababa';
	}
	
	function startCalendar(month, year) {
		new Ajax.Updater('calendarInternal', 'rpc.php', {method: 'post', postBody: 'action=startCalendar&month='+month+'&year='+year+''});
	}
</script>


</head>
<body>

	<div id="calendar" class="calendarBox">
		<div id="calendarInternal">
		
		</div>
		<br style="clear: both;">
	</div>

	
	<script type="text/javascript">
		startCalendar(0,0);
	</script>

</body>
</html>