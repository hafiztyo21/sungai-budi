<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'employee.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new employee;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('get_sales.html');

$txt_outlet = $_GET['outlet_id'];

if ($_POST['add_user']){
	$order_by=$_POST['order_by'];
}

if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='pk_id';
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);


//print_r ($kode);
$link = 'employee_add.php';
$addLink = "<a href='employee.php' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:600px;dialogHeight:400px')>ADD</a>";
//$addLink = "<a href='user.php' onclick=show_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";

if ($_POST['btn_search'] )
{
	$id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];

	$sql = "SELECT tbl_dax_employee.*,tbl_dax_job.name as position, tbl_dax_outlet.name as name_outlet FROM tbl_dax_employee
	left join tbl_dax_job 
	on
	tbl_dax_employee.job_id = tbl_dax_job.CODE
	left join tbl_dax_outlet
	on
	tbl_dax_employee.outlet_id = tbl_dax_outlet.code
	where 
	tbl_dax_employee.job_id='SLS'
	and outlet_id='".$txt_outlet."'
    and tbl_dax_employee.status=1 and upper($id_serach) like upper('%$q_serach%')" ;

	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else
{
	$_SESSION['sql']='';
	$sql = "SELECT tbl_dax_employee.*,tbl_dax_job.name as position, tbl_dax_outlet.name as name_outlet FROM tbl_dax_employee
	left join tbl_dax_job 
	on
	tbl_dax_employee.job_id = tbl_dax_job.CODE
	left join tbl_dax_outlet
	on
	tbl_dax_employee.outlet_id = tbl_dax_outlet.code
	where 
	tbl_dax_employee.job_id='SLS'
	and outlet_id='".$txt_outlet."'
    and tbl_dax_employee.status=1
	order by $order_by $sort_order";
	#$data->showsql($sql);
}
#$data->showsql($sql);

$arrFields = array(
		'id_card'=>'ID CARD',
		'first_name '=>'FIRST NAME',
		'last_name '=>'LAST NAME',
);
#print_r($_GET);
#print_r($sql);
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridEmployee($sql,'pk_id','first_name',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link);
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);

$tmpl->addVar('page','submit',"<input type=\"button\" name=\"btnsubmit\" value='Add' onClick=\"fgetresult()\">");
$tmpl->addVar('page','cancel',"<input type=\"button\" name=\"btnclear\" value='None' onClick=\"fGetNone()\">");

$tmpl->addVar('page','salke',$_GET['s']);

$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>