<?php
session_start();

#session_destroy();
//print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'customer.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new customer;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('user_customer_add.html');
$tablename = 'tbl_dax_customer';

#print_r($_GET);
$_GET[page] = ($_GET[page]=='' ? 1 : $_GET[page]);

#####################################sorting##############################


$order_by = $data->order_by('date_input');
$sort_order = $data->sort_order('asc');


$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################


$link = "user_customer_add.php";
$link_parameter="page=".$_GET[page]."&sort_order_p=$sort_order&order_by=$order_by";
$link_page = "sort_order_p=$sort_order&order_by=$order_by";

if(($_GET[mode]=='marketing') || ($_GET[mode]=='add_customer') || ($_GET[mode]=='edit_customer')){
	$backLink = "";

}else{
	$backLink = "<a href='#' onClick=\"window.parent.window.parent.location = 'customer_all.php?mode=back&page=".$_GET[page]."&sort_order=$sort_order&order_by=$order_by';\">BACK</a>";

}

#$backLink = "<a href='#' onClick=\"window.parent.window.parent.location = 'customer_all.php?mode=back&page=".$_GET[page]."&sort_order=$sort_order&order_by=$order_by';\">BACK</a>";
	
	$tmpl->addVar('page','back',$backLink);




if ($_POST['btn_save'])
{

   $flag = true;
			try {
					#if (!mysql_query("BEGIN"))  {
					if (!$data->begin())  {
					
					throw new Exception($data->err_report('beginTrans_failed'));

					}
						
						
					if($_POST['txt_status']==5){
						
						$date_call = 'now()';
						$sql = "INSERT INTO tbl_dax_booking (fk_customer,date_booking, outlet, tm_code, source_code,date_sign_in,fk_reservation,fk_tm_manager) VALUES ('".$_POST['txt_national_id']."',now(),'".$_SESSION['outletcode']."','".$_POST['txt_telemarketer_id']."','".$_POST['txt_source_code']."','".$_POST['txt_dateday']."','".$_POST['reservasi_pkid']."','".$_POST['txt_telemarketer_manager']."')";
							#$data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
										{	throw new Exception($data->err_report('s02'));	}
						$tm_jobid = $data->get_value("select job_id from tbl_dax_employee where pk_id='".$_POST['txt_telemarketer_id']."'");
						$sql= "INSERT INTO tbl_dax_log (employee_id ,job_id ,status ,customer_id ,date_create,fk_tm_manager,fk_sales_manager)VALUES ('".$_POST['txt_telemarketer_id']."', '".$tm_jobid."', '5', '".$_POST['txt_national_id']."',now(),'".$_POST['txt_telemarketer_manager']."','".$_POST['txt_sales_manager']."')";
							#$data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
							{	throw new Exception($data->err_report('s02'));	
							#mysql_query("ROLLBACK");
							}		
						
						$booking_id=$data->maxval('tbl_dax_booking','pk_id');
						
				
					}
						
					if($_POST['txt_status']==9) {
										
					$date_call = 'now()';
					$agreement_id = $data->next_agreement_id();
					$member_id = $data->member_id($agreement_id);
										
						
							if($_POST['txt_telemarketer_id']){
								$sql = "INSERT INTO tbl_dax_booking (fk_customer,date_booking, outlet, tm_code, source_code,date_sign_in,fk_reservation,fk_tm_manager) VALUES ('".$_POST['txt_national_id']."',now(),'".$_SESSION['outletcode']."','".$_POST['txt_telemarketer_id']."','".$_POST['txt_source_code']."','".$_POST['txt_agreement_date']."','".$_POST['reservasi_pkid']."','".$_POST['txt_telemarketer_manager']."')";
									#$data->showsql($sql);
									if (!$data->inpQueryReturnBool($sql))
												{	throw new Exception($data->err_report('s02'));	}
								$booking_id=$data->maxval('tbl_dax_booking','pk_id');
								 
								
								$tm_jobid = $data->get_value("select job_id from tbl_dax_employee where pk_id='".$_POST['txt_telemarketer_id']."'");
								$sql= "INSERT INTO tbl_dax_log (employee_id ,job_id ,status ,customer_id ,date_create,fk_tm_manager,fk_sales_manager)VALUES ('".$_POST['txt_telemarketer_id']."', '".$tm_jobid."', '5', '".$_POST['txt_national_id']."',now(),'".$_POST['txt_telemarketer_manager']."','".$_POST['txt_sales_manager']."')";
									#$data->showsql($sql);
									if (!$data->inpQueryReturnBool($sql))
									{	throw new Exception($data->err_report('s02'));	}					
							}
						 
							$sales_jobid = $data->get_value("select job_id from tbl_dax_employee where pk_id='".$_POST['txt_sales_id']."'");
							$sql= "INSERT INTO tbl_dax_log (employee_id ,job_id ,status ,customer_id ,date_create,fk_tm_manager,fk_sales_manager)VALUES ('".$_POST['txt_sales_id']."', '".$sales_jobid."', '11', '".$_POST['txt_national_id']."',now(),'".$_POST['txt_telemarketer_manager']."','".$_POST['txt_sales_manager']."')";
									#$data->showsql($sql);
									if (!$data->inpQueryReturnBool($sql))
									{	throw new Exception($data->err_report('s02'));	
									
									}
							
						$sql= "INSERT INTO tbl_dax_deal_info (agreement_date, outlet, member_id, agreement_id,customer_name,status,fk_sales_id,fk_sales_manager,venue,fk_customer,fk_booking_id)
						VALUES
						( '".$_POST['txt_agreement_date']."','".$_POST['txt_outlet']."','".$member_id."','".$agreement_id."','".$_POST['txt_first_name']." ".$_POST['txt_last_name']."','Open','".$_POST['txt_sales_id']."','".$_POST['txt_sales_manager']."','".$_POST['txt_venue']."','".$_POST['txt_national_id']."','".$booking_id."')";
							# $data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
							{	throw new Exception($data->err_report('s02'));	}
													   # $data->showsql($sql);					

					}
					 
					
					
							
												$country_name = explode(';',$_POST[txt_country]);
												$country=$country_name[0];
												#$status= $data->get_value("SELECT pk_id FROM tbl_dax_status where  status='txt_status'");
												$sql= "UPDATE ".$tablename." SET first_name = '".$_POST['txt_first_name']."',last_name = '".$_POST['txt_last_name']."',
														mobile='".$_POST['txt_mobile']."',
														card_id = '".$_POST['txt_card_id']."', sex = '".$_POST['txt_sex']."',
														birth_place = '".$_POST['txt_birth_place']."',birth_date = '".$_POST['txt_birth_date']."',
														address = '".$_POST['txt_address']."',city = '".$_POST['txt_city']."',
														country = '".$country."',country_code = '".$_POST['txt_country_area']."',zip_code = '".$_POST['txt_zip_code']."',
														email = '".$_POST['txt_email']."',phone = '".$_POST['txt_phone']."',
														office = '".$_POST['txt_office']."',fax = '".$_POST['txt_fax']."',
														mgm_program = '".$_POST['txt_mgm_program']."',referal_number = '".$_POST['txt_referal_number']."',
														address2 = '".$_POST['txt_address2']."',age = '".$_POST['txt_age']."',
														race = '".$_POST['txt_race']."',marital_status = '".$_POST['txt_marital_status']."',
														spouse_particulars = '".$_POST['txt_id_spouse_particulars']."',occupation = '".$_POST['txt_occupation']."',
														outlet_code = '".$_POST['txt_outlet']."',data_source_id = '".$_POST['txt_data_source']."',
														quality = '".$_POST['txt_quality']."',income = '".$_POST['txt_income']."',tour_type = '".$_POST['txt_tour_type']."',
														status='".$_POST['txt_status']."',sales_id='".$_POST['txt_sales_id']."',
														status_page='1',telemarketer_id='".$_POST[txt_telemarketer_id]."',
														telemarketer_booking_id='".$booking_id."',
														date_call='".$date_call."'
														WHERE pk_id = '".$_POST['txt_national_id']."'";
#$data->showsql($sql);
												if (!$data->inpQueryReturnBool($sql))
													{	throw new Exception($data->err_report('s02'));	}
												    
					#$sp=$data->get_value				
					

				#if (!mysql_query("COMMIT")) {
				if (!$data->commit()) {
								throw new Exception($data->err_report('commitTrans_failed'));
					}
			}
			catch (Exception $e1)
			{
				#$data->rollbackTrans();
				#mysql_query("ROLLBACK");
				$data->rollback();
				$flag = false;
				$err_msg = $e1->getMessage();

			}

	
	
				if ($flag)
			{	
				
				if($_POST['txt_status']==9) {
					#$addLink = "show_modal('sales_deal.php?cust_id=".$_POST['txt_national_id']."&deal_away=1','status:no;help:no;dialogWidth:400px;dialogHeight:600px');window.parent.close()";					
					$success=1;
					
				}else{
					$success=1;
					$addLink = "show_modal('user_customer_confirm.php?id=".$_POST['txt_national_id']."','status:no;help:no;dialogWidth:200px;dialogHeight:200px');window.parent.close()";
						if($_GET['add']==2){
							echo "<script>window.parent.close();</script>";	
						}
					
				}
			}else{	
				echo "<script>alert('".$data->msgbox('s02')."');</script>";	
			}

#print_r($flag);
	
}

if ($_GET['call_log']==1)
{
    $rows = $data->get_row("select * from tbl_dax_customer where pk_id='".$_GET['id']."'");
# print_r($rows);
	# $rows1 = $data->selectQuery("select * from tbl_dax_customer where pk_id='".$_GET['id']."'");

    //$data->showsql("select tbl_upx_user.user_name,tbl_upx_user.user_password,tbl_upx_user.gerai,tbl_upx_user.warehouse,  tbl_upx_user.hak_akses, tbl_upx_user.kode_akses    from ".$tablename.", tbl_upx_akses where pk_id='".$_GET['id']."'    AND tbl_upx_akses.kode_akses = tbl_upx_user.hak_akses");
    /*$rows = $data->selectQuery("select user_name,user_password,gerai,warehouse,hak_akses
    from ".$tablename." where pk_id='".$_GET['id']."'");    */


         $dataRows = array (
				'TEXT' =>  array('ID No','First Name','Sex','Birth Place','Address','City','Zip Code','Phone','Mobile','MGM Program'),
				'TEXT1' =>  array('Customer ID','Last Name','','Birth Date','','Country','Email','Office','Fax','Referal Number'),
				'DOT1'  => array (':',':','',':','',':',':',':',':',':'),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array ($rows1[0],$rows1[1],'',$rows1[2],'',$rows1[3],"<a href=mailto:$rows1[4]>$rows1[4]</a>",$rows1[5],$rows1[6],$rows1[7]),
				'FIELD1' => $rows,

			);

			$tittle = "CALL LOG DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}

#print_r($param);
if ($_GET['detail']==1)
{
   $rows = $data->selectQuery("select * from tbl_dax_customer where pk_id ='".$_GET['id']."'");
	$sp=$data->get_row("select pk_id,first_name, last_name from tbl_dax_spouse_particulars where fk_customer='".$_GET['id']."'");
	
#print_r($rows);
	 	#$data_source= $data->get_value("SELECT name FROM tbl_dax_typedata where  pk_id='".$rows['data_source_id']."'");
		$data_source = $data->datasource_name($rows['data_source_id']);
		$outlet= $data->get_value("SELECT name FROM tbl_dax_outlet where  code='".$rows['outlet_code']."'");
		$status= $data->get_value("SELECT name FROM tbl_dax_status where  pk_id='".$rows['status']."'");

         $dataRows = array (
				'TEXT' =>  array('Date Input','First Name','Data Source','Sex','Age During Check In' ,'Birth Place','Address1','Address2','City','Zip Code','Phone','Mobile','Race','Income','MGM Program',''),
					'FIELD' => array ('&nbsp;'.$rows[date_input],'&nbsp;'.$rows[first_name],$data_source,'&nbsp;'.$rows[sex],'&nbsp;'.$rows[age],'&nbsp;'.$rows[birth_place],'&nbsp;'.$rows[address],'&nbsp;'.$rows[address2],'&nbsp;'.$rows[city],'&nbsp;'.$rows[zip_code],'&nbsp;'.$rows[phone],'&nbsp;'.$rows[mobile],'&nbsp;'.$rows[race],'&nbsp;'.$rows[income],'&nbsp;'.$rows[mgm_program],'&nbsp;'),
				'TEXT1' =>  array('NIRC/Personal ID','Last Name','Outlet','Marital Status','2nd Nominee','Occupation','Birth Date','Country','Country Code','Email','Office','Fax','Tour Type','Quality','Referal Number','Status'),
			
				'FIELD1' => array ('&nbsp;'.$rows[card_id],'&nbsp;'.$rows[last_name],$outlet,'&nbsp;'.$rows[marital_status],'&nbsp;'.$sp[first_name].' '.$sp[last_name].'&nbsp;'.'&nbsp;'.'&nbsp;'."<input type=hidden name=txt_id_spouse_particulars id=txt_id_spouse_particulars value='".$sp['pk_id']."'>"."<img src='image/page_view.png' width='12' height='12' onClick=\"get_detail_sp('".$_GET['id']."')\">",'&nbsp;'.$rows[occupation],'&nbsp;'.$rows[birth_date],'&nbsp;'.$rows[country],'&nbsp;'.$rows[country_code],'&nbsp;'.$rows[email],'&nbsp;'.$rows[office],'&nbsp;'.$rows[fax],'&nbsp;'.$rows[tour_type],'&nbsp;'.$rows[quality],'&nbsp;'.$rows[referal_number],$status),
			);
			$tittle = "CUSTOMER DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}

if ($_GET['add'] == 1)
{   #$data->auth('09030101',$_SESSION['username']);
	$status_default=1;
	$status_id = $_POST[txt_status];
	if($data->auth_boolean(111014,$_SESSION['pk_id'])){
		$status = $data->cb_status('txt_status',$status_id," or pk_id='".$status_default."' or pk_id=5 or pk_id=9",' onchange="document.form1.submit()"');
		if($status_id==5){
				$status .= "<input type='hidden' name='reservasi_pkid' value='".$_POST['reservasi_pkid']."' readonly>"."<br> Telemarketer Manager :<br>".$data->cb_telemarketer_manager('txt_telemarketer_manager')."<BR>Telemarketer : <br>".$data->cb_telemarketer('txt_telemarketer_id',$_POST['txt_telemarketer_id'])."<input type='hidden' name='customer_id_all' value='".$_GET['id']."'><BR>"."Date : <BR><input type='text' name='txt_dateday' id='txt_dateday' readonly>&nbsp;&nbsp;<img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick='get_reservation()'>"."<BR>"."Session : <BR><input type='text' name='txt_session_name' readonly><BR>Session Time :<br><input type='text' name='txt_session_time' readonly>";
		}
		if($status_id==9){
			$status .= "<br>Agreement Date :<br>".$data->datePicker('txt_agreement_date',$_POST['txt_agreement_date'])."<br> Telemarketer Manager :<br>".$data->cb_telemarketer_manager('txt_telemarketer_manager',$_POST['txt_telemarketer_manager'])."<br> Telemarketer :<br>".$data->cb_telemarketer('txt_telemarketer_id',$_POST['txt_telemarketer_id'])."<br> Sales Manager : <br>".$data->cb_sales_manager('txt_sales_manager',$_POST['txt_sales_manager'])."<br> Sales Person : <br>".$data->cb_sales_person('txt_sales_id',$_POST['txt_sales_id'])."<br> Venue : <br>"."<input type=text name=txt_venue id=txt_venue value='".$_POST['txt_venue']."'>";
		}
	
	}else{
		$status = "<Input type='hidden' name='txt_status' value='1'> New";
	}
	
		if(!$_POST){
				$sql = "delete from tbl_dax_customer where status_page='0' and create_by='".$_SESSION['pk_id']."'";
				$data->inpQueryReturnBool($sql);
			
				$id =$data->nextval('tbl_dax_customer','pk_id'); 
				$sql = "insert into tbl_dax_customer(pk_id,create_by,date_input) values('".$id."','".$_SESSION['pk_id']."',now())";
				#$data->showsql($sql);
				$data->inpQueryReturnBool($sql);
			
			
				$sql = "delete from tbl_dax_spouse_particulars where fk_customer='".$id."'";
				$data->inpQueryReturnBool($sql);
				$sql = "insert into tbl_dax_spouse_particulars(fk_customer) values('".$id."')";
				$data->inpQueryReturnBool($sql);
			
	}else{
		$id=$_POST['txt_national_id'];
	}
	
	
	if($_POST){
			$txt_birth_date=$_POST[txt_birth_date];
		}else{
			$txt_birth_date ='0000-00-00';
		}
	
$sp=$data->get_row("select pk_id, first_name, last_name from tbl_dax_spouse_particulars where fk_customer = '".$id."'");
$country_name = explode(';',$_POST[txt_country]);
$country=$country_name[0];
	#print_r($_POST);
	$dataRows = array (
				'TEXT' =>  array('Date Input','ID No','First Name','Data Source','Sex','Age During Check In  #','Birth Place','Address1','Address2','City','Zip Code  #','Phone','Mobile','Race','Income','MGM Program'),
				'FIELD' => array (date("Y-m-d"),
				"<input type=text name=txt_national_id id=txt_national_id value='".$id."' readonly>",
				"<input type=text name=txt_first_name id=txt_first_name value='".$_POST['txt_first_name']."'>",
				"<input type='hidden' name='txt_data_source' value='".$_POST['txt_data_source']."'>"."<input type='text' name='stxt_data_source' value='".$data->datasource_name($_POST['txt_data_source'])."' readonly><img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick=\"get_datasource()\">",
				$data->cb_sex('txt_sex'),
				"<input type=text name=txt_age id=txt_age value='".$_POST['txt_age']."'>",
				"<input type=text name=txt_birth_place id=txt_birth_place value='".$_POST['txt_birth_place']."'>",
				//"<input type=text name=txt_address id=txt_address value='".$_POST['txt_address']."'>",
				"<textarea rows=3 cols=20 name=txt_address>".$_POST['txt_address']."</textarea>",
				//"<input type=text name=txt_address2 id=txt_address2 value='".$_POST['txt_address2']."'>",
				"<textarea rows=3 cols=20 name=txt_address2>".$_POST['txt_address2']."</textarea>",
				"<input type=text name=txt_city id=txt_city value='".$_POST['txt_city']."'>",
				"<input type=text name=txt_zip_code id=txt_zip_code value='".$_POST['txt_zip_code']."'>",
				"<input type=text name=txt_phone id=txt_phone value='".$_POST['txt_phone']."'>",
				"<input size=20 type=text name=txt_mobile id=txt_mobile value='".$_POST['txt_mobile']."'>",
				$data->cb_race('txt_race',$_POST['txt_race']),
				$data->cb_income('txt_income',$_POST['txt_income']),
				"<input type=checkbox name=txt_mgm_program value=yes>"),
				
				'TEXT1' =>  array('','NIRC/Personal ID','Last Name','Outlet','Marital Status','2nd Nominee','Occupation','Birth Date','Country','Country Code','Email','Office  #','Fax   #','Referal Number  #','Quality','Tour Type','Customer Status'),
				'FIELD1' =>  array('',"<input type=text name=txt_card_id id=txt_card_id value='".$_POST['txt_card_id']."'>",
				"<input type=text name=txt_last_name id=txt_last_name value='".$_POST['txt_last_name']."'>",
				$data->cb_outlet('txt_outlet',$_POST['txt_outlet']),
				$data->cb_marital('txt_marital_status',$_POST['txt_marital_status']),
				"<table><tr><td><input type=text name=txt_spouse_particulars id=txt_spouse_particulars value='".$sp['first_name']." ".$sp['last_name']."'><input type=hidden name=txt_id_spouse_particulars id=txt_id_spouse_particulars value='".$sp['pk_id']."'></td><td><img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick=\"get_edit_particulars('".$id."')\"></td></tr></table>",
				"<input type=text name=txt_occupation id=txt_occupation value='".$_POST['txt_occupation']."'>",
				$data->datePicker('txt_birth_date',$txt_birth_date),
				$data->cb_country('txt_country',$country_name[0],"onchange=\"get_telp_code(this.value);\""),
				"<input size=4 type=text name=txt_country_area id=txt_country_area value='".$_POST['txt_country_area']."'>",
				"<input type=text name=txt_email id=txt_email value='".$_POST['txt_email']."'>",
				"<input type=text name=txt_office id=txt_office value='".$_POST['txt_office']."'>",
				"<input type=text name=txt_fax id=txt_fax value='".$_POST['txt_fax']."'>",
				"<input type=text name=txt_referal_number id=txt_referal_number value='".$_POST['txt_referal_number']."'>",
				$data->cb_quality('txt_quality'),
				$data->cb_tour_type('txt_tour_type'),
				$status),


					  );
#print_r($dataRows);
    $tittle = "CUSTOMER ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
					
		
}

if ($_GET['add'] == 2)
{   #$data->auth('09030101',$_SESSION['username']);
	$status_default=1;
	$status_id = $_POST[txt_status];
	if($data->auth_boolean(111014,$_SESSION['pk_id'])){
		$status = $data->cb_status('txt_status',$status_id," or pk_id='".$status_default."' or pk_id=5 or pk_id=9",' onchange="document.form1.submit()"');
		if($status_id==5){
				$status .= "<input type='hidden' name='reservasi_pkid' value='".$_POST['reservasi_pkid']."' readonly>"."<BR>Telemarketer : <br>".$data->cb_telemarketer('txt_telemarketer_id',$_POST['txt_telemarketer_id'])."<input type='hidden' name='customer_id_all' value='".$_GET['id']."'><BR>"."Date : <BR><input type='text' name='txt_dateday' id='txt_dateday' readonly>&nbsp;&nbsp;<img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick='get_reservation()'>"."<BR>"."Session : <BR><input type='text' name='txt_session_name' readonly><BR>Session Time :<br><input type='text' name='txt_session_time' readonly>";
		}
		if($status_id==9){
			$status .= "<br>Agreement Date :<br>".$data->datePicker('txt_agreement_date')."<br> Telemarketer :<br>".$data->cb_telemarketer('txt_telemarketer')."<br> Sales Person : <br>".$data->cb_sales_person('txt_sales_id');
		}
	
	}else{
		$status = "<Input type='hidden' name='txt_status' value='1'> New";
	}
	
		if(!$_POST){
				$sql = "delete from tbl_dax_customer where status_page='0' and create_by='".$_SESSION['pk_id']."'";
				$data->inpQueryReturnBool($sql);
				$id =$data->nextval('tbl_dax_customer','pk_id'); 
				$sql = "insert into tbl_dax_customer(pk_id,create_by,date_input) values('".$id."','".$_SESSION['pk_id']."',now())";
				#$data->showsql($sql);
				$data->inpQueryReturnBool($sql);
		
			
				$sql = "delete from tbl_dax_spouse_particulars where fk_customer='".$id."'";
				$data->inpQueryReturnBool($sql);
				$sql = "insert into tbl_dax_spouse_particulars(fk_customer) values('".$id."')";
				$data->inpQueryReturnBool($sql);
			
	}else{
		$id=$_POST['txt_national_id'];
	}
	
	
	if($_POST){
			$txt_birth_date=$_POST[txt_birth_date];
		}else{
			$txt_birth_date ='0000-00-00';
		}
	
	$sp=$data->get_row("select pk_id, first_name, last_name from tbl_dax_spouse_particulars where fk_customer = '".$id."'");
	
	$dataRows = array (
				'TEXT' =>  array('Date Input','ID No','First Name','Data Source','Sex','Age During Check In  #','Birth Place','Address1','Address2','City','Zip Code  #','Phone','Mobile','Race','Income','MGM Program'),
				'FIELD' => array (date("Y-m-d"),
				"<input type=text name=txt_national_id id=txt_national_id value='".$id."' readonly>",
				"<input type=text name=txt_first_name id=txt_first_name value='".$_POST['txt_first_name']."'>",
				"<input type='hidden' name='txt_data_source' value='".$_POST['txt_data_source']."'>"."<input type='text' name='stxt_data_source' value='".$data->datasource_name($_POST['txt_data_source'])."' readonly><img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick=\"get_datasource()\">",
				$data->cb_sex('txt_sex'),
				"<input type=text name=txt_age id=txt_age value='".$_POST['txt_age']."'>",
				"<input type=text name=txt_birth_place id=txt_birth_place value='".$_POST['txt_birth_place']."'>",
				"<textarea rows=3 cols=20 name=txt_address>".$_POST['txt_address']."</textarea>",
				"<textarea rows=3 cols=20 name=txt_address2>".$_POST['txt_address2']."</textarea>",
				"<input type=text name=txt_city id=txt_city value='".$_POST['txt_city']."'>",
				"<input type=text name=txt_zip_code id=txt_zip_code value='".$_POST['txt_zip_code']."'>",
				"<input type=text name=txt_phone id=txt_phone value='".$_POST['txt_phone']."'>",
				"<input size=20 type=text name=txt_mobile id=txt_mobile value='".$_POST['txt_mobile']."'>",
				$data->cb_race('txt_race',$_POST['txt_race']),
				$data->cb_income('txt_income',$_POST['txt_income']),
				"<input type=checkbox name=txt_mgm_program value=yes>"),
				'TEXT1' =>  array('','NIRC/Personal ID','Last Name','Outlet','Marital Status','2nd Nominee','Occupation','Birth Date','Country','Country Code','Email','Office  #','Fax   #','Referal Number  #','Quality','Tour Type','Customer Status'),
				'FIELD1' =>  array('',"<input type=text name=txt_card_id id=txt_card_id value='".$_POST['txt_card_id']."'>",
				"<input type=text name=txt_last_name id=txt_last_name value='".$_POST['txt_last_name']."'>",
				$data->cb_outlet('txt_outlet',$_POST['txt_outlet']),
				$data->cb_marital('txt_marital_status',$_POST['txt_marital_status']),
				"<table><tr><td><input type=text name=txt_spouse_particulars id=txt_spouse_particulars value='".$sp['first_name']." ".$sp['last_name']."'><input type=hidden name=txt_id_spouse_particulars id=txt_id_spouse_particulars value='".$sp['pk_id']."'></td><td><img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick=\"get_edit_particulars('".$id."')\"></td></tr></table>",
				"<input type=text name=txt_occupation id=txt_occupation value='".$_POST['txt_occupation']."'>",
				$data->datePicker('txt_birth_date',$txt_birth_date),
				$data->cb_country('txt_country',$_POST[country],"onchange=\"get_telp_code(this.value);\""),
				"<input size=4 type=text name=txt_country_area id=txt_country_area value='".$_POST['txt_country_area']."'>",
				"<input type=text name=txt_email id=txt_email value='".$_POST['txt_email']."'>",
				"<input type=text name=txt_office id=txt_office value='".$_POST['txt_office']."'>",
				"<input type=text name=txt_fax id=txt_fax value='".$_POST['txt_fax']."'>",
				"<input type=text name=txt_referal_number id=txt_referal_number value='".$_POST['txt_referal_number']."'>",
				$data->cb_quality('txt_quality'),
				$data->cb_tour_type('txt_tour_type'),
				$status),


					  );
#print_r($dataRows);
    $tittle = "CUSTOMER ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save >",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
					
		
}


if ($_POST['btn_save_edit'])
{
#print_r($_POST);
	  $flag = true;
			try {
					if (!mysql_query("BEGIN"))  {
					throw new Exception($data->err_report('beginTrans_failed'));

					}
						
						
						if($_POST['txt_status']==5){
						
						$date_call = 'now()';
						$sql = "INSERT INTO tbl_dax_booking (fk_customer,date_booking, outlet, tm_code, source_code,date_sign_in,fk_reservation,fk_tm_manager) VALUES ('".$_POST['txt_national_id']."',now(),'".$_SESSION['outletcode']."','".$_POST['txt_telemarketer_id']."','".$_POST['txt_source_code']."','".$_POST['txt_dateday']."','".$_POST['reservasi_pkid']."','".$_POST['txt_telemarketer_manager']."')";
							#$data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
										{	throw new Exception($data->err_report('s02'));	}
						$tm_jobid = $data->get_value("select job_id from tbl_dax_employee where pk_id='".$_POST['txt_telemarketer_id']."'");
						$sql= "INSERT INTO tbl_dax_log (employee_id ,job_id ,status ,customer_id ,date_create,fk_tm_manager,fk_sales_manager)VALUES ('".$_POST['txt_telemarketer_id']."', '".$tm_jobid."', '5', '".$_POST['txt_national_id']."',now(),'".$_POST['txt_telemarketer_manager']."','".$_POST['txt_sales_manager']."')";
							#$data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
							{	throw new Exception($data->err_report('s02'));	
							#mysql_query("ROLLBACK");
							}		
						
						$booking_id=$data->maxval('tbl_dax_booking','pk_id');
						
				
					}
						
					if($_POST['txt_status']==9) {
										
					$date_call = 'now()';
					$agreement_id = $data->next_agreement_id();
					$member_id = $data->member_id($agreement_id);
										
						
					if($_POST['txt_telemarketer_id']){
						$sql = "INSERT INTO tbl_dax_booking (fk_customer,date_booking, outlet, tm_code, source_code,date_sign_in,fk_reservation,fk_tm_manager) VALUES ('".$_POST['txt_national_id']."',now(),'".$_SESSION['outletcode']."','".$_POST['txt_telemarketer_id']."','".$_POST['txt_source_code']."','".$_POST['txt_agreement_date']."','".$_POST['reservasi_pkid']."','".$_POST['txt_telemarketer_manager']."')";
							#$data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
										{	throw new Exception($data->err_report('s02'));	}
						$booking_id=$data->maxval('tbl_dax_booking','pk_id');
						 
						
						$tm_jobid = $data->get_value("select job_id from tbl_dax_employee where pk_id='".$_POST['txt_telemarketer_id']."'");
						$sql= "INSERT INTO tbl_dax_log (employee_id ,job_id ,status ,customer_id ,date_create,fk_tm_manager,fk_sales_manager)VALUES ('".$_POST['txt_telemarketer_id']."', '".$tm_jobid."', '5', '".$_POST['txt_national_id']."',now(),'".$_POST['txt_telemarketer_manager']."','".$_POST['txt_sales_manager']."')";
							#$data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
							{	throw new Exception($data->err_report('s02'));	}					
					}
					 
					$sales_jobid = $data->get_value("select job_id from tbl_dax_employee where pk_id='".$_POST['txt_sales_id']."'");
					$sql= "INSERT INTO tbl_dax_log (employee_id ,job_id ,status ,customer_id ,date_create,fk_tm_manager,fk_sales_manager)VALUES ('".$_POST['txt_sales_id']."', '".$sales_jobid."', '11', '".$_POST['txt_national_id']."',now(),'".$_POST['txt_telemarketer_manager']."','".$_POST['txt_sales_manager']."')";
							#$data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
							{	throw new Exception($data->err_report('s02'));	
							
							}
					
					}
					 
					
					$sql= "INSERT INTO tbl_dax_deal_info (agreement_date, outlet, member_id, agreement_id,customer_name,status,fk_sales_id,fk_sales_manager,venue,fk_customer,fk_booking_id)
						VALUES
						( '".$_POST['txt_agreement_date']."','".$_POST['txt_outlet']."','".$member_id."','".$agreement_id."','".$_POST['txt_first_name']." ".$_POST['txt_last_name']."','Open','".$_POST['txt_sales_id']."','".$_POST['txt_sales_manager']."','".$_POST['txt_venue']."','".$_POST['txt_national_id']."','".$booking_id."')";
							# $data->showsql($sql);
							if (!$data->inpQueryReturnBool($sql))
							{	throw new Exception($data->err_report('s02'));	}
													   # $data->showsql($sql);
					
							
												
	
								#$rows = $data->get_value("select country from tbl_dax_customer where pk_id ='".$_GET['id']."'");
								$country_name = explode(';',$_POST[txt_country]);
								$country=$country_name[0];
								
		

											$sql= "UPDATE ".$tablename." SET first_name = '".$_POST['txt_first_name']."',
												last_name = '".$_POST['txt_last_name']."',	country_code= '+".$_POST['txt_country_area']."',
												mobile='".$_POST['txt_mobile']."',
													card_id = '".$_POST['txt_card_id']."', sex = '".$_POST['txt_sex']."',
													birth_place = '".$_POST['txt_birth_place']."',birth_date = '".$_POST['txt_birth_date']."',
													address = '".$_POST['txt_address']."',city = '".$_POST['txt_city']."',
													country = '".$country."',country_code = '".$_POST['txt_country_area']."',zip_code = '".$_POST['txt_zip_code']."',
													email = '".$_POST['txt_email']."',phone = '".$_POST['txt_phone']."',
													office = '".$_POST['txt_office']."',fax = '".$_POST['txt_fax']."',
													mgm_program = '".$_POST['txt_mgm_program']."',referal_number = '".$_POST['txt_referal_number']."',
													address2 = '".$_POST['txt_address2']."',age = '".$_POST['txt_age']."',
													race = '".$_POST['txt_race']."',marital_status = '".$_POST['txt_marital_status']."',
													spouse_particulars = '".$_POST['txt_spouse_particulars']."',occupation = '".$_POST['txt_occupation']."',
													outlet_code = '".$_POST['txt_outlet']."',data_source_id = '".$_POST['txt_data_source']."',
													quality = '".$_POST['txt_quality']."',income = '".$_POST['txt_income']."',tour_type = '".$_POST['txt_tour_type']."',
													status='".$_POST['txt_status']."',sales_id='".$_POST['txt_sales_id']."'
													WHERE pk_id = '".$_GET['id']."'";
													#$data->showsql($sql);
											if (!$data->inpQueryReturnBool($sql))
													{	throw new Exception($data->err_report('s02'));	}
												    

				if (!mysql_query("COMMIT")) {
								throw new Exception($data->err_report('commitTrans_failed'));
					}
			}
			catch (Exception $e1)
			{
				#$data->rollbackTrans();
				mysql_query("ROLLBACK");
				$flag = false;
				$err_msg = $e1->getMessage();

			}

				if ($flag)	{	
					if($_POST['txt_status']==9){
								$success=1;
						}else{
							echo "<script>alert('".$data->msgbox('s01')."');window.parent.close();</script>";	
						}
				}else{	
					echo "<script>alert('".$data->msgbox('s02')."');</script>";	
				}
	
}
#print_r($_GET);
if ($_GET['edit'] == 1)
{

	$status='';
	$rows = $data->get_row("select * from ".$tablename." where pk_id='".$_GET['id']."'");
	$status_default = $rows[status];
	if($_POST){
			$txt_birth_date=$_POST[txt_birth_date];
			$data_source_id=$_POST[txt_data_source];
			$status_id = $_POST[txt_status];
		}else{
			$txt_birth_date ='0000-00-00';
			$data_source_id= $rows[data_source_id];
			$status_id = $rows[status];
		}

	
	if($data->auth_boolean(111014,$_SESSION['pk_id'])){
		$status = $data->cb_status('txt_status',$status_id," or pk_id='".$status_default."' or pk_id=5 or pk_id=9",' onchange="document.form1.submit()"');
		if($status_id==5){
				$status .= "<input type='hidden' name='reservasi_pkid' value='".$_POST['reservasi_pkid']."' readonly>"."<br> Telemarketer Manager :<br>".$data->cb_telemarketer_manager('txt_telemarketer_manager')."<BR>Telemarketer : <br>".$data->cb_telemarketer('txt_telemarketer_id',$_POST['txt_telemarketer_id'])."<input type='hidden' name='customer_id_all' value='".$_GET['id']."'><BR>"."Date : <BR><input type='text' name='txt_dateday' id='txt_dateday' readonly>&nbsp;&nbsp;<img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick='get_reservation()'>"."<BR>"."Session : <BR><input type='text' name='txt_session_name' readonly><BR>Session Time :<br><input type='text' name='txt_session_time' readonly>";
		}
		if($status_id==9){
			$status .= "<br>Agreement Date :<br>".$data->datePicker('txt_agreement_date',$_POST['txt_agreement_date'])."<br> Telemarketer Manager :<br>".$data->cb_telemarketer_manager('txt_telemarketer_manager',$_POST['txt_telemarketer_manager'])."<br> Telemarketer :<br>".$data->cb_telemarketer('txt_telemarketer_id',$_POST['txt_telemarketer_id'])."<br> Sales Manager : <br>".$data->cb_sales_manager('txt_sales_manager',$_POST['txt_sales_manager'])."<br> Sales Person : <br>".$data->cb_sales_person('txt_sales_id',$_POST['txt_sales_id'])."<br> Venue : <br>"."<input type=text name=txt_venue id=txt_venue value='".$_POST['txt_venue']."'>";
		}
	
	}else{
		$status = "<Input type='hidden' name='txt_status' value='1'> New";
	}

	$sp = $data->get_value("select concat(first_name,' ',last_name) as sp_name from tbl_dax_spouse_particulars where fk_customer ='".$_GET['id']."'");
	#$cd = $data->get_value("select tlp_code from tbl_dax_country where name = '".$rows['country']."'");

	$dataRows = array (
				'TEXT' =>  array('Date Input','ID No','First Name','Data Source','Sex','Age During Check In  #','Birth Place','Address1','Address2','City','Zip Code  #','Phone','Mobile','Race','Income','MGM Program'),
				'FIELD' => array ('&nbsp;'.$rows[date_input],"<input type=text name=txt_national_id value='".$rows[pk_id]."'>",
				"<input type=text name=txt_first_name value='".$rows[first_name]."'>",
				"<input type='hidden' name='txt_data_source' value='".$data_source_id."'>"."<input type='text' name='stxt_data_source' value='".$data->datasource_name($data_source_id)."' readonly><img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick=\"get_datasource()\">",
				$data->cb_sex('txt_sex',$rows[sex]),
				"<input type=text name=txt_age value='".$rows[age]."'>",
				"<input type=text name=txt_birth_place value='".$rows[birth_place]."'>",
				//"<input type=text name=txt_address value='".$rows[address]."'>",
				"<textarea rows=3 cols=20 name=txt_address>".$rows[address]."</textarea>",
				//"<input type=text name=txt_address2 value='".$rows[address2]."'>",
				"<textarea rows=3 cols=20 name=txt_address2>".$rows[address2]."</textarea>",
				"<input type=text name=txt_city value='".$rows[city]."'>",
				"<input type=text name=txt_zip_code value='".$rows[zip_code]."'>",
				"<input type=text name=txt_phone value='".$rows[phone]."'>",
				" <input size=20 type=text name=txt_mobile value='".$rows[mobile]."'>",
				$data->cb_race('txt_race',$rows[race]),
				$data->cb_income('txt_income',$rows[income]),
				"<input type=checkbox name=txt_mgm_program value=yes>"),

				'TEXT1' =>  array('','NIRC/Personal ID','Last Name','Outlet','Marital Status','2nd Nominee','Occupation','Birth Date','Country','Country Code','Email','Office  #','Fax  #','Referal Number  #','Quality','Tour Type','Status'),
				'DOT1'  => array ('',':',':',':',':',':',':',':',':',':',':',':',':',':',':',':'),
				'FIELD1' =>  array("","<input type=text name='txt_card_id' id='txt_card_id' value='".$rows[card_id]."'>",
				"<input type=text name=txt_last_name value='".$rows[last_name]."'>",
				$data->cb_outlet('txt_outlet',$rows[outlet_code]),
				$data->cb_marital('txt_marital_status',$rows[marital_status]),
				"<input type=text name=txt_spouse_particulars value='".$sp."' readonly><img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick=\"get_edit_particulars('".$_GET[id]."')\">",
				"<input type=text name=txt_occupation value='".$rows[occupation]."'>",
				$data->datePicker('txt_birth_date', $rows['birth_date']),
				$data->cb_country('txt_country',$rows[country],"onchange=\"get_telp_code(this.value);\""),
				" <input size=4 type=text name=txt_country_area value='".$rows[country_code]."'>",
				"<input type=text name=txt_email value='".$rows[email]."'>",
				"<input type=text name=txt_office value='".$rows[office]."'>",
				"<input type=text name=txt_fax value='".$rows[fax]."'>",
				"<input type=text name=txt_referal_number value='".$rows[referal_number]."'>",
				$data->cb_quality('txt_quality',$rows[quality]),
				$data->cb_tour_type('txt_tour_type',$rows[tour_type]),
				$status)

         			   );

    $tittle = "CUSTOMER EDIT";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"

					);
}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);





$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');

if(($_POST['txt_status']==9)and($success==1)){
					$id = $data->maxval('tbl_dax_deal_info','pk_id');
					$addLink = "show_modal('sales_deal.php?id=".$id."&deal_away=1&cust_id=".$_POST['txt_national_id']."','status:no;help:no;dialogWidth:600px;dialogHeight:600px');window.parent.close()";
					/*echo "<script>$addLink;</script>";*/
				echo "<script>alert('".$data->msgbox('s01')."');window.parent.close();</script>";	
	
		
}

if ((!$_GET['detail'])and($_POST['txt_status']!=9))		{
	echo "<script>document.getElementById('txt_card_id').focus();</script>";
	if($_GET['add']==1){
		if($success==1){
				/*echo "<script>$addLink</script>";*/
				echo "<script>alert('".$data->msgbox('s01')."');window.parent.close();</script>";	
		};
	}
}


?>