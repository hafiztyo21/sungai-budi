<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'telemarketer.class.php';
#require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new telemarketer;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('telemarketer_print_detail.html');

$order_by='first_name';
$sort_order='asc';

$link = 'telemarketer_print_detail.php';
$addLink = "<a href='telemarketer_print.php' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:600px;dialogHeight:400px')>ADD</a>";
//$addLink = "<a href='user.php' onclick=show_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";

/*if ($_POST['btprint']=='Print')
{   #$data->auth('09030101',$_SESSION['user_id']);

#print_r($_POST);	
		$sql = "UPDATE tbl_dax_assign_info SET status_print='Printed' WHERE pk_id = '".$_POST['id_assign']."'" ;
	#$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script></script>";	}
	else
	{	echo "<script>alert('update failed');</script>";	}
	
}
*/

if ($_POST['btn_search'] )
{
	$id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];

	$sql = "SELECT * FROM tbl_dax_customer WHERE upper($id_serach) like upper('%$q_serach%')" ;

	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else

{
	$_SESSION['sql']='';
		
	$sql = "select 
			tbl_dax_customer.pk_id,tbl_dax_customer.first_name,tbl_dax_customer.last_name,
			tbl_dax_customer.age, tbl_dax_customer.country_code, tbl_dax_customer.phone_area_code, tbl_dax_customer.mobile, tbl_dax_customer.phone, 
			tbl_dax_status.name as status
			from tbl_dax_customer 
			left join tbl_dax_status on tbl_dax_customer.status = tbl_dax_status.pk_id 
			where tbl_dax_customer.telemarketer_assign_id=".$_GET['id_assign']." and status='2'
			order by $order_by $sort_order";
			#$data->showsql($sql);
			
	$tele = "select CONCAT(tbl_dax_employee.first_name,' ', tbl_dax_employee.last_name) as tele from tbl_dax_assign_info,tbl_dax_employee 	
			where tbl_dax_assign_info.telemarketer_id = tbl_dax_employee.pk_id
			and tbl_dax_assign_info.pk_id='".$_GET['id_assign']."'";
	#$tele_value = $data->getvalue('$tele');
	$tele_value = $data->get_value($tele);
	#print_r($tele_value);
		#$data->showsql($tele);	
	echo '<div align=right> <font size=4 color="#0000FF">'.$tele_value.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></div>';
			
			
}

$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridTelemarketerPrint($sql,'pk_id','first_name',10000,$pg,'view',$link,'menu',$link,'assign to',$link,'delete',$link);
 #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);

$tmpl->addVar('page','add',$addLink);
$tmpl->addVar('page','printdetail',"<input type='hidden' name='id_assign' value='".$_GET[id_assign]."'><input type='submit' name='btprint' value='Print'>");
$tmpl->addVar('page','cancel',"<input type='button' name='btcancel' value='Close' onclick=\"window.parent.close();\">");

$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);

$tmpl->addVar('page','cekclearall',$cekclearall);
$tmpl->displayParsedTemplate('page');
?>