<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('get_report_uang_makan_send_email.html');
$tablename = 'tbl_dax_run_um';


if ($_GET['send_email'] == 1)
{   	
		$rowb = $data->get_row("select * from tbl_dax_batch where pk_id='".$_POST[txt_fk_batch]."'");
		$b_name =  $rowb[name];
		$head_d = $data->get_rows("select tbl_dax_department_head.*,tbl_dax_department.name as department_name,tbl_dax_location.name as location_name, tbl_dax_employee.full_name as employee_name from tbl_dax_department_head left join tbl_dax_department on tbl_dax_department_head.fk_department = tbl_dax_department.pk_id left join tbl_dax_employee on tbl_dax_department_head.fk_employee = tbl_dax_employee.pk_id left join tbl_dax_location on tbl_dax_department_head.fk_location = tbl_dax_location.pk_id order by tbl_dax_department.name,tbl_dax_location.name,tbl_dax_employee.full_name asc");
		//print_r($head_d);
		$chk = '<table width="100%">';
		$chk  .= '<tr style="background-color:#333333;color:#fff;font-weight:bold;"><td width="10%">No</td><td width="10%">&nbsp;</td><td width="20%">Department</td><td width="20%">Location</td><td width="20%">Employee Name</td><td width="20%">Employee ID</td></tr>';
		for($i=0;$i<count($head_d);$i++){
			$no=$i+1;
			//if(($i%2)==0){
			$chk  .= '<tr>';
			//}
			$chk  .= '<td>'.$no.'</td><td><input type="checkbox" id="head" name="'.$head_d[$i]['pk_id'].'" value="'.$head_d[$i]['pk_id'].'" onClick=\'getHead(this)\'></td><td>'.$head_d[$i]['department_name'].'</td><td>'.$head_d[$i]['location_name'].'</td><td>'.$head_d[$i]['employee_name'].'</td><td>'.$head_d[$i]['fk_employee'].'</td>';
			//if(($i%2)!=0){
			$chk  .= '</tr>';
			//}
		}
		$chk  .= '</table>';
		$dataRows = array (
				'TEXT' => array('BATCH', 'REPORT', 'HEAD DEPARTMENT'),
				'DOT'  => array (':',':',':'),
				'FIELD' => array (  "<input type='hidden' name='txt_fk_batch' value='".$_POST[txt_fk_batch]."'>$b_name&nbsp;<img src='image/btn_extend.gif' style='cursor:hand;border:0px' align='absmiddle' onClick='get_um()'>",
									$data->cb_report_uang_makan_znx('txt_report_uang_makan',$_POST[txt_report_uang_makan]),
									"<input type='hidden' name='txt_head_d' id='txt_head_d'><input type='hidden' name='txt_head_id' id='txt_head_id' value='".$head_d[$i]['fk_employee']."'>".$chk
								)
          			  );

    $tittle = "RUN UANG MAKAN SEND EMAIL";

    $button = array ('SUBMIT' => "<input type=button name=btn_save value='Send Email' onclick=\"getSendEmail();\">",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
}

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVars('path',$path);
$tmpl->addVars('row',$dataRows );
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>