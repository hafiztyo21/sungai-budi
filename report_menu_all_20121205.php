<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'payment_cash.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new payment_cash;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>

</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="1800" cellspacing="0" cellpadding="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<? if($data->auth_boolean(1210,$_SESSION['pk_id'])){  ?>
<li> <a href="report_allowance_status.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Attendance Status</a></li>
<? } ?>
<? if($data->auth_boolean(1211,$_SESSION['pk_id'])){  ?>
<li> <a href="report_allowance_payroll.php" target="contentTabFrame" onClick="activeLink(this);">Allowance Payroll</a></li>
<? } ?>
<? if($data->auth_boolean(1222,$_SESSION['pk_id'])){  ?>
<li> <a href="report_overall_performance.php" target="contentTabFrame" onClick="activeLink(this);">Overall Performance </a></li>
<? } ?>
<? if($data->auth_boolean(1212,$_SESSION['pk_id'])){  ?>
<li> <a href="report_refuse.php" target="contentTabFrame" onClick="activeLink(this);">Refuse Report</a></li>
<? } ?>

<? if($data->auth_boolean(1213,$_SESSION['pk_id'])){  ?>
<li> <a href="report_refuse_by_dept.php" target="contentTabFrame" onClick="activeLink(this);">Refuse Report by Batch&Dept</a></li>
<? } ?>
<? if($data->auth_boolean(1214,$_SESSION['pk_id'])){  ?>
<li> <a href="report_entrance.php" target="contentTabFrame" onClick="activeLink(this);">Entrance Report</a></li>
<? } ?>
<? if($data->auth_boolean(1215,$_SESSION['pk_id'])){  ?>
<li> <a href="report_entrance_dept.php" target="contentTabFrame" onClick="activeLink(this);">Entrance Report by Dept Today </a></li>
<? } ?>
<? if($data->auth_boolean(1216,$_SESSION['pk_id'])){  ?>
<li> <a href="report_cuti_tahunan.php" target="contentTabFrame" onClick="activeLink(this);">Cuti Tahunan Report </a></li>
<? } ?>
<? if($data->auth_boolean(1217,$_SESSION['pk_id'])){  ?>
<li> <a href="report_cuti_besar.php" target="contentTabFrame" onClick="activeLink(this);">Cuti Besar Report </a></li>
<? } ?>
<? if($data->auth_boolean(1218,$_SESSION['pk_id'])){  ?>
<li> <a href="report_cuti_haid.php" target="contentTabFrame" onClick="activeLink(this);">Cuti Haid Report </a></li>
<? } ?>
<? if($data->auth_boolean(1218,$_SESSION['pk_id'])){  ?>
<li> <a href="report_ijin.php" target="contentTabFrame" onClick="activeLink(this);">Ijin Report </a></li>
<? } ?>
<? if($data->auth_boolean(1219,$_SESSION['pk_id'])){  ?>
<li> <a href="report_alpha_noncuti.php" target="contentTabFrame" onClick="activeLink(this);">Alpha Non-Cuti Report </a></li>
<? } ?>
<? if($data->auth_boolean(1224,$_SESSION['pk_id'])){  ?>
<li> <a href="report_error_system.php" target="contentTabFrame" onClick="activeLink(this);">Mesin Error Report </a></li>
<? } ?>
<? if($data->auth_boolean(1220,$_SESSION['pk_id'])){  ?>
<li> <a href="report_unusedly.php" target="contentTabFrame" onClick="activeLink(this);">Unusually Report </a></li>
<? } ?>
<? #if($data->auth_boolean(10,$_SESSION['pk_id'])){  ?>
<!--li> <a href="report_uang_makan.php" target="contentTabFrame" onClick="activeLink(this);">Uang Makan Report </a></li-->
<? #} ?>
<? if($data->auth_boolean(1221,$_SESSION['pk_id'])){  ?>
<li> <a href="get_report_uang_makan.php" target="contentTabFrame" onClick="activeLink(this);">Uang Makan Report </a></li>
<? } ?>
<? if($data->auth_boolean(1223,$_SESSION['pk_id'])){  ?>
<li> <a href="report_pengeluaran.php" target="contentTabFrame" onClick="activeLink(this);">Bukti Pengeluaran Report </a></li>
<? } ?>

</ul>
</td>
</tr>
  <!--tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container">&nbsp;</td>
  </tr-->
</table>
</body>
</html>