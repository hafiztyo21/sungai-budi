<?php
 
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

$data = new globalFunction;

//$dow = $data->get_value("select DAYOFWEEK(now())");
//echo $dow;


$sql2 = "UPDATE tbl_dax_attendance_status 
SET STATUS=IF(TIME(date_in)>'08:05:00' AND TIME(date_in)<'08:15:00','HT1', IF(TIME(date_in)>='08:15:00','HT2',IF(TIME(date_out)<'17:00:00','HT3','H'))) 
WHERE fk_employee=1374 AND day_date=DATE(DATE_SUB(NOW(), INTERVAL 1 DAY)) AND STATUS IN ('H','HT1','HT2','HT3')";
$data->inpQueryReturnBool($sql2);

$sql2 = "UPDATE tbl_dax_machine_m
SET STATUS=IF(TIME(checktime)>'08:05:00' AND TIME(checktime)<'08:15:00','HT1', IF(TIME(checktime)>='08:15:00','HT2','H')) 
WHERE userid=1374 AND day_date=DATE(DATE_SUB(NOW(), INTERVAL 1 DAY))";
$data->inpQueryReturnBool($sql2);

$sql2 = "UPDATE  tbl_dax_machine_n
SET STATUS=IF(TIME(checktime)<'17:00:00','HT3','H' )
WHERE userid=1374 AND day_date=DATE(DATE_SUB(NOW(), INTERVAL 1 DAY)) AND STATUS IN ('H','HT1','HT2','HT3')";
$data->inpQueryReturnBool($sql2);

?>