<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'customer.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'warehouse.class.php';

$data = new customer;
$tablename = 'tbl_dax_customer';
$id =$data->nextval($tablename,'pk_id');

?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>

</head>
<body id="activelink" onLoad="">

<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="100%" cellpadding="0" cellspacing="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<? if($data->auth_boolean(1110,$_SESSION['pk_id'])){  ?>
<li> <a href="user_customer.php?id=<?=$id?>" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Customer</a></li>
<? } ?>
<? if($data->auth_boolean(1110,$_SESSION['pk_id'])){  ?>
<li> <a href="user_customer_invalid.php" target="contentTabFrame"  onClick="activeLink(this);">Invalid Phone Call</a></li>
<? } ?>
<? if($data->auth_boolean(1110,$_SESSION['pk_id'])){  ?>
<li> <a href="import_customer_invalid.php" target="contentTabFrame"  onClick="activeLink(this);">Invalid Lead</a></li>
<? } ?>
<? if($data->auth_boolean(1111,$_SESSION['pk_id'])){  ?>
<li> <a href="import_customer.php" target="contentTabFrame" onClick="activeLink(this);">Import Data</a></li>
<? } ?>
<? if($data->auth_boolean(11119999,$_SESSION['pk_id'])){  ?>
<li> <a href="import_customer_invalid.php" target="contentTabFrame" onClick="activeLink(this);">Invalid Data</a></li>
<? } ?>
<? if($data->auth_boolean(1112,$_SESSION['pk_id'])){  ?>
<li> <a href="data_from.php" target="contentTabFrame" onClick="activeLink(this);">Data Source</a></li>
<? } ?>

</ul>
</td>
<tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
</tr>
</table>
</body>
</html>