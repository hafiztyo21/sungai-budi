<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'reimburse.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new reimburse;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('reimbursement_data.html');


if($_GET['del']==1){

	$sql = "DELETE FROM tbl_dax_reimbursement_data where pk_id='".$_GET['id']."'";
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');window.location='reimbursement_data.php?page=".$_GET[page]."';</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}
}
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='tbl_dax_employee.pk_id'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
$linkEdit='user_edit.php';
$link = 'reimbursement_data_add.php';

if($data->auth_boolean(161010,$_SESSION['pk_id'])){

$addLink = "<a href='reimbursement_data.php' onclick=popup('".$link."?add=1','ReimbursementData')>ADD</a>";
//$addLink = "<a href='user.php' onclick=show_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
$tmpl->addVar('page','add',$addLink);
}

	if ($_SESSION['pajak'] =='P'){
		$filter = " tbl_dax_employee.tax_status='".$_SESSION['pajak']."'";
	}else{
		$filter = " tbl_dax_employee.tax_status in ('P','NP','')";
	}

$sql_emp = $data->get_rows("SELECT distinct pk_id FROM tbl_dax_employee");
$id_emp = '';
					for($i=0;$i<count($sql_emp);$i++){
						$id_emp .= "'".$sql_emp[$i][pk_id]."'".',';
					}
					$id_emp = substr($id_emp,0,-1);

if ($_POST['btn_search'] )
{

	if($_POST[txt_status]=='-1'){
		$filter_status = "";
	}else{
		$filter_status = " and  tbl_dax_employee.status= '".$_POST[txt_status]."' ";
	}
	if($_POST[txt_department]=='0'){
		$filter_department = "";
	}else{
		$filter_department = " and  tbl_dax_employee.fk_department='".$_POST[txt_department]."'  ";
	}
	if($_POST[txt_location]=='0'){
		$filter_location = "";
	}else{
		$filter_location = " and  tbl_dax_employee.fk_location='".$_POST[txt_location]."'  ";
	}

	if($_POST[txt_job]=='-1'){
		$filter_job = "";
	}else{
		$filter_job = " and  tbl_dax_employee.fk_job='".$_POST[txt_job]."'  ";
	}

	if($_POST[txt_kelamin]=='-1'){
		$filter_kelamin = "";
	}else{
		$filter_kelamin = " and  tbl_dax_employee.sex='".$_POST[txt_kelamin]."'  ";
	}

	if(($_POST[txt_name]=='0') || ($_POST[txt_name]=='')){
		$filter_name = "";
	}else{
		$filter_name = " and tbl_dax_employee.pk_id = '".$_POST[txt_name]."'  ";
	}
	if($_POST[txt_id_emp]==''){
		$filter_id_emp = "";
	}else{
		$filter_id_emp = " and tbl_dax_employee.pk_id ='".$_POST[txt_id_emp]."'  ";
	}


	/*$sql  = "select distinct tbl_dax_reimbursement_data.fk_employee,
			tbl_dax_employee.pk_id as id_employee,tbl_dax_employee.full_name as name_employee,tbl_dax_employee.fk_job,
			tbl_dax_employee.fk_department,tbl_dax_employee.fk_location,tbl_dax_employee.status,
			DATE_FORMAT(tbl_dax_employee.start_date,'%d-%M-%Y') as vstart_date,tbl_dax_employee.start_date,
			tbl_dax_department.pk_id as id_department,tbl_dax_department.name as department,
			tbl_dax_location.pk_id as id_location,tbl_dax_location.name as location,
			tbl_dax_job.pk_id as id_job,tbl_dax_job.name as job_position,tbl_dax_golongan.code as code_medic
			from tbl_dax_reimbursement_data
			left join tbl_dax_employee on tbl_dax_employee.pk_id = tbl_dax_reimbursement_data.fk_employee
			left join tbl_dax_department on tbl_dax_department.pk_id = fk_department
			left join tbl_dax_location on tbl_dax_location.pk_id = fk_location
			left join tbl_dax_job on tbl_dax_job.pk_id = fk_job
			left join tbl_dax_golongan on   tbl_dax_employee.fk_medic  = tbl_dax_golongan.pk_id
			where tbl_dax_employee.pk_id in($id_emp) and
			upper(tbl_dax_employee.full_name) like upper('%".$_POST[txt_name]."%')
			$filter_department $filter_location order by tbl_dax_employee.pk_id asc";*/

	$sql = "SELECT tbl_dax_employee.pk_id,tbl_dax_employee.full_name,tbl_dax_employee.email,tbl_dax_location.name as location,tbl_dax_employee_status.description as emp_status,
			DATE_FORMAT(tbl_dax_employee.start_date,'%d-%M-%Y') as vstart_date,
			tbl_dax_employee.nickname ,tbl_dax_job.name as job, tbl_dax_department.name as department,
			tbl_dax_golongan.code
			FROM tbl_dax_employee
			LEFT JOIN tbl_dax_job on tbl_dax_employee.fk_job = tbl_dax_job.pk_id
			LEFT JOIN tbl_dax_department on tbl_dax_employee.fk_department = tbl_dax_department.pk_id
			LEFT JOIN tbl_dax_location on tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			LEFT JOIN tbl_dax_employee_status on tbl_dax_employee.status = tbl_dax_employee_status.pk_id
			left join tbl_dax_golongan on tbl_dax_employee.fk_medic = tbl_dax_golongan.pk_id
			where
			$filter $filter_name $filter_department $filter_location
			$filter_id_emp
			 order by $order_by $sort_order " ;
#print_r($sql);
	$_SESSION['sql']=$sql;
}elseif (($_SESSION['sql']) and ($_GET)){
    $sql = $_SESSION['sql'];
}else{
	$_SESSION['sql']='';

	/*$sql_lama  = "select distinct tbl_dax_reimbursement_data.fk_employee,
			tbl_dax_employee.pk_id as id_employee,tbl_dax_employee.full_name as name_employee,tbl_dax_employee.fk_job,
			tbl_dax_employee.fk_department,tbl_dax_employee.fk_location,tbl_dax_employee.status,
			DATE_FORMAT(tbl_dax_employee.start_date,'%d-%M-%Y') as vstart_date,tbl_dax_employee.start_date,
			tbl_dax_department.pk_id as id_department,tbl_dax_department.name as department,
			tbl_dax_location.pk_id as id_location,tbl_dax_location.name as location,
			tbl_dax_job.pk_id as id_job,tbl_dax_job.name as job_position,tbl_dax_golongan.code as code_medic
			from tbl_dax_reimbursement_data
			left join tbl_dax_employee on tbl_dax_employee.pk_id = tbl_dax_reimbursement_data.fk_employee
			left join tbl_dax_department on tbl_dax_department.pk_id = fk_department
			left join tbl_dax_location on tbl_dax_location.pk_id = fk_location
			left join tbl_dax_job on tbl_dax_job.pk_id = fk_job
			left join tbl_dax_golongan on   tbl_dax_employee.fk_medic  = tbl_dax_golongan.pk_id
			where tbl_dax_employee.pk_id in($id_emp)
			order by $order_by $sort_order";*/

	$sql = "SELECT tbl_dax_employee.pk_id,tbl_dax_employee.full_name,tbl_dax_employee.email,tbl_dax_location.name as location,tbl_dax_employee_status.description as emp_status,
			DATE_FORMAT(tbl_dax_employee.start_date,'%d-%M-%Y') as vstart_date,
			tbl_dax_employee.nickname ,tbl_dax_job.name as job, tbl_dax_department.name as department,
			tbl_dax_golongan.code
			FROM tbl_dax_employee
			LEFT JOIN tbl_dax_job on tbl_dax_employee.fk_job = tbl_dax_job.pk_id
			LEFT JOIN tbl_dax_department on tbl_dax_employee.fk_department = tbl_dax_department.pk_id
			LEFT JOIN tbl_dax_location on tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			LEFT JOIN tbl_dax_employee_status on tbl_dax_employee.status = tbl_dax_employee_status.pk_id
			left join tbl_dax_golongan on tbl_dax_employee.fk_medic = tbl_dax_golongan.pk_id
			where
			$filter and tbl_dax_employee.status in(0,1,2)
	 		order by $order_by $sort_order";


}
$arrFields = array(
		'tbl_dax_employee.full_name'=>'NAME',
		'tbl_dax_department.name '=>'DEPARTMENT',
		'tbl_dax_location.name '=>'LOCATION',
		);

#print_r($sql);

$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridReimbursementData($sql,'pk_id','code',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link);
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         #$paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		 $paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);

#$tmpl->addVar('page','name',$data->cb_employee_all('txt_name',$_POST[txt_name]));
$tmpl->addVar('page','name',$data->cb_employee_all_number('txt_name',$_POST[txt_name]));
$tmpl->addVar('page','txt_id_employee',"<input type='text' name='txt_id_emp' value='".$_POST[txt_id_emp]."'>");
$tmpl->addVar('page', 'department',$data->cb_department_search('txt_department',$_POST[txt_department]));
$tmpl->addVar('page', 'location',$data->cb_location_search('txt_location',$_POST[txt_location]));
#$tmpl->addVar('page', 'job',$data->cb_dax_job('txt_job',$_POST[txt_job]));

#$arrSex = array ('-1' => '- All -', '0' => 'Male' , '1' => 'Female');
#$_POST[txt_kelamin] = ($_POST[txt_kelamin]=='')? '-1' : $_POST[txt_kelamin];
#$tmpl->addVar('page', 'kelamin',$data->cb_select('txt_kelamin',$arrSex,$_POST[txt_kelamin]));


#$arrStatus = array ('-1' => '- All -', '0' => 'Not Active' , '1' => 'Active','2' => 'Mutasi');
#$_POST[txt_status] = ($_POST[txt_status]=='')? '-1' : $_POST[txt_status];
#$tmpl->addVar('page', 'status',$data->cb_select('txt_status',$arrStatus,$_POST[txt_status]));
$p_pengobatan = $data->get_value("select count(*) from tbl_dax_reimbursement_data where fk_type_reimburse = 1 and year(now())=year(date) and status_reimburse=0");
$p_pengobatan_link = "<a href='#' onclick=\"popup('modal_reimbursement_data_view02.php?detail=1&mode=pengobatan','ReimbursementAdd')\">Pengobatan Submit $p_pengobatan Data</a>&nbsp;<img src='image/page_view.png' width='12' height='12'>";

$p_lnormal = $data->get_value("select count(*) from tbl_dax_reimbursement_data where fk_type_reimburse = 2 and year(now())=year(date) and status_reimburse=0");
$p_lnormal_link = "<a href='#' onclick=\"popup('modal_reimbursement_data_view02.php?detail=1&mode=lahir_normal','ReimbursementAdd')\">Lahir Normal Submit $p_lnormal Data</a>&nbsp;<img src='image/page_view.png' width='12' height='12'>";

$p_loperasi = $data->get_value("select count(*) from tbl_dax_reimbursement_data where fk_type_reimburse = 3 and year(now())=year(date) and status_reimburse=0");
$p_loperasi_link = "<a href='#' onclick=\"popup('modal_reimbursement_data_view02.php?detail=1&mode=lahir_operasi','ReimbursementAdd')\">Lahir Operasi Submit $p_loperasi Data</a>&nbsp;<img src='image/page_view.png' width='12' height='12'>";

$p_lensakc= $data->get_value("select count(*) from tbl_dax_reimbursement_data where fk_type_reimburse = 4 and year(now())=year(date) and status_reimburse=0");
$p_lensakc_link = "<a href='#' onclick=\"popup('modal_reimbursement_data_view02.php?detail=1&mode=lensa','ReimbursementAdd')\">Lensa Kacamata Submit $p_lensakc Data</a>&nbsp;<img src='image/page_view.png' width='12' height='12'>";

$p_rangkakc = $data->get_value("select count(*) from tbl_dax_reimbursement_data where fk_type_reimburse = 5 and year(now())=year(date) and status_reimburse=0");
$p_rangkakc_link = "<a href='#' onclick=\"popup('modal_reimbursement_data_view02.php?detail=1&mode=rangka','ReimbursementAdd')\">Rangka Kacamata Submit $p_rangkakc Data</a>&nbsp;<img src='image/page_view.png' width='12' height='12'>";

$pending = array (
				'P_PENGOBATAN' => $p_pengobatan_link,
				'P_LAHIR_NORMAL' => $p_lnormal_link,
				'P_LAHIR_OPERASI' => $p_loperasi_link,
				'P_LENSA' => $p_lensakc_link,
				'P_RANGKA' => $p_rangkakc_link

			);

$tmpl->addVars('pending', $pending);


$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>