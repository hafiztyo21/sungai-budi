<?php


require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$last_month = $data->get_value("select date(DATE_ADD(now(),INTERVAL -1 MONTH))");

$sql2 = "delete from tbl_dax_mail_log where  date_created < $last_month ";
$data->inpQueryReturnBool($sql2);

$h = $data->get_value("select hour(now())");

if($h==11){
#if($h==0){
	$email_send_to = $data->email_send_to_entrance('A',date("Y-m-d"));
	if($email_send_to==1){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail Alpha Status on Entrance Success')";
	 	$data->inpQueryReturnBool($sql2);
	}elseif($email_send_to==0){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail Alpha Status  on Entrance Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail Alpha Status  on Entrance Failed, more than 100 employee have experienced this case')";
	 	$data->inpQueryReturnBool($sql2);
	}
	$email_send_to = $data->email_send_to_entrance('HT1',date("Y-m-d"));
	if($email_send_to==1){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail HT1 Status  on Entrance Success')";
	 	$data->inpQueryReturnBool($sql2);
	}elseif($email_send_to==0){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail HT1 Status  on Entrance Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail HT1 Status  on Entrance Failed, more than 100 employee experienced have this case')";
	 	$data->inpQueryReturnBool($sql2);
	}
	$email_send_to = $data->email_send_to_entrance('HT2',date("Y-m-d"));
	if($email_send_to==1){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail HT2 Status  on Entrance Success')";
	 	$data->inpQueryReturnBool($sql2);
	}elseif($email_send_to==0){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail HT2 Status  on Entrance Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail HT2 Status  on Entrance Failed, more than 100 employee experienced have this case')";
	 	$data->inpQueryReturnBool($sql2);
	}
	/*$email_send_to = $data->email_send_to_entrance('HT3',date("Y-m-d"));
	if($email_send_to){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail HT3 Status  on Entrance Success')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail HT3 Status  on Entrance Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}*/

	$date = date("Y-m-d");
	#$date = $data->get_value("select date(DATE_ADD(now(),INTERVAL -20 DAY))");
	$email_send_to = $data->email_send_to_entrance_HD("'HT2','A','HT1'",$date);
	if($email_send_to==1){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  to Head Department  on Entrance Success')";
	 	$data->inpQueryReturnBool($sql2);
	}elseif($email_send_to==0){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  to Head Department  on Entrance Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  to Head Department  on Entrance Failed, more than 100 employee experienced have this case')";
	 	$data->inpQueryReturnBool($sql2);
	}

}

if($h==0){
#if($h==23){
	$date = $data->get_value("select date(DATE_ADD(now(),INTERVAL -1 DAY))");
	#$date = $data->get_value("select date(DATE_ADD(now(),INTERVAL -30 DAY))");
	$email_send_to = $data->email_send_to_refuse("'A','A-N-CT'",$date);
	if($email_send_to==1){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail Alpha Status on Refuse Attendance  Success')";
	 	$data->inpQueryReturnBool($sql2);
	}elseif($email_send_to==0){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail Alpha Status on Refuse Attendance  Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail Alpha Status on Refuse Attendance  Failed,  more than 100 employee experienced have this case')";
	 	$data->inpQueryReturnBool($sql2);
	}

	$email_send_to = $data->email_send_to_refuse("'HT3'",$date);
	if($email_send_to==1){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  HT3 Status on Refuse Attendance  Success')";
	 	$data->inpQueryReturnBool($sql2);
	}elseif($email_send_to==0){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  HT3 Status on Refuse Attendance  Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  HT3 Status on Refuse Attendance  Failed,  more than 100 employee experienced have this case')";
	 	$data->inpQueryReturnBool($sql2);
	}

	$email_send_to = $data->email_send_to_refuse("'AH'",$date);
	if($email_send_to==1){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  AH Status on Refuse Attendance  Success')";
	 	$data->inpQueryReturnBool($sql2);
	}elseif($email_send_to==0){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  AH Status on Refuse Attendance  Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  AH Status on Refuse Attendance  Failed,  more than 100 employee experienced have this case')";
	 	$data->inpQueryReturnBool($sql2);
	}


	$email_send_to = $data->email_send_to_refuse_HD("'AH','A','HT3','A-N-CT'",$date);
	if($email_send_to==1){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  to Head Department on Refuse Attendance  Success')";
	 	$data->inpQueryReturnBool($sql2);
	}elseif($email_send_to==0){
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  to Head Department on Refuse Attendance  Failed')";
	 	$data->inpQueryReturnBool($sql2);
	}else{
		$sql2 = "insert into tbl_dax_mail_log (date_created,description) values(now(),'Automatic Mail  to Head Department on Refuse Attendance  Failed,  more than 100 employee experienced have this case')";
	 	$data->inpQueryReturnBool($sql2);
	}
}

/*echo "<script>window.parent.close();</script>";*/
?>
