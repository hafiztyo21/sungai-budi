<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_GET);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('emp_personal_salary_add.html');
$tablename = 'tbl_dax_employee';
$tablename_salary = 'tbl_dax_personal_salary';



if ($_POST['btn_save']){
	
	
		//$pos = strrpos($_POST['txt_daily_formula'], "salary_monthly");
		//$pos2 = strrpos($_POST['txt_weekly_formula'], "salary_monthly");
		//$pos3 = strrpos($_POST['txt_hourly_formula'], "salary_monthly");
		$pos = $data->cek_formula($_POST['txt_daily_formula'],"salary_monthly");
		$pos2 = $data->cek_formula($_POST['txt_weekly_formula'],"salary_monthly");
		$pos3 = $data->cek_formula($_POST['txt_hourly_formula'],"salary_monthly");
		#echo $pos."==".$pos2;
		$x=1;
		if($_POST['txt_type_daily'] == 'Fixed'){
			$x = $x * 1;
		}else{
			if($pos == 1){
				$x = $x * 1;
			}else{
				$x = $x * 0;
			}
		}
		
		if($_POST['txt_type_weekly'] == 'Fixed'){
			$x = $x * 1;
		}else{
			if($pos2 == 1){
				$x = $x * 1;
			}else{
				$x = $x * 0;
			}
		}
		
		if($_POST['txt_type_hourly'] == 'Fixed'){
			$x = $x * 1;
		}else{
			if($pos3 == 1){
				$x = $x * 1;
			}else{
				$x = $x * 0;
			}
		}
		
		//print_r($x);
		
		if ($x == 1) { 
			$sql_change = "UPDATE ".$tablename_salary." SET 
			status = '0'
			WHERE fk_employee = '".$_GET['id']."'";
			#$data->showsql($sql);
			$data->inpQueryReturnBool($sql_change);
			/*
			if($_POST['txt_calculate_type']=='Exempt'){
				$cby = $_POST['txt_calculate_by_exam'];
			}else{
				$cby = $_POST['txt_calculate_by_attendance'];
			}
			*/
			$sql = "INSERT INTO tbl_dax_personal_salary (overtime_value,fk_employee,date,daily_type,daily_formula,daily_value,weekly_type,weekly_formula,weekly_value,monthly_value,hourly_type,hourly_formula,hourly_value)
					VALUES ('".$_POST['overtime_value']."','".$_GET['id']."','".$_POST['txt_date']."', '".$_POST['txt_type_daily']."', '".$_POST['txt_daily_formula']."',
					'".$_POST['txt_daily_value']."','".$_POST['txt_type_weekly']."','".$_POST['txt_weekly_formula']."',
					'".$_POST['txt_weekly_value']."','".$_POST['txt_monthly_value']."','".$_POST['txt_type_hourly']."','".$_POST['txt_hourly_formula']."','".$_POST['txt_hourly_value']."')";
					# $data->showsql($sql);
			if ($data->inpQueryReturnBool($sql)){
				echo "<script>alert('".$data->err_report('s01')."');window.parent.close();</script>";
			}else{	
				echo "<script>alert('".$data->err_report('s02')."');</script>";	
			}
		}else{
			echo "<script>alert('".$data->err_report('s02')." *salary_monthly variables in each formula cannot change');</script>";
			
		}
	
		
#print_r($sql);
}

if ($_GET['add'] == 1){   #$data->auth('09030101',$_SESSION['user_id']);

	$rows = $data->selectQuery("select * from tbl_dax_employee where pk_id='".$_GET['id']."'");
	$fk_job = $data->get_value("select name from tbl_dax_job where pk_id='".$rows['fk_job']."'");
	$fk_department = $data->get_value("select name from tbl_dax_department where pk_id='".$rows['fk_department']."'");
	$fk_location = $data->get_value("select name from tbl_dax_location where pk_id='".$rows['fk_location']."'");
	$fk_responsible = $data->get_value("select fk_employee from tbl_dax_department_head where pk_id='".$rows['fk_responsible']."'");
	
	$hid_day_for = 'display:none;';
	$hid_day_val = 'display:none;';
	if($_POST['txt_type_daily']=='Fixed'){
		$hid_day_for = 'display:none;';
		$hid_day_val = 'display:block;';
	}elseif($_POST['txt_type_daily']=='Calculate'){
		$hid_day_for = 'display:block;';
		$hid_day_val = 'display:none;';
	}else{
		$hid_day_for = 'display:none;';
		$hid_day_val = 'display:none;';
	}
	
	$hid_week_for = 'display:none;';
	$hid_week_val = 'display:none;';
	if($_POST['txt_type_weekly']=='Fixed'){
		$hid_week_for = 'display:none;';
		$hid_week_val = 'display:block;';
	}elseif($_POST['txt_type_weekly']=='Calculate'){
		$hid_week_for = 'display:block;';
		$hid_week_val = 'display:none;';
	}else{
		$hid_week_for = 'display:none;';
		$hid_week_val = 'display:none;';
	}
	
	$hid_hour_for = 'display:none;';
	$hid_hour_val = 'display:none;';
	if($_POST['txt_type_hourly']=='Fixed'){
		$hid_hour_for = 'display:none;';
		$hid_hour_val = 'display:block;';
	}elseif($_POST['txt_type_hourly']=='Calculate'){
		$hid_hour_for = 'display:block;';
		$hid_hour_val = 'display:none;';
	}else{
		$hid_hour_for = 'display:none;';
		$hid_hour_val = 'display:none;';
	}
	
	$dataRows = array (
				'TEXT' =>  array('Date','Monthly','Daily','Weekly','Hourly','','Overtime by Unit'),
				'DOT'  => array (':',':',':',':',':'),
				'FIELD' => array (
				$data->datePicker('txt_date', $_POST[txt_date]),
				
				'<input type=text name=txt_monthly_value id=txt_monthly_value onChange="formula_salary(this.value);" value="'.$_POST[txt_monthly_value].'">',
				
				$data->cb_type_hourly_personal_salary('txt_type_daily',$_POST['txt_type_daily']," onchange='type_daily(this.value);' ").
				'<input type=text name=txt_daily_formula id=txt_daily_formula value="(salary_monthly * 12)/ 364.25" size="50" style="'.$hid_day_for.'" onChange="formula_daily(this.value);">
				<input type=text name=txt_daily_value id=txt_daily_value value="" style="'.$hid_day_val.'" onChange="daily_changed();">',
				
				$data->cb_type_hourly_personal_salary('txt_type_weekly',$_POST['txt_type_weekly']," onchange='type_weekly(this.value);' ").
				'<input type=text name=txt_weekly_formula id=txt_weekly_formula value="(salary_monthly *12) / 52" size="50" style="'.$hid_week_for.'" onChange="formula_weekly(this.value);">
				<input type=text name=txt_weekly_value id=txt_weekly_value value="" style="'.$hid_week_val.'" onChange="weekly_changed();">',
				
				$data->cb_type_hourly_personal_salary('txt_type_hourly',$_POST['txt_type_hourly']," onchange='type_hourly(this.value);' ").
				'<input type=text name=txt_hourly_formula id=txt_hourly_formula value="(salary_monthly * 12)/(52*40)" size="50" style="'.$hid_hour_for.'" onChange="formula_hourly(this.value);">
				<input type=text name=txt_hourly_value id=txt_hourly_value value="" style="'.$hid_hour_val.'" onChange="hourly_changed();">',
				
				'<font size=1><em> * </em></font><font size=1 color=red><b><u>salary_monthly</u></b></font> <font size=1>variables in each formula cannot change</font>',
				'<input type="text" name="overtime_value" id="overtime_value" value="'.$_POST[overtime_value].'">'
				
				)




						  );

    $tittle = "PERSONAL SALARY ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
}

if ($_GET['detail']==1)
{
   
	$rows_personal_salary = $data->selectQuery("select tbl_dax_personal_salary.*,DATE_FORMAT(tbl_dax_personal_salary.date,'%d-%M-%Y')as date_personal_salary from tbl_dax_personal_salary where fk_employee='".$_GET['id']."' and pk_id='".$_GET['id_personal_salary']."'");
	
	#print_r($default_absence);

	
	#print_r($rows[status]);
         $dataRows = array (
				'TEXT' =>  array('Date','Monthly Value','Daily Type','Daily Formula','Daily Value','Weekly Type','Weekly Formula','Weekly Value','Hourly Type','Hourly Formula','Hourly Value'),
				'DOT'  => array (':',':',':',':',':',':',':',':',':'),
				'FIELD' =>array ('&nbsp;'.$rows_personal_salary['date_personal_salary'],'&nbsp;'.$rows_personal_salary['monthly_value'],
				'&nbsp;'.$rows_personal_salary['daily_type'],'&nbsp;'.$rows_personal_salary['daily_formula'],'&nbsp;'.$rows_personal_salary['daily_value'],
				'&nbsp;'.$rows_personal_salary['weekly_type'],'&nbsp;'.$rows_personal_salary['weekly_formula'],'&nbsp;'.$rows_personal_salary['weekly_value'],
				'&nbsp;'.$rows_personal_salary['hourly_type'],'&nbsp;'.$rows_personal_salary['hourly_formula'],'&nbsp;'.$rows_personal_salary['hourly_value'],
				'&nbsp;'.$rows_personal_salary['overtime_value']
				)

			);

			$tittle = "PERSONAL SALARY DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}





$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>