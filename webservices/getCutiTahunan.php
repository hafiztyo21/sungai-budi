<?php
session_start();
require_once('config.php');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


if(stripos($_SERVER["CONTENT_TYPE"],"application/json")==0)
{
	$param_POST = json_decode(file_get_contents("php://input"));
}
$data = (empty($_POST['data']))?trim($param_POST->data):trim($_POST['data']);	
$data = json_decode($data);
$positionID =  ($data->JobPositionID)   ;
$departmentID =  ($data->DepartmentID)   ;
$locationID =  ($data->LocationID)   ;
$employeeID =  ($data->EmployeeID)   ;


		
$query="SELECT A.pk_id, A.full_name, A.start_date, A.fk_department, A.fk_job, A.fk_location, B.d_begin, B.dend, B.tot, ifnull(C.masal,0) masal, ifnull(C.used,0) used
FROM
(
select pk_id, full_name, start_date, fk_department, fk_job, fk_location  from tbl_dax_employee  
where 1=1 ".
(($positionID!="0")?" and  fk_job=".$positionID." ":"").
(($departmentID!="0")?" and  fk_department=".$departmentID." ":"").
(($locationID!="0")?" and  fk_location=".$locationID." ":"").
(($employeeID!="0")?" and  pk_id=".$employeeID." ":"").
") A left join 
( 
select n_employee_id, d_begin, date_add(d_end, INTERVAL n_extend_day DAY) dend, n_total_cuti+n_adjustment  tot
from tbl_dax_employee_yearly 
where date(now())>=d_begin and date(now())<=date_add(d_end, INTERVAL n_extend_day DAY)
".
(($employeeID!="0")?" and  n_employee_id=".$employeeID." ":"").
") B
ON A.pk_id=B.n_employee_id
LEFT JOIN 
(
select X.n_employee_id, sum(Y.masal) masal, sum(Y.used) used
FROM
(
select n_employee_id, d_begin, date_add(d_end, INTERVAL n_extend_day DAY) dend
from tbl_dax_employee_yearly 
where date(now())>=d_begin and date(now())<=date_add(d_end, INTERVAL n_extend_day DAY)
".
(($employeeID!="0")?" and  n_employee_id=".$employeeID." ":"").
") X left join (
select fk_employee, if(status='CMS',1,0) masal, if(status <>'CMS', 1,0) used, day_date 
from tbl_dax_attendance_status 
where status in ('AT','A','AH','CB','CT', 'CMS')
".
(($employeeID!="0")?" and  fk_employee=".$employeeID." ":"").
"
) Y on X.n_employee_id=Y.fk_employee and Y.day_date>=X.d_begin and   Y.day_date<=X.dend
group by X.n_employee_id
) C on A.pk_id=C.n_employee_id
where B.tot is not null";
		

$msg_return='';
$con = mysql_connect($host,$user,$pass);
if(!$con ){
	// error connect to mysql
	$err_msg=mysql_error();	
	$msg_return = '{ "status":"ok", "message":"'.$err_msg.'", "records":[]}';
} else {
	$conn = mysql_select_db($dbname, $con);
	if(!$conn){
		// error connect DB
		$err_msg=mysql_error();	
		$msg_return = '{ "status":"ok", "message":"'.$err_msg.'", "records":[]}';
	} else {
		//  mysql_real_escape_string()   
		$result = mysql_query($query);
		$outp="";
		while ($row = mysql_fetch_assoc($result))
		{
			if( strlen($outp)>3 ) 
			{
				$outp.=',';
			}
			$outp .= '{"id":"'  . rawurlencode($row["pk_id"]) . '",';
			$outp .= '"name":"'. rawurlencode($row["full_name"]).  '",';		
			$outp .= '"start":"'. rawurlencode($row["start_date"]).  '",';
			$outp .= '"department":"'. rawurlencode($row["fk_department"]).  '",';
			$outp .= '"job":"'. rawurlencode($row["fk_job"]).  '",';
			$outp .= '"location":"'. rawurlencode($row["fk_location"]).  '",';
			$outp .= '"begin":"'. rawurlencode($row["d_begin"]).  '",';
			$outp .= '"end":"'. rawurlencode($row["dend"]).  '",';
			$outp .= '"total":"'. rawurlencode($row["tot"]).  '",';
			$outp .= '"masal":"'. rawurlencode($row["masal"]).  '",';
			$outp .= '"used":"'. rawurlencode($row["used"]).  '",';
			$outp .= '"available":"'. rawurlencode($row["tot"]-5-$row["used"]).  '"}';
		}
		$msg_return = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	}
	mysql_close($con);
}
echo $msg_return;

?>
