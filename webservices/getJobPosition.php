<?php
session_start();
require_once('config.php');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$msg_return='';
$con = mysql_connect($host,$user,$pass);
if(!$con ){
	// error connect to mysql
	$err_msg=mysql_error();	
	$msg_return = '{ "status":"ok", "message":"'.$err_msg.'", "records":[]}';
} else {
	$conn = mysql_select_db($dbname, $con);
	if(!$conn){
		// error connect DB
		$err_msg=mysql_error();	
		$msg_return = '{ "status":"ok", "message":"'.$err_msg.'", "records":[]}';
	} else {
		$query = "SELECT pk_id, name FROM tbl_dax_job";
		//  mysql_real_escape_string()   
		$result = mysql_query($query);
		$outp="";
		while ($row = mysql_fetch_assoc($result))
		{
			if( strlen($outp)>3 ) 
			{
				$outp.=',';
			}
			$outp .= '{"id":"'  . rawurlencode($row["pk_id"]) . '",';
			$outp .= '"name":"'. rawurlencode($row["name"]).  '"}';		
		}
		$msg_return = '{ "status":"ok", "message":"", "records":['.$outp.']}';
	}
	mysql_close($con);
}
echo $msg_return;
?>
