<?php
ob_start();
session_start();
include_once 'global.inc.php';
//require_once 'include/class/global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'setting.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
$data = new setting;
$data->auth_boolean('10',$_SESSION['pk_id']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sistem Informasi Sekolah</title>
<link rel="stylesheet" type="text/css" href="menu/tree.css" />
<script type="text/javascript" src="menu/document.js"></script>
<script type="text/javascript" src="menu/tree.js"></script>
<script type="text/javascript">
    
     var objTree = new jsTree;
 	//create the root
     objTree.createRoot("menu/images/menu.jpeg", "All Menu", "welcome.php");

		//add Lead Management under the root
<?php if($data->auth_boolean(10,$_SESSION['pk_id'])){ ?>
        var objLeadManagement = objTree.root.addChild("menu/images/lead_management.jpeg", "Lead Management", "welcome.php", "mainFrame");
<?php } ?> 				
		//add Sales under the root
        var objSales = objTree.root.addChild("menu/images/sales.jpeg", "Sales", "welcome.php", "mainFrame");

		//add Customer under the root
        var objCustomer = objTree.root.addChild("menu/images/customer.jpeg", "Customer", "welcome.php", "mainFrame");

		//add Dashboard under the root
        var objDashboard = objTree.root.addChild("menu/images/dashboard.jpeg", "Dashboard", "report_all.php", "mainFrame");

		//add Campaign under the root
        var objCampaign = objTree.root.addChild("menu/images/campaign.jpg", "Campaign", "", "mainFrame");

		//add Knowledge-based under the root
        var objKnowledge = objTree.root.addChild("menu/images/knowledge.jpeg", "Knowledge-based", "", "mainFrame");

		//add Knowledge-based under the root
        var objHR = objTree.root.addChild("menu/images/human.jpeg", "Human Resource", "employee_all.php", "mainFrame");

		//add Knowledge-based under the root
        var objSetting = objTree.root.addChild("menu/images/setting.jpeg", "Setting", "admin_all.php", "mainFrame");


<?php if($data->auth_boolean(10,$_SESSION['pk_id'])){ ?>
				//var objDataSource = objLeadManagement.addChild("menu/images/datasource.jpg", "Data Source", "data_from.php", "mainFrame");
				var objLead = objLeadManagement.addChild("menu/images/lead.jpeg", "Lead", "customer_all.php", "mainFrame");
				var objTM = objLeadManagement.addChild("menu/images/tm.jpg", "Telemarketing", "telemarketer_all.php", "mainFrame");
				var objBooking = objLeadManagement.addChild("menu/images/booking.jpeg", "Booking", "booking.php", "mainFrame");
				//var objBooking = objLeadManagement.addChild("menu/images/xxx.jpeg", "Invalid Customer", "user_customer_invalid.php", "mainFrame");
<?php } ?> 
				
				var objSchedule = objSales.addChild("menu/images/schedule.jpeg", "Schedule", "schedule_all.php", "mainFrame");
				var objTour = objSales.addChild("menu/images/tour.jpeg", "Tour", "sales_all.php", "mainFrame");
				
				var objPackage = objCustomer.addChild("menu/images/package.jpeg", "Package", "package_inventory.php", "mainFrame");
				var objPaymentTemplate = objCustomer.addChild("menu/images/payment.jpeg", "Payment Template", "payment_view.php", "mainFrame");
				var objMarketing = objCustomer.addChild("menu/images/marketing.jpg", "Marketing", "marketing.php", "mainFrame");
				var objAccounting = objCustomer.addChild("menu/images/accounting.jpeg", "Accounting", "accounting_all.php", "mainFrame");
				var objCustomerList = objCustomer.addChild("menu/images/customer_list.jpeg", "Customer List", "customer_list.php", "mainFrame");



 /*  var objTree = new jsTree;        
        //create the root
        objTree.createRoot("menu/images/iconDesktop.gif", "Home", "login.php");
        
        //add My Documents under the root
        var objMyDocuments = objTree.root.addChild("menu/images/iconMyDocuments.gif", "My Documents", "nowhere.htm", "_blank");
        
        //add My Computer under the root
        var objMyComputer = objTree.root.addChild("menu/images/iconMyComputer.gif", "My Computer", "nowhere.htm", "_blank");
        
        //add My Network Places under the root
        var objMyNetworkPlaces = objTree.root.addChild("menu/images/iconMyNetworkPlaces.gif", "My Network Places", "nowhere.htm", "_blank");
        
        //the Recycle Bin under the root
        var objRecycleBin = objTree.root.addChild("menu/images/iconRecycleBin.gif", "Recycle Bin", "blank.htm", "_blank");     
        
        //add subfolders of My Documents
        var objMyEBooks = objMyDocuments.addChild("menu/images/iconFolder.gif", "My eBooks", "blank.htm", "_blank");
        var objMyMusic = objMyDocuments.addChild("menu/images/iconMyMusic.gif", "My Music", "blank.htm", "_blank");
        var objMyPictures = objMyDocuments.addChild("menu/images/iconMyPictures.gif", "My Pictures", "blank.htm", "_blank");
        
        
        //add the drives
        var objFloppyDrive = objMyComputer.addChild("menu/images/iconFloppyDrive.gif", "3� Floppy (A:)", "blank.htm", "_blank");
        var objHardDrive = objMyComputer.addChild("menu/images/iconHardDrive.gif", "Local Disk (C:)", "blank.htm", "_blank");
        var objCDROMDrive = objMyComputer.addChild("menu/images/iconCDROM.gif", "CD Drive (D:)", "blank.htm", "_blank");
        var objZipDrive = objMyComputer.addChild("menu/images/iconZipDrive.gif", "Zip 250 (E:)", "blank.htm", "_blank");
        var objCDROMDrive2 = objMyComputer.addChild("menu/images/iconCDROM.gif", "CD Drive (F:)", "blank.htm", "_blank");
        
        //add some folders under the Hard Drive
        var objDocsAndSettings = objHardDrive.addChild("menu/images/iconFolder.gif", "Documents", "blank.htm", "_blank");
        var objProgramFiles = objHardDrive.addChild("menu/images/iconFolder.gif", "Program Files", "blank.htm", "_blank");
        var objWindows = objHardDrive.addChild("menu/images/iconFolder.gif", "Windows", "blank.htm", "_blank");
        */
        function doLoad() {
            objTree.buildDOM();
        }
    
</script>
</head>

<body onload="doLoad()">

</body>
</html>
