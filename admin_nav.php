<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'user.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new user;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>

</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="100%" cellspacing="0" cellpadding="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<? if($data->auth_boolean(1510,$_SESSION['pk_id'])){  ?>
<li> <a href="user.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">User</a></li>
<? } ?>
<? if($data->auth_boolean(1511,$_SESSION['pk_id'])){  ?>
<li> <a href="level.php" target="contentTabFrame" onClick="activeLink(this);">Level</a></li>
<? } ?>
<? if($data->auth_boolean(9991512,$_SESSION['pk_id'])){  ?>
<li> <a href="combo_box_all.php" target="contentTabFrame" onClick="activeLink(this);">Combo Box</a></li>
 <? } ?>
 
 <? if($data->auth_boolean(9991513,$_SESSION['pk_id'])){  ?>
<li> <a href="admin_inbox_group.php" target="contentTabFrame" onClick="activeLink(this);">Inbox Menu Group</a></li>
 <? } ?>

 <? if($data->auth_boolean(9991514,$_SESSION['pk_id'])){  ?>
<li> <a href="outlet.php" target="contentTabFrame" onClick="activeLink(this);">Outlet</a></li>
 <? } ?>
</ul>
</td>
</tr>
 <tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
</table>
</body>
</html>