<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'schedule.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new schedule;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>

</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="100%" cellpadding="0" cellspacing="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<? if($data->auth_boolean(1610,$_SESSION['pk_id'])){  ?>
<li> <a href="schedule_print.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Sales Schedule Print</a></li>
<? } ?>
<? if($data->auth_boolean(1610,$_SESSION['pk_id'])){  ?>
<li> <a href="schedule_manager_template.php" target="contentTabFrame" onClick="activeLink(this);">Sales Schedule Template</a></li>
<? } ?>
<? if($data->auth_boolean(1611,$_SESSION['pk_id'])){  ?>
<li> <a href="schedule_reservation.php" target="contentTabFrame" onClick="activeLink(this);">Sales Reservation</a></li>
<? } ?>
<? if($data->auth_boolean(1612,$_SESSION['pk_id'])){  ?>
<li> <a href="schedule_sales.php" target="contentTabFrame" onClick="activeLink(this);">Sales Schedule</a></li>
<? } ?>
<? if($data->auth_boolean(1613,$_SESSION['pk_id'])){  ?>
<li> <a href="schedule_type_session.php" target="contentTabFrame" onClick="activeLink(this);">Type of Session</a></li>
<? } ?>

</ul>
</td>
  <tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
</tr></table>
</body>
</html>