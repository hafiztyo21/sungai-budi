<?php
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'setting.class.php';



$data = new globalFunction();
#print_r($_GET);

$view = $data->get_row("select * from tbl_dax_employee where pk_id='".$_GET['id']."'");

	$job = $data->get_value("select name from tbl_dax_job where pk_id='".$view['fk_job']."'");
	$department = $data->get_value("select name from tbl_dax_department where pk_id='".$view['fk_department']."'");
	
#print_r($view);
	
	$head_divisi = $data->get_row("SELECT tbl_dax_department_head.*,tbl_dax_employee.pk_id,tbl_dax_employee.full_name,tbl_dax_employee.fk_job as job
									FROM tbl_dax_department_head
									LEFT JOIN tbl_dax_employee ON tbl_dax_employee.pk_id = tbl_dax_department_head.fk_employee
									where tbl_dax_department_head.fk_department='".$view['fk_department']."'");

$status_alpha = $data->get_rows("SELECT *
						FROM tbl_dax_attendance_status
						WHERE fk_employee = '".$_GET['id']."' and status in('A','A-N-CT')
						AND date_format(day_date,'%m-%Y') = date_format('".date("Y-m-d")."','%m-%Y')");
$status_telat = $data->get_rows("SELECT *
						FROM tbl_dax_attendance_status
						WHERE fk_employee = '".$_GET['id']."' and status in('HT1','HT2','HT3')
						AND date_format(day_date,'%m-%Y') = date_format('".date("Y-m-d")."','%m-%Y')");
						
						
$alpha = count($status_alpha);
$telat = count($status_telat);


if(($alpha > 0) && ($telat == 0)){
$status_sp = "(Alpha sebanyak $alpha hari)";
}elseif(($alpha == 0) && ($telat > 0)){
$status_sp = "(Telat Hadir sebanyak $telat hari)";
}elseif(($alpha > 0) && ($telat > 0)){
$status_sp = "(Alpha sebanyak $alpha hari dan Telat Hadir sebanyak $telat hari)";
}else{
$status_sp = "()";
}

$attachment = $data->get_row("select * from tbl_dax_upload_sp where pk_id='".$_GET['id']."'");

#$webmaster_email = $GLOBALS['EMAILFROM']; // Who the email is from
$webmaster_email = "transforme@sat.net.id";

$subject = "Surat Peringatan"; // The Subject of the email
$to = "waone090@gmail.com"; // Who the email is too
#$to =  "'".$view[email]."'";

$headers = "From: ".$webmaster_email;
$body = "<html>
						<body>
						<span style=\" font-family:Arial, Tahoma, sans-serif\">
						\r\n<br><br>
						<table width='100%' align='center' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td height='10' colspan='6' align='center' ></td>
								  </tr>
								  <tr>
											<td height='10' colspan='10' align='center' background='templates/image/bg_menu.gif'>
											<span style='font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;'>
											SURAT PERINGATAN</span>
											
								  <tr>
											<td height='10' colspan='10' align='center' background='templates/image/bg_menu.gif'>
											<span style='font-size:16px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;'>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/PI/PERS-SB/II/".date("Y")."</span>
											</td>
								  </tr>
								  <tr>
										<td height='5px' colspan='6' align='center' ></td>
								  </tr>
								</table>
								  <div align='center' class='border'>
								<table  width='100%' border='0'>
								<tr height='30'>
								<td align='center' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>&nbsp; </span></td>
								</tr>
								
								<tr>
								<td align='center' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								Surat Peringatan ini di berikan kepada : </span></td>
								</tr>
								
								<tr height='30' colspan='2'>
								<td align='center'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>&nbsp; </span></td>
								</tr>
								
								<tr>
								<td align='center' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								Nama : ".$view[full_name]."</span></td>
								</tr>
								
								<tr>
								<td align='center' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								Jabatan : ".$job."&nbsp;&nbsp;".$department."</span></td>
								</tr>
								
								<tr height='30'>
								<td align='center' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>&nbsp; </span></td>
								</tr>
								
								<td align='justify' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif; line-height:30px;'>
								Setelah kami memperhatikan laporan absensi atas nama <strong>".$view[full_name]."</strong> yang telah melakukan pelanggaran <strong>Disiplin Waktu Kerja selama bulan ".$data->this_month(date('M'))." ".$status_sp."</strong>, maka kami berikan Surat Peringatan ke I (satu). Apabila di kemudian hari masih melakukan pelanggaran yang sama atau hal lainnya yang bertentangan dengan Peraturan yang berlaku, maka akan diberikan sanksi dan tindakan lebih lanjut.
								
								</span></td>
								</tr>
								
								<tr height='30'>
								<td align='center' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>&nbsp; </span></td>
								</tr>
								
								<tr>
								<td align='left' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								Jakarta, ".date('d').' '.$data->this_month(date('M')).' '.date('Y')."</span></td>
								</tr>
								
								<tr height='30'>
								<td align='center' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>&nbsp; </span></td>
								</tr>
								
								<tr>
								<td align='left'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								Yang Memberi Peringatan,</span></td>
								<td align='left'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								Yang Menerima Peringatan</span></td>
								</tr>
								
								<tr height='60'>
								<td align='center' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>&nbsp; </span></td>
								</tr>
								
								<tr>
								<td align='left'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								<u>SANTOSO WINATA</u></span></td>

								<td align='left'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								".$view[full_name]."</span></td>
								</tr>
								
								<tr>
								<td align='left' colspan='2'><span style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
								PIMPINAN PERUSAHAAN</span></td>
								</tr>
								
								</table>
									
								
								
							</table>
							
			
						\r\n
						
						</body>
						</html>"; // Message that the email has in it


// boundary 
$semi_rand = md5(time()); 
$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
 
// headers for attachment 
$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 


// multipart boundary 
$body = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $body . "\n\n"; 
$body .= "--{$mime_boundary}\n";
 
///////////////////////////////// attachment pertama ///////////////////////////////// 

$fileatt =  $GLOBALS['WWW']."upload_sp/".$_GET['id'].".doc";
$fileatt_type = "application/doc"; // File Type
$fileatt_name = "$attachment[file_name].doc"; // Filename that will be used for the file as the attachment

$file = fopen($fileatt,'rb');
$data = fread($file,filesize($fileatt));
fclose($file);

$data = chunk_split(base64_encode($data));
$body .= "--{$mime_boundary}\n" .
"Content-Type: {$fileatt_type};\n" .
" name=\"{$fileatt_name}\"\n" .
"Content-Transfer-Encoding: base64\n\n" .
$data . "\n\n" .
"--{$mime_boundary}\n";
unset($data);
unset($file);
unset($fileatt);
unset($fileatt_type);
unset($fileatt_name);


// send email
$ok = mail($to, $subject, $body, $headers);


if($ok) {
 /*echo "<script>window.location = 'individu_10.php?id=".$_GET['id']."';</script>";*/
 echo "<script>alert('Send Email Success');</script>";
 echo "<script>window.parent.close();</script>";
} else {
#die("Sorry but the email could not be sent. Please go back and try again!");
#print_r($body);
 echo "<script>alert('Send Email Failed!');</script>";
 echo "<script>window.parent.close();</script>";
/*  */

}	


?>