<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'employee.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';
require_once $GLOBALS['CLASS'].'warehouse.class.php';

$data = new employee;
?>

<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="include/css/menu_tab.css"/>
<title>activeLink</title>
</head>
<body id="activelink" onLoad="">
<script type="text/javascript">

//Sets active link for ul.li.a
function activeLink(objLink)
{ var list = document.getElementById('videoList').getElementsByTagName('a');
for (var i = list.length - 1; i >= 0; i--){
list[i].className='nonActiveVid';
};
objLink.className= 'activeVid';
}
</script>
<table width="100%" cellpadding="0" cellspacing="0"><tr>
<td >
<ul class="activeVid" id="videoList">
<? if($data->auth_boolean(1610,$_SESSION['pk_id'])){  ?>
<li> <a href="reimbursement_data.php" target="contentTabFrame" class="activeVid" onClick="activeLink(this);">Reimbursement Data</a></li>
 <? } ?>
<? if($data->auth_boolean(1611,$_SESSION['pk_id'])){  ?>
<li> <a href="reimbursement_pengobatan.php" target="contentTabFrame" onClick="activeLink(this);">Reimbursement Pengobatan</a></li>
 <? } ?>
<? if($data->auth_boolean(1612,$_SESSION['pk_id'])){  ?>
<li> <a href="reimbursement_bersalin.php" target="contentTabFrame" onClick="activeLink(this);">Reimbursement Bersalin</a></li>
 <? } ?>
<? if($data->auth_boolean(1613,$_SESSION['pk_id'])){  ?>
<li> <a href="reimbursement_kacamata.php" target="contentTabFrame" onClick="activeLink(this);">Reimbursement Kaca Mata</a></li>
 <? } ?>
<? if($data->auth_boolean(1614,$_SESSION['pk_id'])){  ?>
<li> <a href="golongan.php" target="contentTabFrame" onClick="activeLink(this);">Golongan</a></li>
 <? } ?>
<? if($data->auth_boolean(1615,$_SESSION['pk_id'])){  ?>
<li> <a href="kategori.php" target="contentTabFrame" onClick="activeLink(this);">Kategori</a></li>
 <? } ?>
 
<!-- start tambah menu report reimbusment tahunan 20121106 bagas-->
 <? if($data->auth_boolean(1615,$_SESSION['pk_id'])){  ?>
<li> <a href="reimbursement_tahunan.php" target="contentTabFrame" onClick="activeLink(this);">Report Reimbursement</a></li>
 <? } ?>
<!-- end tambah menu report reimbusment tahunan 20121106 bagas-->


</ul>
</td>
  <tr>
    <td height="20" colspan="0" valign="top" bgcolor="#bababa" class="container"><!--DWLayoutEmptyCell-->&nbsp;</td>
  </tr>
</tr>
</table>
</body>
</html>