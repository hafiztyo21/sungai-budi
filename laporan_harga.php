<?php
session_start();
#print_r($_SESSION['user_id']);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
#require_once $GLOBALS['CLASS'].'chart.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
#$data->auth('030702',$_SESSION['user_id']);

$data->ResultsPerPage = 10;
$data->LinksPerPage = 10;


$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('laporan_harga.html');

if ($_POST['search'] )
{

	$date_from = $_POST['date_from'];
	$date_to = $_POST['date_to'];
	$cb_sales = $_POST['txt_kode_gerai'];
    $cbx_sales = (empty($cb_sales))? "LIKE '%".$cb_sales."%'": "= '".$cb_sales."'";


	$printLink='laporan_hargaPrint.php';
	$printLink = "<img src='image/print.gif' height='12' width='12'><a href='#' onclick=openWin1('".$printLink."?print=1&date_from=$date_from&date_to=$date_to')>PRINT</a>";

	if($data->greaterDate($date_from, $date_to))
	{
		$date_from = $_POST['date_to'];
		$date_to = $_POST['date_from'];
	}

	if ($date_from == $date_to)
	{	$periode = "PERIODE: ".date('d M Y', strtotime($date_from));		}
	else
	{	$periode = "PERIODE: ".date('d M Y', strtotime($date_from)).' - '.date('d M Y', strtotime($date_to));	}


	$sql = "SELECT kode_gerai,nama_gerai FROM tbl_upx_gerai
			WHERE kode_gerai ".$cbx_sales."";


    $_SESSION['sql'] = $sql;
	$_SESSION['periode'] = $periode;
	$_SESSION['date_from'] = $date_from;
	$_SESSION['date_akhir'] = $date_to;
}
else if (($_SESSION['sql']) and ($_GET))
{
	$sql = $_SESSION['sql'];
	$periode = $_SESSION['periode'];
	$date_from = $_SESSION['date_from'];
	$date_to = $_SESSION['date_to'];
}
else
{
	$_SESSION['sql'] = '';
	$_SESSION['periode'] = '';
	$_SESSION['date_from'] = '';
	$_SESSION['date_to'] = '';
	$sql = '';
}

#echo $sql;
/*$date_temp = '';

if (!empty($sql))
{
	$pg = ($_POST['search'] )? 1 : $_GET['page'];
	$DG = $data->dataGridBase($sql, $data->ResultsPerPage, $pg);

	if (!empty($DG))
	{
		$total_type = array('biaya_kirim' => 0,
							'total_charge' => 0,
							'total_diskon' => 0,
							'total_biaya' => 0
							);
		$type_temp = '';

		foreach ($DG as $w => $det)
		{
			$bgcolor = (($w % 2) == 1)? '#fdd6ad': '#ffffff';

			if ($w > 0 && $type_temp != $det['kode_gerai'])
			{
				$total_type['jumlah_koli'] = $det['jumlah_koli'];
				$total_type['biaya_kirim'] = $det['biaya_kirim'];
				$total_type['total_charge'] = $det['total_charge'];
				$total_type['total_diskon'] = $det['total_diskon'];
				$total_type['total_biaya'] = $det['total_biaya'];

			}
			else
			{
				$total_type['jumlah_koli'] += $det['jumlah_koli'];
				$total_type['biaya_kirim'] += $det['biaya_kirim'];
				$total_type['total_charge'] += $det['total_charge'];
				$total_type['total_diskon'] += $det['total_diskon'];
				$total_type['total_biaya'] += $det['total_biaya'];

			}

			$type_temp = $det['kode_gerai'];

			$list[]['TABLE_LIST'] .=
						"<tr bgcolor='".$bgcolor."'>
							 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;' align='center'>".($w+1)."</td>
							 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['kode_gerai']."</td>
            				 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['nama_gerai']."</td>
          					 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['kota']."</td>
            				 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['no_conote']."</td>
            				 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['jumlah_koli']."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['biaya_kirim'], '', 0)."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['total_charge'], '')."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['total_diskon'], '')."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['total_biaya'], '')."</td>

						 </tr>";

			if ($det['kode_gerai'] != $DG[$w+1]['kode_gerai'])
			{
				$list[]['TABLE_LIST'] .=
					"<tr class='tr_footer'>
          				<td style='padding:2;' colspan='5' align='right'>TOTAL PER GERAI</td>
              			<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['jumlah_koli'], '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['biaya_kirim'], '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['total_charge'], '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['total_diskon'], '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['total_biaya'], '')."</td>

					</tr>";
			}
		}

		$list[]['TABLE_LIST'] .=
				"<tr class='tr_footer'>
          				<td style='padding:2;' colspan='5' align='right'>TOTAL</td>
                        <td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'jumlah_koli'), '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'biaya_kirim'), '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'total_charge'), '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'diskon'), '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'total_biaya'), '')."</td>

				</tr>";
	}
} */

#echo '<pre>';
//print_r();
#echo '</pre>';


$date_temp = '';

if (!empty($sql))
{
	$pg = ($_POST['search'] )? 1 : $_GET['page'];
	$DG = $data->dataGridBase($sql, $data->ResultsPerPage, $pg);

	if (!empty($DG))
	{
		foreach ($DG as $d => $grid)
		{
			$list[$d]['SALESMAN'] = 'GERAI'.' : ('.$grid['kode_gerai'].') '.$grid['nama_gerai'];


			$sql_dg = "SELECT tbl_upx_transaksi.kode_gerai, tbl_upx_gerai.nama_gerai, tbl_upx_gerai.kota, tbl_upx_transaksi.no_conote,
			tbl_upx_transaksi.jumlah_koli,tbl_upx_transaksi.biaya_kirim,
			sum(tbl_upx_transaksi.charge * tbl_upx_transaksi.biaya_kirim) as total_charge,
			sum(tbl_upx_transaksi.diskon * tbl_upx_transaksi.biaya_kirim) as total_diskon,
			tbl_upx_transaksi.total_biaya
	 		 FROM tbl_upx_transaksi, tbl_upx_gerai
			 WHERE tbl_upx_transaksi.kode_gerai = tbl_upx_gerai.kode_gerai
			 AND tbl_upx_transaksi.tgl BETWEEN '".$date_from."' AND '".$date_to."'
			 AND tbl_upx_transaksi.kode_gerai = '".$grid['kode_gerai']."'
		     GROUP BY tbl_upx_transaksi.kode_gerai, tbl_upx_gerai.nama_gerai, tbl_upx_transaksi.no_conote
	 		 ORDER BY tbl_upx_transaksi.kode_gerai, tbl_upx_transaksi.no_conote";
           #print_r($sql_dg);

			 $sql_dg = "SELECT tbl_upx_transaksi.kode_gerai, tbl_upx_gerai.nama_gerai, tbl_upx_gerai.kota, tbl_upx_transaksi.no_conote,
			tbl_upx_transaksi.jumlah_koli,tbl_upx_transaksi.biaya_kirim, sum(tbl_upx_transaksi.charge * tbl_upx_transaksi.biaya_kirim) as total_charge,
			sum(tbl_upx_transaksi.diskon * tbl_upx_transaksi.biaya_kirim) as total_diskon,
			tbl_upx_transaksi.total_biaya
	 		 FROM tbl_upx_transaksi, tbl_upx_gerai
			 WHERE tbl_upx_transaksi.kode_gerai = tbl_upx_gerai.kode_gerai
			 AND tbl_upx_transaksi.tgl BETWEEN '".$date_from."' AND '".$date_to."'
			 AND tbl_upx_transaksi.kode_gerai = '".$grid['kode_gerai']."'
		     GROUP BY tbl_upx_transaksi.kode_gerai, tbl_upx_gerai.nama_gerai, tbl_upx_transaksi.no_conote
	 		 ORDER BY tbl_upx_transaksi.kode_gerai, tbl_upx_transaksi.no_conote";

			 //echo $sql_dg.'<br>';
			$details = $data->get_rows($sql_dg);
			$list[$d]['TABLE_LIST'] = '';
			$list[$d]['TABLE_LIST'] .=
					"<table border = '0' cellpadding='2' cellspacing='2' width='100%'>
							<tr bgcolor='#fdd6ad' align='center'><td align='center'><b>No</b></td>
								<td align='center'><b>KODE GERAI</b></td>
            					<td align='center'><b>NAMA GERAI</b></td>
								<td align='center'><b>KOTA</b></td>
          						<td align='center'><b>NO CONOTE</b></td>
            					<td align='center'><b>JUMLAH KOLI</b></td>
								<td align='center'><b>BIAYA KIRIM</b></td>
								<td align='center'><b>CHARGE</b></td>
            					<td align='center'><b>DISKON</b></td>
								<td align='center'><b>TOTAL BIAYA</b></td>
							</tr>";


			if (!empty($details))
			{
				$total_type = array('biaya_kirim' => 0,
									'total_charge' => 0,
									'total_diskon' => 0,
									'total_biaya' => 0
							);
				$type_temp = '';

				foreach($details as $w => $det)
				{
					$bgcolor = (($w % 2) == 1)? '#ffffff': '#e6f0fc';

					if ($w > 0 && $type_temp != $det['kode_gerai'])
						{
							$total_type['jumlah_koli'] = $det['jumlah_koli'];
							$total_type['biaya_kirim'] = $det['biaya_kirim'];
							$total_type['total_charge'] = $det['total_charge'];
							$total_type['total_diskon'] = $det['total_diskon'];
							$total_type['total_biaya'] = $det['total_biaya'];

						}
					else
						{
							$total_type['jumlah_koli'] += $det['jumlah_koli'];
							$total_type['biaya_kirim'] += $det['biaya_kirim'];
							$total_type['total_charge'] += $det['total_charge'];
							$total_type['total_diskon'] += $det['total_diskon'];
							$total_type['total_biaya'] += $det['total_biaya'];

						}

					$type_temp = $det['kode_gerai'];

					$list[$d]['TABLE_LIST'] .=
						"<tr bgcolor='".$bgcolor."'>
							 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;' align='center'>".($w+1)."</td>
							 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['kode_gerai']."</td>
            				 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['nama_gerai']."</td>
          					 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['kota']."</td>
            				 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['no_conote']."</td>
            				 <td style='padding-bottom:1; padding-left:2; padding-right:1; padding-top:1;'>".$det['jumlah_koli']."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['biaya_kirim'], '', 0)."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['total_charge'], '')."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['total_diskon'], '')."</td>
							 <td align='right' style='padding-bottom:1; padding-left:2; padding-right:2; padding-top:1;'>".$data->convert_to_money($det['total_biaya'], '')."</td>

						 </tr>";

					if ($det['kode_gerai'] != $DG[$w+1]['kode_gerai'])
					{
						$list[$d]['TABLE_LIST'] .=
						"<tr class='tr_footer'>
          				<td style='padding:2;' colspan='5' align='right'>TOTAL</td>
              			<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['jumlah_koli'], '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['biaya_kirim'], '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['total_charge'], '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['total_diskon'], '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($total_type['total_biaya'], '')."</td>

					</tr>";
					}
				}
			}

			/*$list[$d]['TABLE_LIST'] .=
					"<tr class='tr_footer'>
          				<td style='padding:2;' colspan='5' align='right'>TOTAL</td>
                        <td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'jumlah_koli'), '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'biaya_kirim'), '', 0)."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'total_charge'), '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'diskon'), '')."</td>
						<td align='right' style='padding:2;'>".$data->convert_to_money($data->sum_subarrays_by_key($DG, 'total_biaya'), '')."</td>

					</tr>";
				$list[$d]['TABLE_LIST'] .= "</table>";  */


		}
	}
}

#echo '<pre>';
#print_r($_POST);
#echo '</pre>';


$sql_sales = "SELECT kode_gerai,nama_gerai FROM tbl_upx_gerai ORDER BY nama_gerai";
$sales_list = $data->get_rows($sql_sales);
$arrFields = array('' => 'All');

if (!empty($sales_list))
{
	foreach ($sales_list as $s => $sl)
	{
		$arrFields[$sl['kode_gerai']] = $sl['nama_gerai'];
	}
}

$salesman = 'GERAI: '.$data->cb_select('txt_kode_gerai', $arrFields, $cb_sales);

$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);


/*$include = array
		(
				'INCLUDEAJAXJS' => $GLOBALS['JS'].'ajax.js',
				'INCLUDEJSVALJS' => $GLOBALS['JS'].'jsval.js'
		); */


$tmpl->addRows('list', $list);
$tmpl->addVar('page', 'PRINT',$printLink);

$tmpl->addVar('date', 'SALESMAN', $salesman);
$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVar('date', 'DATE_TO', $data->datePicker('date_to', $date_to));
$tmpl->addVar('page', 'PERIODE', $periode);
//$tmpl->addVars('button',$button);
//$tmpl->addVar('paging', 'paging_no',$paging_no);
//$tmpl->addVar('legend', 'page',$page_info);
//$tmpl->addVar('legend', 'result',$result_info);
//$tmpl->addVar('search', 'search',$searchCB);
$tmpl->addRows('row',$DG );
$tmpl->addVars('path',$path);
#$tmpl->addVars('include',$include);
$tmpl->displayParsedTemplate('page');

?>