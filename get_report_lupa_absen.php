<?php
session_start();
#session_destroy();
#print_r($_SESSION);
#print_r($_POST);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'report.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new report;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('get_report_lupa_absen.html');


####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='full_name';//default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
if($data->auth_boolean(121110,$_SESSION['pk_id'])){
	$link = 'batch_add.php';
	$addLink = "<a href='batch.php?page=".$_GET[page]."' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:600px;dialogHeight:500px')>ADD</a>";
	$tmpl->addVar('page','add',$addLink);
}
#############################    PILIH STATUS     ##########################################
$rowb = $data->get_row("select * from tbl_dax_batch where pk_id='".$_POST[txt_fk_batch]."'");
$b_name =  $rowb[name];
$txt_from =  $rowb[start_period];
$txt_to =  $rowb[end_period];



    $report_option = "<tr> <td>Report</td><td>:</td><td colspan='4'>".$data->cb_report_uang_makan('txt_report_uang_makan',$_POST[txt_report_uang_makan])."</td>  </tr>";
 	$tmpl->addVar('page','report_option',$report_option);
    $tmpl->addVar('page','status_custom',$status_custom);
	$tmpl->addVar('page','status_in',$status_in);
	$tmpl->addVar('page','status_not_in',$status_not_in);






if ($_POST['btn_search'])
{
	
	$sql = "SELECT A.fk_employee PK_ID, A.day_date, if(DATE_FORMAT(A.date_in,'%H:%i:%s')='00:00:00','-',DATE_FORMAT(A.date_in,'%H:%i:%s')) att_in, if(DATE_FORMAT(A.date_out,'%H:%i:%s')='00:00:00','-',DATE_FORMAT(A.date_out,'%H:%i:%s')) att_out, B.full_name, C.name,A.status, DAYOFWEEK(A.day_date) dow
FROM tbl_dax_attendance_status A LEFT JOIN tbl_Dax_employee B
ON A.fk_employee=B.pk_id LEFT JOIN tbl_dax_department C
ON B.fk_department=C.pk_id
WHERE ((DATE_FORMAT(A.date_in,'%H:%i:%s')='00:00:00' and DATE_FORMAT(A.date_out,'%H:%i:%s')<>'00:00:00') or  (DATE_FORMAT(A.date_in,'%H:%i:%s')<>'00:00:00' and DATE_FORMAT(A.date_out,'%H:%i:%s')='00:00:00') ) and A.status in ('HT2','AH') and  A.day_date>='$txt_from' and A.day_date<='$txt_to'";

#print_r($sql);
#print_r($filter_emp);
	$_SESSION['sql']=$sql;


	$data->ResultsPerPage = 1000;
	$DG  = $data->DGLupaAbsen($txt_from,$txt_to,$sql,'pk_id',$data->ResultsPerPage,$pg,'view',$link,'tambah',$link,'edit',$link,'delete',$link);
	
	 
}

#-- PAXLINK
$tmpl->addVar('loopdata','txt_all',"<input type='checkbox' name='approve[]' value='".$DG[0][pk_id]."'>");

//<input type="checkbox" name="approve[]" value="{PK_ID}" />

if($data->auth_boolean(122110,$_SESSION['pk_id'])){
	$print = "&nbsp;<input type='button' name='btprint' value='Print Lupa Absen' onclick=\"getUangMakanPrint('".$_POST[txt_department]."','".$_POST[txt_location]."','".$_POST[status_custom]."','".$_POST[txt_custom]."','".$txt_from."','".$txt_to."','".$_POST[txt_report_uang_makan]."')\">";
}



$pg = ($_POST['btn_search'])? 1 : $_GET['page'];


#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
        # $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		$paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }


###############################################################################################
$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);

$arrFields = array('0' => ' - All - ','A'=>'Alpha (A)','H'=>'Hadir (H)','HE'=>'Hadir hasil edit (HE)',
			'CB'=>'Cuti Besar (CB)','CT'=>'Cuti Tahunan (CT)','CB'=>'Cuti Dasar (CB)',
			'CH'=>'Cuti Haid (CH)','T'=>'Tugas (T)',
			'TLK'=>'Tugas Luar Kota (TLK)','IAC' =>'Ijin Akumulasi Cuti (IAC)',
			'IAS' =>'Ijin Akumulasi Potong 50% (IAC)','IAF' =>'Ijin Akumulasi Potong 100% (IAF)',
				'HT1' =>'Hadir Terlambat < 30 menit (HT1)',
				'HT2' =>'Hadir Terlambat > 30 menit (HT2)',
				'HT3' =>'Hadir, Pulang Cepat (HT3)',
				'S' =>'Sakit (S)',
				'TLK' =>'Tugas Luar Kota (TLK)'
			);
$tmpl->addVar('page','txt_name',$data->cb_employee_all('txt_name',$_POST[txt_name]));
$tmpl->addVar('page','cb_location',$data->cb_location_search('txt_location',$_POST[txt_location]));
$tmpl->addVar('page','cb_department',$data->cb_department_search('txt_department',$_POST[txt_department]));



$tmpl->addVar('page','from',$data->datePicker('txt_from',$_POST[txt_from]));
$tmpl->addVar('page','to',$data->datePicker('txt_to',$_POST[txt_to]));
$total_all = 0;
for($i=0;$i<count($DG);$i++){
$total_all = $total_all + $DG[$i][TOTAL];
}

$tmpl->addVar('page','total_all',$data->convert_to_money($total_all,'Rp. '));

$tmpl->addRows('loopData',$DG);
$tmpl->addVar('page','add',$addLink);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);
$tmpl->addVar('page', 'print',$print);
$tmpl->addVar('page', 'send_email',$send_email);
$tmpl->addVar('page','cb_location',$data->cb_location_search('txt_location',$_POST[txt_location]));
$tmpl->addVar('page','cb_department',$data->cb_department_search('txt_department',$_POST[txt_department]));
$tmpl->addVar('page','txt_batch',"<input type='hidden' name='txt_fk_batch' value='".$_POST[txt_fk_batch]."'>$b_name");
$tmpl->addVar('page','rb_status',$status_report);
$tmpl->addVar('page','rb_all',$rb_all);
$tmpl->displayParsedTemplate('page');
?>
