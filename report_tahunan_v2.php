<!doctype html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title>Report Tahunan</title>
	<link href="styles/styles.css" rel="stylesheet">
	<link href="node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="node_modules/angular-ui-grid/ui-grid.min.css">
	<link href="styles/fontawesome-all.min.css" type="text/css" rel="stylesheet" />
	<link href="node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css" type="text/css" rel="stylesheet" />
	
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/tooltip.js/dist/umd/tooltip.min.js"></script>
	
	<script src="node_modules/angular/angular.min.js"></script>
	<script src="node_modules/angular/angular-animate.js"></script>
	<script type="text/javascript" src="node_modules/grunt/csv.js"></script>
    <script type="text/javascript" src="node_modules/grunt/pdfmake.js"></script>
    <script type="text/javascript" src="node_modules/grunt/vfs_fonts.js"></script>		
	<script type="text/javascript" src="js/apps.js"></script>	
	<script type="text/javascript" src="js/report_tahunan.js"></script>	
	<script src="node_modules/angular-ui-grid/ui-grid.min.js"></script>
	<script src="node_modules/jquery/dist/jquery.min.js"></script>
	<script src="node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js"></script>
	
	<!--
	
	
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>	
	-->
</head>
<body ng-app="SungaiBudi">

	<div id="mainContainer" ng-controller="mainCtrl">		
		
			<div style="text-align:center;padding-left:15px;font-size:20px;font-weight: bold;padding-bottom:5px;font-family: 'Montserrat';color:#5b4fe8;">Cuti Tahunan</div>
			<p class="input-group" style="margin-left:15px;margin-bottom:0px;width:20%">
			
				<table width="100%">
					<tr>
						<td style="padding-left:10px;">Name</td>
						<td>
							<select  ng-model="data.EmployeeID" style="width:90%;">
								<option value="0" >- ALL -</option>
								<option ng-repeat="option in listEmployee" value="{{option.id}}">{{option.name}}</option>
							</select>
						</td>
						<td>&nbsp;</td>
						<td style="padding-left:10px;">Location</td>
						<td>
							<select   ng-model="data.LocationID" style="width:90%;">
								<option value="0" >- ALL -</option>
								<option ng-repeat="option in listLocation" value="{{option.id}}">{{option.name}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td style="padding-left:10px;">Department</td>
						<td>
							<select   ng-model="data.DepartmentID" style="width:90%;">
								<option value="0" >- ALL -</option>
								<option ng-repeat="option in listDepartment" value="{{option.id}}">{{option.name}}</option>
							</select>
						</td>
						<td>&nbsp;</td>
						<td style="padding-left:10px;">Job Position</td>
						<td>
							<select   ng-model="data.JobPositionID" style="width:90%;">
								<option value="0" >- ALL -</option>
								<option ng-repeat="option in listJobPosition" value="{{option.id}}">{{option.name}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="5" style="padding-left:10px;">
							<button type="button" class="btn btn-default" ng-click="refresh()">Search</button>
						</td>
					</tr>
				</table>
				
			</p>	
			
			<div class="container-fluid" id="main">
				<div id="grid1" ui-grid="gridOrdersOptions" class="grid" ui-grid-resize-columns ui-grid-auto-resize  ui-grid-exporter ng-style="{'height': getHeight()+'px'}">
					<div class="grid-nodata-container" ng-show="gridOptions.data.length == 0 && !gridIsLoading">No Data Available</div>
					<div class="grid-loading-container" ng-show="gridIsLoading">
						<img src="image/loading.gif" />
					</div>
				</div>
			</div>
		
	</div>

</body>
</html>