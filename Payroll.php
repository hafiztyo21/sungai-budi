<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'batch.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new batch;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('Payroll.html');


if( isset($_GET['del']) ) {
  if($_GET['del']==1){

    $sql = "delete from tbl_dax_batch_payroll  where pk_id='".$_GET['id']."'";
    $sql1 = "delete from tbl_dax_batch_calculate  where fk_batch_payroll='".$_GET['id']."'";
      #$data->showsql($sql);
     if ($data->inpQueryReturnBool($sql))
    { echo "<script>alert('".$data->err_report('d01')."');window.close();</script>";  }
    else
    { echo "<script>alert('".$data->err_report('d02')."');</script>"; }
    if ($data->inpQueryReturnBool($sql1))
    { echo "<script>alert('".$data->err_report('d01')."');window.close();</script>";  }
    else
    { echo "<script>alert('".$data->err_report('d02')."');</script>"; }
  }
}

#$addLink = "<a href='user.php' onclick=show_modal('TEL.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";
#$cekLink = "<a href='user.php' onclick=show_modal('user_detail.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('detail')." </a>";

####################################sorting##############################
if( isset($_POST['order_by'])) {
  if ($_POST['order_by']){
    $order_by=$_POST['order_by'];
  }else{
    $order_by='pk_id';//default
  }
} else {
  $order_by='pk_id';//default
}
if ($_POST['sort_order']){
  $sort_order=$_POST['sort_order'];
}else{
  $sort_order='desc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
$link = 'batch_payroll_add.php';
if($data->auth_boolean(1110,$_SESSION['pk_id'])){ 

$addLink = "<a href='Payroll.php?page=".$_GET[page]."' onclick=popup('".$link."?add=1','status:no;help:no;dialogWidth:600px;dialogHeight:500px')>ADD</a>";
$tmpl->addVar('page','add',$addLink);
}
if ($_POST['btn_search'] )
{ 
  if($_SESSION["show"]=="open"){
    $condition=" and status != 'Close'";
  }
	$id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];

	$sql = "SELECT tbl_dax_batch_payroll.*,DATE_FORMAT(start_period,'%d-%M-%Y') as vstart_period,
    DATE_FORMAT(end_period,'%d-%M-%Y') as vend_period,
    DATE_FORMAT(pay_date,'%d-%M-%Y') as vpay_date
     FROM (".$data->get_role('BATCH',$_SESSION[pk_id]).") tbl_dax_batch_payroll  WHERE   upper($id_serach) like upper('%$q_serach%')".$condition ;


	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and (count($_GET)>1))
{
    $sql = $_SESSION['sql'];
}
else
{
	$_SESSION['sql']='';
	$sql =  "SELECT tbl_dax_batch_payroll.*,DATE_FORMAT(start_period,'%d-%M-%Y') as vstart_period,
    DATE_FORMAT(end_period,'%d-%M-%Y') as vend_period,
    DATE_FORMAT(pay_date,'%d-%M-%Y') as vpay_date FROM
  (".$data->get_role('BATCH',$_SESSION[pk_id]).")tbl_dax_batch_payroll ".$condition." order by $order_by $sort_order";
}

$arrFields = array(
		'name'=>'NAME',
		'description'=>'DESCRIPTION'
);

#$data->showsql($sql);
#print_r($sql);
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridBatch($sql,'pk_id',$data->ResultsPerPage,$pg,'view',$link,'tambah',$link,'edit',$link,'delete',$link);
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
        # $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		$paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

   if($_SESSION['show']=="open"||strlen($_SESSION['show'])==0){
    $allopen='<a href="javascript:void(0)" onclick="allopen(\'1\')">Show All</a>';
  }else{
    $allopen='<a href="javascript:void(0)" onclick="allopen(\'1\')">Show Open</a>';
  }
###############################################################################################


// $tmpl->addRows('loopData',$DG);
// $tmpl->addVar('page','add',$addLink);
// $tmpl->addVar('legend', 'page',$page_info);
// $tmpl->addVar('legend', 'result',$result_info);
// $tmpl->addVar('paging', 'paging_no',$paging_no);
// $tmpl->addVar('page', 'search',$searchCB);



$tmpl->addRows('loopData',$DG);
$tmpl->addVar('page','allopen',$allopen);
$tmpl->addVar('page','add',$addLink);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);


//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>
