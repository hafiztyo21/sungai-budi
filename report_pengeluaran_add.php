<?php
session_start();
#session_destroy();
//print_r($_SESSION);
#print_r($_GET);
#print_r($_POST);

require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'nota.class.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

#$data = new globalFunction;
$data = new nota;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('report_pengeluaran_add.html');
$tablename = 'tbl_dax_nota_pengeluaran';



if ($_POST['btn_save'])
{

		$sql_memo = "UPDATE tbl_dax_nota_memo SET
						status = '1',datecreated=now(),usercreated='".$_SESSION[pk_id]."'
						WHERE fk_nota = '".$_GET['max_id']."'";
		#$data->showsql($sql_memo);
		if (!$data->inpQueryReturnBool($sql_memo)){
			echo "<script>alert('".$data->err_report('s02')."');</script>";
		}

	$txt_unit_usaha = $_POST['txt_unit_usaha1']."-".$_POST['txt_unit_usaha2']."-".$_POST['txt_unit_usaha3']."-".$_POST['txt_unit_usaha4'];
	$txt_kode_bukti = $_POST['txt_kode_bukti1']."-".$_POST['txt_kode_bukti2'];
	$txt_no_bukti = $_POST['txt_no_bukti1']."-".$_POST['txt_no_bukti2']."-".$_POST['txt_no_bukti3']."-".$_POST['txt_no_bukti4']."-".$_POST['txt_no_bukti5']."-".$_POST['txt_no_bukti6'];

	$sql = "INSERT INTO tbl_dax_nota_pengeluaran
			(unit_usaha,jenis_pengeluaran,kode_bukti,no_bukti_1,
			no_ac,no_cekgiro,date_tempo,value_cekgiro,memo,
			value,keterangan,no_perkiraan,value_debet,value_kredit,
			paid_to,accepted_by,date_accepted,pemohon,diperiksa,
			fk_currency,ka_bag,datecreated,usercreated)
	 		VALUES
			('".$txt_unit_usaha."', '".$_POST['type']."','".$txt_kode_bukti."','".$txt_no_bukti."',
			'".$_POST['txt_no_ac']."','".$_POST['txt_no_cek_giro']."','".$_POST['txt_jatuh_tempo']."','".$_POST['txt_value_cek_giro']."','".$_POST['txt_memo']."',
			'".$_POST['txt_value']."','".$_POST['txt_keterangan']."','".$_POST['txt_no_perkiraan']."','".$_POST['txt_value_debet']."','".$_POST['txt_value_kredit']."',
			'".$_POST['txt_paid_to']."','".$_POST['txt_accept_by']."','".$_POST['txt_date_accept']."','".$_POST['txt_pemohon']."','".$_POST['txt_diperiksa']."',
			'".$_POST['txt_kabag']."','".$_POST['txt_currency']."',now(),'".$_SESSION[pk_id]."')";
			# $data->showsql($sql);
	if ($data->inpQueryReturnBool($sql)){
		echo "<script>alert('".$data->err_report('s01')."');window.parent.close();</script>";
	}else{
		echo "<script>alert('".$data->err_report('s02')."');</script>";
	}

#print_r($sql);
}



if ($_GET['add'] == 1)
{   #$data->auth('09030101',$_SESSION['user_id']);

	$id = $data->nextval('tbl_dax_nota_pengeluaran','pk_id');

	$dataRows = array (
				'TEXT' =>  array('NO.','Beban Unit Usaha','Type','Kode Bukti','No. Bukti','A/C No/','Cek/Giro No.'
								,'Tanggal Jatuh Tempo','Jumlah Nilai Cek/Giro','Mata Uang'),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
				'FIELD' => array (
				"<input type='hidden' name='txt_id' value='".$id."'>".substr('000000'.$id,-7,7),
				"<input type='text' name='txt_unit_usaha1' maxlength='1' size='1' value='".$_POST[txt_unit_usaha1]."'>
				 <input type='text' name='txt_unit_usaha2' maxlength='1' size='1' value='".$_POST[txt_unit_usaha2]."'>
				 <input type='text' name='txt_unit_usaha3' maxlength='1' size='1' value='".$_POST[txt_unit_usaha3]."'>
				 <input type='text' name='txt_unit_usaha4' maxlength='1' size='1' value='".$_POST[txt_unit_usaha4]."'>",
				$data->radio('type',array('1' => 'KAS KECIL', '2' => 'KAS', '3' => 'BANK'),'v',$_POST[type],"onclick='document.formaddType.submit()'"),
				"<input type='text' name='txt_kode_bukti1' maxlength='1' size='1' value='".$_POST[txt_kode_bukti1]."'>
				 <input type='text' name='txt_kode_bukti2' maxlength='1' size='1' value='".$_POST[txt_kode_bukti2]."'>",
				"<input type='text' name='txt_no_bukti1' maxlength='1' size='1' value='".$_POST[txt_no_bukti1]."'>
				 <input type='text' name='txt_no_bukti2' maxlength='1' size='1' value='".$_POST[txt_no_bukti2]."'>&nbsp;&nbsp;&nbsp;
				 <input type='text' name='txt_no_bukti3' maxlength='1' size='1' value='".$_POST[txt_no_bukti3]."'>
				 <input type='text' name='txt_no_bukti4' maxlength='1' size='1' value='".$_POST[txt_no_bukti4]."'>
				 <input type='text' name='txt_no_bukti5' maxlength='1' size='1' value='".$_POST[txt_no_bukti5]."'>
				 <input type='text' name='txt_no_bukti6' maxlength='1' size='1' value='".$_POST[txt_no_bukti6]."'>",
				"<input type='text' name='txt_no_ac' value='".$_POST[txt_no_ac]."'>",
				"<input type='text' name='txt_no_cek_giro' value='".$_POST[txt_no_cek_giro]."'>",
				$data->datePicker('txt_jatuh_tempo',$_POST[txt_jatuh_tempo]),
				"<input type='text' name='txt_value_cek_giro' value='".$_POST[txt_value_cek_giro]."'>",
				$data->cb_dax_currency('txt_currency',$_POST[txt_currency])
				),


				'TEXT1' =>  array('Keterangan','Nomor Perkiraan','Jumlah Debet','Jumlah Kredit','* Dibayarkan Kepada'
								,'Diterima Oleh','Tanggal','Pemohon/Kasir','Diperiksa/Dibukukan','Diketahui Kabag/Kadiv'),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
				'FIELD1' =>  array(
				'<textarea rows=3 cols=20 name=txt_keterangan>'.$_POST[txt_keterangan].'</textarea>',
				"<input type='text' name='txt_no_perkiraan' value='".$_POST[txt_no_perkiraan]."'>",
				"<input type='text' name='txt_value_debet' value='".$_POST[txt_value_debet]."'>",
				"<input type='text' name='txt_value_kredit' value='".$_POST[txt_value_kredit]."'>",
				"<input type='text' name='txt_paid_to' value='".$_POST[txt_paid_to]."'>",
				"<input type='text' name='txt_accept_by' value='".$_POST[txt_accept_by]."'>",
				$data->datePicker('txt_date_accept',$_POST[txt_date_accept]),
				"<input type='text' name='txt_pemohon' value='".$_POST[txt_pemohon]."'>",
				"<input type='text' name='txt_diperiksa' value='".$_POST[txt_diperiksa]."'>",
				"<input type='text' name='txt_kabag' value='".$_POST[txt_kabag]."'>"
				),
		);


	############datagrid

			if($_GET[id_del]){
			$sql_del = "delete from tbl_dax_nota_memo
						where pk_id='".$_GET['id_del']."'";
			#$data->showsql($sql_del);
				if ($data->inpQueryReturnBool($sql_del)){
					echo "<script>alert('".$data->err_report('d01')."');window.location='report_pengeluaran_add.php?add=1';</script>";
				}else{
					echo "<script>alert('".$data->err_report('d02')."');</script>";
				}
			}

			if ($_POST['Add']){

				$sql = "INSERT INTO tbl_dax_nota_memo
					(fk_nota,memo,value,status,datecreated,usercreated)
					VALUES
					('".$id."','".$_POST['txt_memo_2']."','".$_POST['txt_value_2']."','0',now(),'".$_SESSION[pk_id]."')";
					$data->inpQueryReturnBool($sql);
				#print_r($sql);
			}


	$arrNota = $data->get_rows("SELECT * FROM tbl_dax_nota_memo where fk_nota = '$id'
								order by pk_id asc");

#print_r($arrNota);
	foreach($arrNota as $k => $v ){
			$arrNotanew[$k] = $v;
			$arrNotanew[$k]['NOMOR'] = $k+1;
			$arrNotanew[$k]['DELETE'] = "<img src='image/page_delete.png' width='12' height='12'>&nbsp;
										<a href='?add=1&id_del=".$v['pk_id']."'>delete</a>";

	}

	$tmpl->addRows('loopAddMore',$arrNotanew);



	$tmpl->AddVar('page','memo',"Keperluan");
	$tmpl->AddVar('page','txt_memo',"<input type=text size=80 name=txt_memo_2 value=''>");
	$tmpl->AddVar('page','jumlah',"Jumlah");
	$tmpl->AddVar('page','txt_value',"<input type='text' name='txt_value_2' value=''>");
	$tmpl->AddVar('page','add',"<input type='submit' name='Add' value='Add'>");
	$tmpl->AddVar('page','space',"&nbsp;");

	$tot = $data->get_value("SELECT sum(value) FROM tbl_dax_nota_memo where fk_nota = '$id'");
	$jum_total = $data->convert_to_money($tot,'');
	$tmpl->AddVar('page','jum_total',$jum_total);


	$tittle = "BUKTI PENGELUARAN ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
}



################################################
if ($_GET['detail']==1)
{
    $rows = $data->selectQuery("select tbl_dax_nota_pengeluaran.*,
								DATE_FORMAT(tbl_dax_nota_pengeluaran.date_tempo,'%d-%M-%Y')as vdate_tempo,
								DATE_FORMAT(tbl_dax_nota_pengeluaran.date_accepted,'%d-%M-%Y')as vdate_accepted
								from tbl_dax_nota_pengeluaran where pk_id='".$_GET['id']."'");
$currency = $data->get_row("SELECT currency_name,symbol from tbl_dax_currency where pk_id='".$rows[fk_currency]."'");
#print_r($currency);

if($rows[jenis_pengeluaran] == '1'){
	$type_pengeluaran = "KAS KECIL";
}elseif($rows[jenis_pengeluaran] == '2'){
	$type_pengeluaran = "KAS";
}elseif($rows[jenis_pengeluaran] == '3'){
	$type_pengeluaran = "BANK";
}else{
	$type_pengeluaran = "-";
}

	$unit_usaha = explode("-", $rows[unit_usaha]);
	$kode_bukti = explode("-", $rows[kode_bukti]);
	$no_bukti_1 = explode("-", $rows[no_bukti_1]);

	#print_r($rows[status]);
        $dataRows = array (
				'TEXT' =>  array('NO.','Beban Unit Usaha','Type','Kode Bukti','No. Bukti','A/C No/','Cek/Giro No.'
								,'Tanggal Jatuh Tempo','Jumlah Nilai Cek/Giro','Mata Uang'),
				'DOT'  => array (':',':',':',':',':',':',':',':'),
				'FIELD' => array (
				substr('000000'.$rows[pk_id],-7,7),
				$unit_usaha[0].$unit_usaha[1].$unit_usaha[2].$unit_usaha[3],
				$type_pengeluaran,
				$kode_bukti[0].$kode_bukti[1],
				$no_bukti_1[0].$no_bukti_1[1].$no_bukti_1[2].$no_bukti_1[3].$no_bukti_1[4].$no_bukti_1[5],
				$rows[no_ac],
				$rows[no_cekgiro],
				$rows[vdate_tempo],
				$rows[value_cekgiro],
				$currency[currency_name]." (".$currency[symbol].")"
				),

				#$rows[memo],
				#$rows[value]

				'TEXT1' =>  array(/*'',*/'Keterangan','Nomor Perkiraan','Jumlah Debet','Jumlah Kredit','* Dibayarkan Kepada'
								,'Diterima Oleh','Tanggal','Pemohon/Kasir','Diperiksa/Dibukukan','Diketahui Kabag/Kadiv'),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
				'FIELD1' =>  array(
				/*'',*/
				$rows[keterangan],
				$rows[no_perkiraan],
				$rows[value_debet],
				$rows[value_kredit],
				$rows[paid_to],
				$rows[accepted_by],
				$rows[vdate_accepted],
				$rows[pemohon],
				$rows[diperiksa],
				$rows[ka_bag]
				),


				);

	################## datagrid
	$arrNota = $data->get_rows("SELECT * FROM tbl_dax_nota_memo where fk_nota = '".$_GET[id]."'
								order by pk_id asc");

#print_r($arrNota);

	foreach($arrNota as $k => $v ){
			$arrNotanew[$k] = $v;
			$arrNotanew[$k]['NOMOR'] = $k+1;
			/*$arrNotanew[$k]['DELETE'] = "<img src='image/page_delete.png' width='12' height='12'>&nbsp;
										<a href='?edit=1&id=".$_GET[id]."&id_del=".$v['pk_id']."'>delete</a>";*/

	}

	$tmpl->addRows('loopAddMore',$arrNotanew);

	/*$sql = "SELECT * FROM tbl_dax_nota_memo where fk_nota = '".$_GET[id]."'
 			order by pk_id asc";
	$DG= $data->dataGridMemoNotaDetail($sql,'pk_id',$_GET[id],$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$linkEdit,'delete',$link);
	$tmpl->addRows('loopAddMore',$DG);*/


	$tot = $data->get_value("SELECT sum(value) FROM tbl_dax_nota_memo where fk_nota = '".$_GET[id]."'");
	$jum_total = $data->convert_to_money($tot,'');
	$tmpl->AddVar('page','jum_total',$jum_total);
	$tmpl->AddVar('page','symbol',$currency[symbol]);

			$tittle = "BUKTI PENGELUARAN DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}


if ($_POST['btn_save_edit'])
{

	$sql_memo = "UPDATE tbl_dax_nota_memo SET
						status = '1',datecreated=now(),usercreated='".$_SESSION[pk_id]."'
						WHERE fk_nota = '".$_GET['id']."'";
		#$data->showsql($sql_memo);
		#$data->inpQueryReturnBool($sql_memo);
		if (!$data->inpQueryReturnBool($sql_memo)){
			echo "<script>alert('".$data->err_report('s02')."');</script>";
		}


	$txt_unit_usaha = $_POST['txt_unit_usaha1']."-".$_POST['txt_unit_usaha2']."-".$_POST['txt_unit_usaha3']."-".$_POST['txt_unit_usaha4'];
	$txt_kode_bukti = $_POST['txt_kode_bukti1']."-".$_POST['txt_kode_bukti2'];
	$txt_no_bukti = $_POST['txt_no_bukti1']."-".$_POST['txt_no_bukti2']."-".$_POST['txt_no_bukti3']."-".$_POST['txt_no_bukti4']."-".$_POST['txt_no_bukti5']."-".$_POST['txt_no_bukti6'];


	$sql_change = "UPDATE ".$tablename." SET
	unit_usaha='".$txt_unit_usaha."',jenis_pengeluaran='".$_POST['type']."',kode_bukti='".$txt_kode_bukti."',no_bukti_1='".$txt_no_bukti."',
	no_ac='".$_POST['txt_no_ac']."',no_cekgiro='".$_POST['txt_no_cek_giro']."',date_tempo='".$_POST['txt_jatuh_tempo']."',value_cekgiro='".$_POST['txt_value_cek_giro']."',memo='".$_POST['txt_memo']."',
	value='".$_POST['txt_value']."',keterangan='".$_POST['txt_keterangan']."',no_perkiraan='".$_POST['txt_no_perkiraan']."',value_debet='".$_POST['txt_value_debet']."',value_kredit='".$_POST['txt_value_kredit']."',
	paid_to='".$_POST['txt_paid_to']."',accepted_by='".$_POST['txt_accept_by']."',date_accepted='".$_POST['txt_date_accept']."',pemohon='".$_POST['txt_pemohon']."',diperiksa='".$_POST['txt_diperiksa']."',
	ka_bag='".$_POST['txt_kabag']."',fk_currency='".$_POST['txt_currency']."',datecreated=now(),usercreated='".$_SESSION[pk_id]."'
	WHERE pk_id = '".$_GET['id']."'";
   # $data->showsql($sql_change);
	if ($data->inpQueryReturnBool($sql_change)){

		echo "<script>alert('".$data->err_report('s01')."');window.parent.close();</script>";
	}else{
		echo "<script>alert('".$data->err_report('s02')."');</script>";
	}

}

############################# edit

if ($_GET['edit'] == 1)
{   #$data->auth('09030101',$_SESSION['user_id']);
#print_r($_GET);



	$rows = $data->selectQuery("select tbl_dax_nota_pengeluaran.*,
								DATE_FORMAT(tbl_dax_nota_pengeluaran.date_tempo,'%d-%M-%Y')as vdate_tempo,
								DATE_FORMAT(tbl_dax_nota_pengeluaran.date_accepted,'%d-%M-%Y')as vdate_accepted
								from tbl_dax_nota_pengeluaran where pk_id='".$_GET['id']."'");

	$unit_usaha = explode("-", $rows[unit_usaha]);
	$kode_bukti = explode("-", $rows[kode_bukti]);
	$no_bukti_1 = explode("-", $rows[no_bukti_1]);

	if($_POST){
				$txt_unit_usaha1 = $_POST['txt_unit_usaha1'];
				$txt_unit_usaha2 = $_POST['txt_unit_usaha2'];
				$txt_unit_usaha3 = $_POST['txt_unit_usaha3'];
				$txt_unit_usaha4 = $_POST['txt_unit_usaha4'];
				$type = $_POST['type'];
				$txt_kode_bukti1 = $_POST['txt_kode_bukti1'];
				$txt_kode_bukti2 = $_POST['txt_kode_bukti2'];
				$txt_no_bukti1 = $_POST['txt_no_bukti1'];
				$txt_no_bukti2 = $_POST['txt_no_bukti2'];
				$txt_no_bukti3 = $_POST['txt_no_bukti3'];
				$txt_no_bukti4 = $_POST['txt_no_bukti4'];
				$txt_no_bukti5 = $_POST['txt_no_bukti5'];
				$txt_no_bukti6 = $_POST['txt_no_bukti5'];
				$txt_no_ac = $_POST['txt_no_ac'];
				$txt_no_cek_giro = $_POST['txt_no_cek_giro'];
				$txt_jatuh_tempo = $_POST['txt_jatuh_tempo'];
				$txt_value_cek_giro = $_POST['txt_value_cek_giro'];
				$txt_memo = $_POST['txt_memo'];
				$txt_value = $_POST['txt_value'];
				$txt_keterangan = $_POST['txt_keterangan'];
				$txt_no_perkiraan = $_POST['txt_no_perkiraan'];
				$txt_value_debet = $_POST['txt_value_debet'];
				$txt_value_kredit = $_POST['txt_value_kredit'];
				$txt_paid_to = $_POST['txt_paid_to'];
				$txt_accept_by = $_POST['txt_accept_by'];
				$txt_date_accept = $_POST['txt_date_accept'];
				$txt_pemohon = $_POST['txt_pemohon'];
				$txt_diperiksa = $_POST['txt_diperiksa'];
				$txt_kabag = $_POST['txt_kabag'];
				$txt_currency = $_POST['txt_currency'];

	}else{
				$txt_unit_usaha1 = $unit_usaha[0];
				$txt_unit_usaha2 = $unit_usaha[1];
				$txt_unit_usaha3 = $unit_usaha[2];
				$txt_unit_usaha4 = $unit_usaha[3];
				$type = $rows[jenis_pengeluaran];
				$txt_kode_bukti1 = $kode_bukti[0];
				$txt_kode_bukti2 = $kode_bukti[1];
				$txt_no_bukti1 = $no_bukti_1[0];
				$txt_no_bukti2 = $no_bukti_1[1];
				$txt_no_bukti3 = $no_bukti_1[2];
				$txt_no_bukti4 = $no_bukti_1[3];
				$txt_no_bukti5 = $no_bukti_1[4];
				$txt_no_bukti6 = $no_bukti_1[5];
				$txt_no_ac = $rows[no_ac];
				$txt_no_cek_giro = $rows[no_cekgiro];
				$txt_jatuh_tempo = $rows[date_tempo];
				$txt_value_cek_giro = $rows[value_cekgiro];
				$txt_memo = $rows[memo];
				$txt_value = $rows[value];
				$txt_keterangan = $rows[keterangan];
				$txt_no_perkiraan = $rows[no_perkiraan];
				$txt_value_debet = $rows[value_debet];
				$txt_value_kredit = $rows[value_kredit];
				$txt_paid_to = $rows[paid_to];
				$txt_accept_by = $rows[accepted_by];
				$txt_date_accept = $rows[date_accepted];
				$txt_pemohon = $rows[pemohon];
				$txt_diperiksa = $rows[diperiksa];
				$txt_kabag = $rows[ka_bag];
				$txt_currency = $rows[fk_currency];
	}

	$dataRows = array (
				'TEXT' =>  array('NO.','Beban Unit Usaha','Type','Kode Bukti','No. Bukti','A/C No/','Cek/Giro No.'
								,'Tanggal Jatuh Tempo','Jumlah Nilai Cek/Giro','Mata Uang'),
				'DOT'  => array (':',':',':',':',':',':',':',':'),
				'FIELD' => array (
				"<input type='hidden' name='txt_id' value='".$rows[pk_id]."'>".substr('000000'.$rows[pk_id],-7,7),
				"<input type='text' name='txt_unit_usaha1' maxlength='1' size='1' value='".$txt_unit_usaha1."'>
				 <input type='text' name='txt_unit_usaha2' maxlength='1' size='1' value='".$txt_unit_usaha2."'>
				 <input type='text' name='txt_unit_usaha3' maxlength='1' size='1' value='".$txt_unit_usaha3."'>
				 <input type='text' name='txt_unit_usaha4' maxlength='1' size='1' value='".$txt_unit_usaha4."'>",
				$data->radio('type',array('1' => 'KAS KECIL', '2' => 'KAS', '3' => 'BANK'),'v',$type,"onclick='document.formaddType.submit()'"),
				"<input type='text' name='txt_kode_bukti1' maxlength='1' size='1' value='".$txt_kode_bukti1."'>
				 <input type='text' name='txt_kode_bukti2' maxlength='1' size='1' value='".$txt_kode_bukti2."'>",
				"<input type='text' name='txt_no_bukti1' maxlength='1' size='1' value='".$txt_no_bukti1."'>
				 <input type='text' name='txt_no_bukti2' maxlength='1' size='1' value='".$txt_no_bukti2."'>
				 <input type='text' name='txt_no_bukti3' maxlength='1' size='1' value='".$txt_no_bukti3."'>
				 <input type='text' name='txt_no_bukti4' maxlength='1' size='1' value='".$txt_no_bukti4."'>
				 <input type='text' name='txt_no_bukti5' maxlength='1' size='1' value='".$txt_no_bukti5."'>
				 <input type='text' name='txt_no_bukti6' maxlength='1' size='1' value='".$txt_no_bukti6."'>",
				"<input type='text' name='txt_no_ac' value='".$txt_no_ac."'>",
				"<input type='text' name='txt_no_cek_giro' value='".$txt_no_cek_giro."'>",
				$data->datePicker('txt_jatuh_tempo',$txt_jatuh_tempo),
				"<input type='text' name='txt_value_cek_giro' value='".$txt_value_cek_giro."'>",
				$data->cb_dax_currency('txt_currency',$txt_currency)
				),

				'TEXT1' =>  array(/*'',*/'Keterangan','Nomor Perkiraan','Jumlah Debet','Jumlah Kredit','* Dibayarkan Kepada'
								,'Diterima Oleh','Tanggal','Pemohon/Kasir','Diperiksa/Dibukukan','Diketahui Kabag/Kadiv'),
				'DOT'  => array (':',':',':',':',':',':',':',':',':',':'),
				'FIELD1' =>  array(
				/*'',*/
				'<textarea rows=3 cols=20 name=txt_keterangan>'.$txt_keterangan.'</textarea>',
				"<input type='text' name='txt_no_perkiraan' value='".$txt_no_perkiraan."'>",
				"<input type='text' name='txt_value_debet' value='".$txt_value_debet."'>",
				"<input type='text' name='txt_value_kredit' value='".$txt_value_kredit."'>",
				"<input type='text' name='txt_paid_to' value='".$txt_paid_to."'>",
				"<input type='text' name='txt_accept_by' value='".$txt_accept_by."'>",
				$data->datePicker('txt_date_accept',$txt_date_accept),
				"<input type='text' name='txt_pemohon' value='".$txt_pemohon."'>",
				"<input type='text' name='txt_diperiksa' value='".$txt_diperiksa."'>",
				"<input type='text' name='txt_kabag' value='".$txt_kabag."'>"
				),

			);


	############datagrid
	if($_GET['id_del']){

	$sql = "delete from tbl_dax_nota_memo
 			where pk_id='".$_GET['id_del']."'";
  	# $data->showsql($sql);
		   if ($data->inpQueryReturnBool($sql))
			{	echo "<script>alert('".$data->err_report('d01')."');window.location='report_pengeluaran_add.php?edit=1&id=".$_GET['id']."';</script>";	}
			else
			{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}
	}
	#print_r($_POST);
	if ($_POST['Add']){

		$sql = "INSERT INTO tbl_dax_nota_memo
			(fk_nota,memo,value,status,datecreated,usercreated)
	 		VALUES
			('".$_GET['id']."','".$_POST['txt_memo_2']."','".$_POST['txt_value_2']."','0',now(),'".$_SESSION[pk_id]."')";
			$data->inpQueryReturnBool($sql);
		#print_r($sql);
	}


	$arrNota = $data->get_rows("SELECT * FROM tbl_dax_nota_memo where fk_nota = '".$_GET[id]."'
								order by pk_id asc");

#print_r($arrNota);
	foreach($arrNota as $k => $v ){
			$arrNotanew[$k] = $v;
			$arrNotanew[$k]['NOMOR'] = $k+1;
			$arrNotanew[$k]['DELETE'] = "<img src='image/page_delete.png' width='12' height='12'>&nbsp;
										<a href='?edit=1&id=".$_GET[id]."&id_del=".$v['pk_id']."'>delete</a>";

	}

	$tmpl->addRows('loopAddMore',$arrNotanew);




	$tmpl->AddVar('page','memo',"Keperluan");
	$tmpl->AddVar('page','txt_memo',"<input type=text size=80 name=txt_memo_2 value=''>");
	$tmpl->AddVar('page','jumlah',"Jumlah");
	$tmpl->AddVar('page','txt_value',"<input type='text' name='txt_value_2' value=''>");
	$tmpl->AddVar('page','add',"<input type='submit' name='Add' value='Add'>");
	$tmpl->AddVar('page','space',"&nbsp;");

	$tot = $data->get_value("SELECT sum(value) FROM tbl_dax_nota_memo where fk_nota = '".$_GET[id]."'");
	$jum_total = $data->convert_to_money($tot,'');
	$tmpl->AddVar('page','jum_total',$jum_total);
	#$tmpl->AddVar('page','symbol',$currency[symbol]);

    $tittle = "BUKTI PENGELUARAN EDIT";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save_edit value='save'>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.parent.close();\">"
					);
}




$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);

$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('date_from', $date_from));
$tmpl->addVars('row',$dataRows );
$tmpl->addVars('path',$path);
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>