<script type="text/javascript">

/***********************************************
* Scrollable Menu Links- � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* Visit http://www.dynamicDrive.com for hundreds of DHTML scripts
* This notice must stay intact for legal use
***********************************************/

//configure path for left and right arrows
var goleftimage='pointer2.gif'
var gorightimage='pointer.gif'
//configure menu width (in px):
var menuwidth=300
//configure menu height (in px):
var menuheight=25
//Specify scroll buttons directions ("normal" or "reverse"):
var scrolldir="normal"
//configure scroll speed (1-10), where larger is faster
var scrollspeed=6
//specify menu content
var menucontents='<nobr><a href="http://www.dynamicdrive.com">Dynamic Drive</a> | <a href="http://www.javascriptkit.com">JavaScript Kit</a> | <a href="http://www.codingforums.com">CodingForums.com</a> | <a href="http://www.builder.com">Builder.com</a> | <a href="http://freewarejava.com">Freewarejava.com</a></nobr>'


////NO NEED TO EDIT BELOW THIS LINE////////////

var iedom=document.all||document.getElementById
var leftdircode='onMouseover="moveright()" onMouseout="clearTimeout(righttime)"'
var rightdircode='onMouseover="moveleft()" onMouseout="clearTimeout(lefttime)"'
if (scrolldir=="reverse"){
var tempswap=leftdircode
leftdircode=rightdircode
rightdircode=tempswap
}
if (iedom)
document.write('<span id="temp" style="visibility:hidden;position:absolute;top:-100;left:-5000">'+menucontents+'</span>')
var actualwidth=''
var cross_scroll, ns_scroll
var loadedyes=0
function fillup(){
if (iedom){
cross_scroll=document.getElementById? document.getElementById("test2") : document.all.test2
cross_scroll.innerHTML=menucontents
actualwidth=document.all? cross_scroll.offsetWidth : document.getElementById("temp").offsetWidth
}
else if (document.layers){
ns_scroll=document.ns_scrollmenu.document.ns_scrollmenu2
ns_scroll.document.write(menucontents)
ns_scroll.document.close()
actualwidth=ns_scroll.document.width
}
loadedyes=1
}
window.onload=fillup

function moveleft(){
if (loadedyes){
if (iedom&&parseInt(cross_scroll.style.left)>(menuwidth-actualwidth)){
cross_scroll.style.left=parseInt(cross_scroll.style.left)-scrollspeed+"px"
}
else if (document.layers&&ns_scroll.left>(menuwidth-actualwidth))
ns_scroll.left-=scrollspeed
}
lefttime=setTimeout("moveleft()",50)
}

function moveright(){
if (loadedyes){
if (iedom&&parseInt(cross_scroll.style.left)<0)
cross_scroll.style.left=parseInt(cross_scroll.style.left)+scrollspeed+"px"
else if (document.layers&&ns_scroll.left<0)
ns_scroll.left+=scrollspeed
}
righttime=setTimeout("moveright()",50)
}


if (iedom||document.layers){
with (document){
write('<table border="0" cellspacing="0" cellpadding="2">')
write('<td valign="middle"><a href="#" '+leftdircode+'><img src="'+goleftimage+'"border=0></a> </td>')
write('<td width="'+menuwidth+'px" valign="top">')
if (iedom){
write('<div style="position:relative;width:'+menuwidth+'px;height:'+menuheight+'px;overflow:hidden;">')
write('<div id="test2" style="position:absolute;left:0;top:0">')
write('</div></div>')
}
else if (document.layers){
write('<ilayer width='+menuwidth+' height='+menuheight+' name="ns_scrollmenu">')
write('<layer name="ns_scrollmenu2" left=0 top=0></layer></ilayer>')
}
write('</td>')
write('<td valign="middle"> <a href="#" '+rightdircode+'>')
write('<img src="'+gorightimage+'"border=0></a>')
write('</td></table>')
}
}

</script>