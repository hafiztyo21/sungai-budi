<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'employee.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new employee;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('get_employee_check.html');

if ($_POST['add_user']){
	$order_by=$_POST['order_by'];
}

if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='pk_id';
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc';
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);


//print_r ($kode);
$link = 'employee_add.php';
$addLink = "<a href='employee.php' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:600px;dialogHeight:400px')>ADD</a>";
//$addLink = "<a href='user.php' onclick=show_modal('user_add.php?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>".('add')." </a>";

if ($_POST['btn_search'] )
{
	#$id_serach =  $_POST['cb_search'];
	#$q_serach =  $_POST['txt_search'];
	
	if($_POST[txt_name]==''){
		$filter_name = "";
	}else{
		$filter_name = " and tbl_dax_employee.pk_id='".$_POST[txt_name]."'  ";
	}
	if($_POST[txt_department]=='0'){
		$filter_department = "";
	}else{
		$filter_department = " and  fk_department='".$_POST[txt_department]."'  ";
	}
	if($_POST[txt_location]=='0'){
		$filter_location = "";
	}else{
		$filter_location = " and  fk_location='".$_POST[txt_location]."'  ";
	}

	if($_POST[txt_job]=='-1'){
		$filter_job = "";
	}else{
		$filter_job = " and fk_job='".$_POST[txt_job]."'  ";
	}
	

	$sql = "SELECT tbl_dax_location.name as location ,tbl_dax_employee.pk_id, tbl_dax_employee.full_name, tbl_dax_employee.nickname,  tbl_dax_job.name as JOB_NAME,tbl_dax_department.name as department
			, tbl_dax_employee.start_date, tbl_dax_employee.status, DATE_FORMAT(tbl_dax_employee.start_date,'%d-%m-%Y') as vstart_date
			FROM tbl_dax_employee
			LEFT JOIN tbl_dax_job ON tbl_dax_employee.fk_job = tbl_dax_job.pk_id
			LEFT JOIN tbl_dax_department ON tbl_dax_employee.fk_department = tbl_dax_department.pk_id
			LEFT JOIN tbl_dax_location ON tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			WHERE tbl_dax_employee.status in ('0','1','2')
			$filter_department $filter_location $filter_name $filter_job
			 order by $order_by $sort_order";

	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else
{
	$_SESSION['sql']='';
	$sql = "SELECT tbl_dax_location.name as location ,tbl_dax_employee.pk_id, tbl_dax_employee.full_name, tbl_dax_employee.nickname,  tbl_dax_job.name as JOB_NAME,tbl_dax_department.name as department
			, tbl_dax_employee.start_date, DATE_FORMAT(tbl_dax_employee.start_date,'%d-%m-%Y') as vstart_date
			FROM tbl_dax_employee
			LEFT JOIN tbl_dax_job ON tbl_dax_employee.fk_job = tbl_dax_job.pk_id
			LEFT JOIN tbl_dax_department ON tbl_dax_employee.fk_department = tbl_dax_department.pk_id
LEFT JOIN tbl_dax_location ON tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			order by $order_by $sort_order";
#print_r($sql);
#$data->showsql($sql);
}


$arrFields = array(
		'tbl_dax_employee.full_name '=>'FIRST_NAME',
		'tbl_dax_employee.nickname_name '=>'NICKNAME',
		'tbl_dax_job.name'=>'JOB_NAME'
		
);

#print_r($sql);
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridGetEmployee($sql,'pk_id','first_name',$data->ResultsPerPage,$pg,'view',$link,'menu',$link,'edit',$link,'delete',$link);
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addVar('page','txt_name',$data->cb_employee_all_number('txt_name',$_POST[txt_name]));
$tmpl->addVar('page','cb_location',$data->cb_location_search('txt_location',$_POST[txt_location]));
$tmpl->addVar('page','cb_department',$data->cb_department_search('txt_department',$_POST[txt_department]));
$tmpl->addVar('page', 'cb_job',$data->cb_dax_job('txt_job',$_POST[txt_job]));


$tmpl->addRows('loopData',$DG);

$tmpl->addVar('page','submit',"<input type=\"button\" name=\"btnsubmit\" value='Add' onClick=\"fgetresult()\">");
$tmpl->addVar('page','cancel',"<input type=\"button\" name=\"btnclear\" value='None' onClick=\"fGetNone()\">");

$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>