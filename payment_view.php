<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'payment_template.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';


$data = new payment_template;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('payment_view.html');


if($_GET['del']==1){

	$sql = "UPDATE tbl_dax_payment_template SET status = 0 where pk_id ='".$_GET['id']."'";
	#$sql = "DELETE FROM tbl_dax_payment_template where pk_id='".$_GET['id']."'";
    #$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('delete successfull');windows.close();</script>";	}
	else
	{	echo "<script>alert('delete failed');</script>";	}
}
####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='name'; #default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='asc'; #default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
$linkDetail = 'payment_template_detail.php';
$linkEdit = 'payment_template_detail.php';

if($data->auth_boolean(151210,$_SESSION['pk_id'])){ 
$link = 'payment_template_all.php';
$addLink = "<a href='payment_view.php' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:700px;dialogHeight:500px')>ADD</a>";
#$linkDetail = "<a href='payment_view.php' onclick=show_modal('payment_view.php?detail=1','status:no;help:no;dialogWidth:800px;dialogHeight:400px')>VIEW</a>";
$tmpl->addVar('page','add',$addLink);

}

if ($_POST['btn_search'] )
{
	$id_serach =  $_POST['cb_search'];
	$q_serach =  $_POST['txt_search'];

	$sql = "SELECT tbl_dax_payment_template.pk_id, tbl_dax_payment_template.date_interval as date_interval, tbl_dax_payment_template.code_method, tbl_dax_payment_template.name AS name, tbl_dax_payment_template.down_payment_percent AS down_payment_percent, tbl_dax_payment_type.name AS payment_type
FROM tbl_dax_payment_template
LEFT JOIN tbl_dax_payment_method ON tbl_dax_payment_method.pk_id = tbl_dax_payment_template.code_method 
LEFT JOIN tbl_dax_payment_type ON tbl_dax_payment_type.pk_id = tbl_dax_payment_template.type_payment
WHERE tbl_dax_payment_template.status = 1 
AND upper($id_serach) like upper('%$q_serach%')" ;

	$_SESSION['sql']=$sql;
}
else if (($_SESSION['sql']) and ($_GET))
{
    $sql = $_SESSION['sql'];
}
else

{
	$_SESSION['sql']='';
	$sql = "SELECT tbl_dax_payment_template.pk_id, tbl_dax_payment_template.date_interval as date_interval, tbl_dax_payment_template.code_method, tbl_dax_payment_template.name AS name, tbl_dax_payment_template.down_payment_percent AS down_payment_percent, tbl_dax_payment_type.name AS payment_type
FROM tbl_dax_payment_template
LEFT JOIN tbl_dax_payment_method ON tbl_dax_payment_method.pk_id = tbl_dax_payment_template.code_method 
LEFT JOIN tbl_dax_payment_type ON tbl_dax_payment_type.pk_id = tbl_dax_payment_template.type_payment
WHERE tbl_dax_payment_template.status = 1 order by $order_by $sort_order";
	
}
$arrFields = array(

		'tbl_dax_payment_template.name'=>'NAME',
		'tbl_dax_payment_template.down_payment_percent'=>'DOWN_PAYMENT_PERCENT',
		
		
		
);

#$data->showsql($sql);


$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridPayment($sql,'pk_id','user_name',$data->ResultsPerPage,$pg,'view',$linkDetail,'assign',$link,'edit',$linkEdit,'delete',$link);
#$money = convert_to_money( $s, $currency = '$', $dec=2 );
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         #$paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		 $paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);



$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>