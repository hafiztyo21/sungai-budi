<?php
session_start();
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'report.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new report;

$nid = $_GET['nid'];
$year1 = $_GET['y1'];
$year2 = $_GET['y2'];
#-> DATA EMPLOYEE
$sql_employee = mysql_query("SELECT `tbl_dax_employee`.`full_name`,`tbl_dax_employee`.`start_date`,`tbl_dax_department`.`name` AS departemen,
`tbl_dax_job`.`name` AS jabatan
FROM `tbl_dax_employee` 
LEFT JOIN `tbl_dax_department` ON `tbl_dax_employee`.`fk_department`=`tbl_dax_department`.`pk_id`
LEFT JOIN `tbl_dax_job` ON `tbl_dax_employee`.`fk_job`=`tbl_dax_job`.`pk_id`
WHERE `tbl_dax_employee`.`pk_id`='$nid'") or die (mysql_error());
$row_employee = mysql_fetch_array($sql_employee);

#-> DATA DETAIL CB
$sql_cb = mysql_query("SELECT 
`date_from1`,`date_to1`,`qty`,`keperluan`
FROM `tbl_dax_leave_plan` WHERE `fk_employee`='$nid' AND `status_code`='CB' and qty !='0'
AND `date_from1` BETWEEN '$year1' AND '$year2'");
$count = mysql_num_rows($sql_cb);


$sql_sum = mysql_query("SELECT sum(qty) as total
FROM `tbl_dax_leave_plan` WHERE `fk_employee`='$nid' AND `status_code`='CB' and qty !='0'
AND `date_from1` BETWEEN '$year1' AND '$year2'");
$row_sum = mysql_fetch_array($sql_sum);

$quota = "17";
?>
<body>
<link rel="stylesheet" type="text/css" href="include/css/normal.css" />
<script language="javascript" type="text/javascript">
<!--
function popitup(url) {
	newwindow=window.open(url,'Report Cuti Besar All','screen.availWidth, screen.availHeight');
	if (window.focus) {newwindow.focus()}
	return false;
}

// -->
</script>

<form action='' name='formaddType' method='post'>

<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
<tr>
    <td height="10" colspan="6" align="center" ></td>
  </tr>
  <tr>
		  	<td height="50" colspan="10" align="center" background="image/bg_menu.gif">
			<patTemplate:tmpl name="tittles">
			  <h1 class="style1">DETAIL CUTI BESAR</h1></patTemplate:tmpl>
    </tr>
  <tr>
		<td height="5px" colspan="6" align="center" ></td>
  </tr>
</table>
  <div align="center" class="border">
<table  width="90%" border="0">
<tr>
<td align="right">&nbsp;</td>
</tr>
<!--tr>
<td>Yang bertanda tangan di bawah ini:</td>
</tr-->
</table>
	<table border=0 width="90%">
      	<tr>
          		<td  height="20" width="213" bgcolor="#CCCCCC">Nama :</td>
          	 	<td  height="20" width="652" bgcolor="#EBEBEB" class="mandatory"><?php echo $row_employee['full_name'];?></td>
      </tr>
	  <tr>
			  <td  bgcolor="#CCCCCC" width="213">Tgl Masuk :</td>
			  <td  bgcolor="#EBEBEB" width="652" class="mandatory"><?php echo $row_employee['start_date'];?></td>
	  </tr>
	  <tr>
				<td  bgcolor="#CCCCCC" width="213">Jabatan/Pekerjaan :</td>
	            <td  bgcolor="#EBEBEB" width="652" class="mandatory"><?php echo $row_employee['jabatan'];?></td>
	  </tr>
	  <tr>
				<td  bgcolor="#CCCCCC" width="213">Divisi/Bagian :</td>
	            <td  bgcolor="#EBEBEB" width="652" class="mandatory"><?php echo $row_employee['departemen'];?></td>	
	  </tr>

    </table>
<br>

<table width="90%" border="0">
  <tr>
    <td width="5%" height="38" align="center" bgcolor="#ccc">No</td>
    <td width="23%" align="center" bgcolor="#ccc">DATE FROM</td>
    <td width="26%" align="center" bgcolor="#ccc">DATE TO</td>
    <td width="9%" align="center" bgcolor="#ccc">QTY</td>
    <td width="37%" align="center" bgcolor="#ccc">MEMO</td>
  </tr>
<?php 
if ($count == 0)
	{
		echo '<tr><td>Data Kosong.</td></tr>';	
	}
else
{
$n=0;
while($row_cb = mysql_fetch_array($sql_cb)){
	$n++;
	
?>  
  <tr>
    <td align="center"><?php echo $n;?></td>
    <td align="center"><?php echo $row_cb['date_from1'];?></td>
    <td align="center"><?php echo $row_cb['date_to1'];?></td>
    <td align="center"><?php echo $row_cb['qty'];?></td>
    <td align="center"><?php echo $row_cb['keperluan'];?></td>
  </tr>
<?php }};?>  
</table>



	<table width="90%">	
		</table>
	<br>
<table width="90%">
<tr style="background-image:url(image/bg_menu1.gif)">
  <td align="center" bgcolor="#CCCCCC"><b>Quota Cuti 17 Hari</b></td>
</tr></table>
<br>

<TABLE width="90%">
<!--TR>
  <TD width="17%">Sampai Periode Tanggal : {TXT_END_PERIOD}<br>
  HAK CUTI ATAS BULAN YANG TELAH DIJALANI TIMBUL SETIAP TAHUN PADA PERULANGAN TANGGAL MASUK KERJA</TD>  
   
</TR>
<tr>
<TD width="83%">&nbsp; </TD> 
</tr-->
<tr>
 <TD width="54%">Jatah Cuti</TD> 
 <TD width="8%" align="center">=</TD> 
 <TD width="38%"><?php echo $quota;?> hari</TD> 
</tr>
<tr>
 <TD width="54%">Terpakai</TD> 
 <TD width="8%" align="center">=</TD> 
 <TD width="38%"><?php echo $row_sum['total'];?> Hari</TD> 
</tr>
<tr>
 <TD width="54%" height="36" bgcolor="#ccc"><strong>SISA CUTI BESAR</strong></TD> 
 <TD width="8%" align="center" bgcolor="#ccc"><strong>=</strong></TD> 
 <TD width="38%" bgcolor="#ccc"><strong><?php echo $quota-$row_sum['total'];?> Hari</strong></TD> 
</tr>


</TABLE>

<br>


  </div>
</form>

</body>

