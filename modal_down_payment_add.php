<?php
session_start();
#session_destroy();
//print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new globalFunction;
$tmpl = new patTemplate();

$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('modal_down_payment_add.html');
$tablename = 'tbl_dax_inventory';
$tablename1 = 'tbl_dax_customer';



if ($_POST['btn_save']=="save")
{

				$sql2="insert into tbl_dax_down_payment(pk_id,fk_deal_info,down_payment_date,down_payment_type,ccv_number,cc_expired,cc_name,cc_bank,value,bank_charge,receipt_no)values('',
								'".$_GET['id']."',
								'".$_POST['txt_down_payment_date']."',
								'".$_POST['txt_payment_type']."',
								'".$_POST['txt_ccv_number']."',
								'".$_POST['txt_cc_expired']."',
								'".$_POST['txt_cc_name']."',
								'".$_POST['txt_cc_bank']."',
								'".$_POST['txt_value']."',
								'".$_POST['txt_bank_charge']."',
								'".$_POST['txt_receipt_no']."'
								)";
								#$data->showsql($sql2);
							
	if ($data->inpQueryReturnBool($sql2))
	{	echo "<script>alert('".$data->err_report('s01')."');window.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('s02')."');</script>";	}


}

if ($_POST['btn_update_inventory'])
{

$sql= "UPDATE ".$tablename." SET code = '".$_POST['txt_code']."',
			name = '".$_POST['txt_name']."',description = '".$_POST['txt_description']."', 
			price = '".$_POST['txt_price']."', member_period = '".$_POST['txt_member_period']."', 
			member_payment = '".$_POST['txt_member_payment']."' where pk_id='".$_GET['id']."'";
   	#$data->showsql($sql);
	if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('s01')."');window.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('s02')."');</script>";	}


}

if ($_GET['add']==1)
{   

	    $dataRows = array (
				'TEXT' =>  array('Date Payment','Payment Type','Receipt No','CCV Number','CC Expired', 'CC Name','CC Bank','Value','Bank Charge'),
				'FIELD' => array (
					$data->datePicker('txt_down_payment_date', $_POST['txt_down_payment_date']),
					$data->cb_payment_type_item('txt_payment_type',$_POST[txt_payment_type],'document.formaddDP.submit();'),
					"<input type='text' name='txt_receipt_no' id='txt_receipt_no' value='".$_POST['txt_receipt_no']."'>",
					"<input type='text' name='txt_ccv_number' id='txt_ccv_number' value='".$_POST['txt_ccv_number']."'>",
					"<input type='text' name='txt_cc_expired' id='txt_cc_expired' value='".$_POST['txt_cc_expired']."'>",
					"<input type='text' name='txt_cc_name' id='txt_cc_name' value='".$_POST['txt_cc_name']."'>",
					"<input type='text' name='txt_cc_bank' id='txt_cc_bank' value='".$_POST['txt_cc_bank']."'>",
					"<input type='text' name='txt_value' id='txt_value' value='".$_POST['txt_value']."'>",
					"<input type='text' name='txt_bank_charge' id='txt_bank_charge' value='".$_POST['txt_bank_charge']."'>"
					)
				);
	$tittle = "DOWN PAYMENT ADD";

    $button = array ('SUBMIT' => "<input type=submit name=btn_save value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.close();\">"
					);
}

if ($_GET['edit']==1)
{   
$rows = $data->selectQuery("select * from ".$tablename." where pk_id='".$_GET['id']."'");
$server_date = date("F j, Y H:i:s");
$server_time = date("H:i:s");
#$data->auth('09030101',$_SESSION['user_id']);
	$dataRows = array (
				'TEXT' =>  array('Code','Name','Description','Price','Membership Period', 'Membership Price'),
				'DOT'  => array (':',':',':',':'),
				'FIELD' => array ("<input type=text name=txt_code value='".$rows[code]."'>",
				"<input type=text name=txt_name value='".$rows[name]."'>",
				"<input type=text name=txt_description value='".$rows[description]."'>",
				"<input type=text name=txt_price value='".$rows[price]."'>",
				"<input type=text name=txt_member_period value='".$rows[member_period]."'>",
				"<input type=text name=txt_member_payment value='".$rows[member_payment]."'>"
				
					)
					
					);

    $tittle = "PACKAGE INVENTORY EDIT";

    $button = array ('SUBMIT' => "<input type=submit name=btn_update_inventory value=save>",
					 'RESET'  => "<input type=reset name=reset value=reset>
					 			  <input type=button name=cancel value=cancel onclick=\"window.close();\">"
					);
}



if ($_GET['detail']==1)
{
    $rows = $data->selectQuery("select * from tbl_dax_inventory where pk_id ='".$_GET['id']."'");
	$grand = $data->get_value("select price from tbl_dax_inventory where pk_id ='".$_GET['id']."'");


         $dataRows = array (
				'TEXT' =>  array('Code','Name','Description','Price','Membership Period','Membership Price'),
				'DOT'  => array (':',':',':',':'),
				'FIELD' => array ($rows[code], 
								$rows[name],
								$rows[description], 
								$data->convert_to_money($grand,' ') ,
								$rows[member_period], 
								$data->convert_to_money($rows[member_payment],' '), 
					)
				
				
				);
				
				
				
				
	#$data->showsql($sql);
			#print_r($dataRows);
			$tittle = "PACKAGE INVENTORY DETAIL";

    $button = array ('SUBMIT' => "",
					 'RESET'  => ""
					);
}


$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);


#$tmpl->addVar('date', 'DATE_FROM',$data->datePicker('txt_payment_type', $txt_down_payment_date));
$tmpl->addVars('path',$path);
$tmpl->addVars('row',$dataRows );
$tmpl->addVar('tittles','tittle',$tittle );
$tmpl->addVars('button',$button);
$tmpl->displayParsedTemplate('page');
?>