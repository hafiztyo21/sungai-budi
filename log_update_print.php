<?php
session_start();
#session_destroy();
#print_r($_SESSION);
#print_r($_GET);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';

require_once $GLOBALS['CLASS'].'absence.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new absence;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('log_update_print.html');


if($_GET['del']==1){

	$sql = "delete from tbl_dax_update_log where pk_id='".$_GET['id']."'";
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');window.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}
}

####################################sorting##############################
if ($_GET['order_by']){
	$order_by=$_GET['order_by'];
}else{
	$order_by='day_date';//default
}
if ($_GET['sort_order']){
	$sort_order=$_GET['sort_order'];
}else{
	$sort_order='desc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################
if($data->auth_boolean(101410,$_SESSION['pk_id'])){ 
$link = 'cuti_add.php';
$linkView = 'cuti_view.php';

	if($_SESSION['level_id'] == "1"){
		$addLink = "<a href='cuti.php?page=".$_GET[page]."' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:800px')>ADD</a>";
		$filter = "";
	}else{
		$addLink = "<a href='cuti.php?page=".$_GET[page]."' onclick=show_modal('".$link."?add=1','status:no;help:no;dialogWidth:800px;dialogHeight:800px')>ADD</a>";
		$filter = "and tbl_dax_update_log.fk_employee='".$_SESSION[fk_employee]."'";
	}

$tmpl->addVar('page','add',$addLink);
}


	if ($_SESSION['pajak'] =='P'){
		$filter_pajak = " and tbl_dax_employee.tax_status='".$_SESSION['pajak']."'";
	}



	if($_GET[id_status]=='0'){
		$filter_status = "";
	}else{
		$filter_status = " and tbl_dax_update_log.status = '".$_GET[id_status]."' ";
	}
	if($_GET[id_bstatus]=='0'){
		$filter_bstatus = "";
	}else{
		$filter_bstatus = " and tbl_dax_update_log.before_status = '".$_GET[id_bstatus]."' ";
	}
		
	if($_GET[id_department]=='0'){
		$filter_department = "";
	}else{
		$filter_department = " and tbl_dax_department.pk_id = '".$_GET[id_department]."'  ";
	}
	if($_GET[id_location]=='0'){
		$filter_location = "";
	}else{
		$filter_location = " and tbl_dax_location.pk_id = '".$_GET[id_location]."'  ";
	}
		if($_GET[txt_name]==''){
			$filter_name = "";
		}else{
			$filter_name = " and tbl_dax_employee.pk_id='".$_GET[txt_name]."'  ";
		}
		

	#$id_serach =  $_GET['cb_search'];
	#$q_serach =  $_GET['txt_search'];

	$sql = "SELECT tbl_dax_update_log.*,DATE_FORMAT(tbl_dax_update_log.day_date,'%d-%M-%Y') as vday_date,
			DATE_FORMAT(tbl_dax_update_log.day_date,'%a') as vday_name,
			tbl_dax_employee.pk_id as pk_employee,tbl_dax_employee.full_name as name,
			tbl_dax_status.code,
			tbl_dax_department.name as department,tbl_dax_location.name as location,
			tbl_dax_status.name as descreption , tbl_dax_user.user_name  as operator,DATE_FORMAT(tbl_dax_update_log.date_changed,'%d-%M-%Y %H:%i:%s') as vdate_changed
			FROM tbl_dax_update_log 
			LEFT JOIN tbl_dax_employee ON tbl_dax_employee.pk_id = tbl_dax_update_log.fk_employee
			LEFT JOIN tbl_dax_status ON tbl_dax_status.code = tbl_dax_update_log.status
			LEFT JOIN tbl_dax_job ON tbl_dax_employee.fk_job = tbl_dax_job.pk_id
			LEFT JOIN tbl_dax_department ON tbl_dax_employee.fk_department = tbl_dax_department.pk_id
			LEFT JOIN tbl_dax_location ON tbl_dax_employee.fk_location = tbl_dax_location.pk_id
			LEFT JOIN tbl_dax_user ON tbl_dax_update_log.changed_by = tbl_dax_user.pk_id
			where
			day_date between '".$_GET[txt_from]."' and '".$_GET[txt_to]."'
			$filter_status
			$filter_bstatus
			$filter_department
			$filter_location
			
			$filter_name
			$filter $filter_pajak
			order by $order_by,tbl_dax_employee.full_name $sort_order";

	
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_GET['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridApprovalPermit($sql,'pk_id',10000000000,$pg,'view',$linkView,'tambah',$link,'edit',$link,'delete',$link);
  #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?page=" . $InfoArray["PREV_PAGE"] . "'><img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
        # $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
		$paging_no .= "<font style=\"BACKGROUND-COLOR: #3238A3\" color=\"white\"><b>&nbsp;".$InfoArray["PAGE_NUMBERS"][$i] . "&nbsp;<b></font> | ";
      } else {
         $paging_no .= "<a href='?page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }


###############################################################################################
$path = array
 		(
      'PATHCALENDARCSS' => $GLOBALS['CALENDAR'].'calendar.css',
      'PATHCALENDARJS' => $GLOBALS['CALENDAR'].'mootools.js',
      'PATHMOOTOOLSJS'  => $GLOBALS['CALENDAR'].'DatePicker.js',
      'PATHDATEPICKERJS' => $GLOBALS['CALENDAR'].'calendar.js',
	  'PATHPRINTCSS' => $GLOBALS['CSS'].'stylePrint.css'
      	);
$tmpl->addVars('path',$path);


$arrFields = array('0' => ' - All - ','A'=>'Alpha (A)','H'=>'Hadir (H)','HE'=>'Hadir hasil edit (HE)',
			'CB'=>'Cuti Besar (CB)','CT'=>'Cuti Tahunan (CT)','CB'=>'Cuti Dasar (CB)',
			'CH'=>'Cuti Haid (CH)','T'=>'Tugas (T)',
			'TLK'=>'Tugas Luar Kota (TLK)','IAC' =>'Ijin Akumulasi Cuti (IAC)',
			'IAS' =>'Ijin Akumulasi Potong 50% (IAC)','IAF' =>'Ijin Akumulasi Potong 100% (IAF)',
				'HT1' =>'Hadir Terlambat < 30 menit (HT1)',
				'HT2' =>'Hadir Terlambat > 30 menit (HT2)',
				'HT3' =>'Hadir, Pulang Cepat (HT3)',
				'S' =>'Sakit (S)',
				'TLK' =>'Tugas Luar Kota (TLK)'
			);

#$tmpl->addVar('page','txt_name',"<input type='text' name='txt_name' value='".$_GET[txt_name]."'>");
$tmpl->addVar('page','txt_name',$data->cb_employee_all_number('txt_name',$_GET[txt_name]));
$tmpl->addVar('page','cb_location',$data->cb_location_search('txt_location',$_GET[txt_location]));
$tmpl->addVar('page','cb_department',$data->cb_department_search('txt_department',$_GET[txt_department]));
$tmpl->addVar('page','cb_before_status',$data->cb_status_all('txt_bstatus',$_GET[txt_bstatus]));
$tmpl->addVar('page','cb_status',$data->cb_status_all('txt_status',$_GET[txt_status]));
$tmpl->addVar('page','from',$data->datePicker('txt_from',$_GET[txt_from]));
$tmpl->addVar('page','to',$data->datePicker('txt_to',$_GET[txt_to]));

$tmpl->addRows('loopData',$DG);
/*$tmpl->addVar('page','add',$addLink);
$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);


//$tmpl->addVar('page','cek',$cekLink);*/
$tmpl->displayParsedTemplate('page');
?>
