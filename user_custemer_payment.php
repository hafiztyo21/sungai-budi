<?php
session_start();
#session_destroy();
#print_r($_SESSION);
require_once 'global.inc.php';
require_once $GLOBALS['CLASS'].'global.class.php';
require_once $GLOBALS['CLASS'].'customer.class.php';
require_once $GLOBALS['CLASS'].'xajax.inc.php';
require_once $GLOBALS['TMPL'].'patError/patErrorManager.php';
require_once $GLOBALS['TMPL'].'patTemplate/patTemplate.php';

$data = new customer;
$tmpl = new patTemplate();
$tmpl->setRoot('templates');
$tmpl->readTemplatesFromInput('user_custemer_payment.html');

/*
if($_GET['del']==1){

	$sql = "DELETE FROM tbl_dax_service_checkup where fk_customer='".$_GET['id']."'";
    //$data->showsql($sql);
   if ($data->inpQueryReturnBool($sql))
	{	echo "<script>alert('".$data->err_report('d01')."');window.parent.close();</script>";	}
	else
	{	echo "<script>alert('".$data->err_report('d02')."');</script>";	}
}*/
#####################################sorting##############################
if ($_POST['order_by']){
	$order_by=$_POST['order_by'];
}else{
	$order_by='pk_id';//default
}
if ($_POST['sort_order']){
	$sort_order=$_POST['sort_order'];
}else{
	$sort_order='desc';//default
}
$tmpl->addVar('page', 'order_by',$order_by);
$tmpl->addVar('page', 'sort_order',$sort_order);

###########################end of sorting##################################

$id_deal = $data->get_value("SELECT fk_customer FROM tbl_dax_deal_info where fk_customer='".$_GET[id]."'");

			
	$sql = "SELECT * FROM(
			SELECT 'DP'as mode,down_payment_date as dating, tbl_dax_payment_type.name as payment_type, bank_charge,value FROM tbl_dax_down_payment,tbl_dax_payment_type,tbl_dax_deal_info where tbl_dax_down_payment.down_payment_type=tbl_dax_payment_type.pk_id
			 and tbl_dax_down_payment.fk_deal_info=tbl_dax_deal_info.pk_id
			and tbl_dax_deal_info.fk_customer='".$id_deal."'
			
			
			UNION
			
			SELECT 'INSTALLMENT'as mode,date_cash as dating,tbl_dax_payment_type.name as payment_type,bank_charge,total_payment FROM tbl_dax_payment
			,tbl_dax_payment_type,tbl_dax_deal_info where tbl_dax_payment.type_payment=tbl_dax_payment_type.pk_id and tbl_dax_payment.fk_deal_info=tbl_dax_deal_info.pk_id
			and tbl_dax_deal_info.fk_customer='".$id_deal."'
			
			UNION
			
			SELECT 'MEMBERSHIP'as mode,date_cash as dating,tbl_dax_payment_type.name as payment_type,bank_charge,total_payment FROM tbl_dax_membership_payment
			,tbl_dax_payment_type,tbl_dax_deal_info where tbl_dax_membership_payment.type_payment=tbl_dax_payment_type.pk_id
			 and tbl_dax_membership_payment.fk_deal_info=tbl_dax_deal_info.pk_id
			and tbl_dax_deal_info.fk_customer='".$id_deal."'
			
			UNION
			
			SELECT 'REFUND'as mode,date_refund as dating,'' as payment_type,cancellation_fee,value_refund FROM tbl_dax_refund,tbl_dax_deal_info
			where 
			tbl_dax_refund.fk_deal_info=tbl_dax_deal_info.pk_id
			and tbl_dax_deal_info.fk_customer='".$id_deal."')as tbl
			order by tbl.dating desc";		
			

#print_r($sql);
$searchCB = $data->searchDG($arrFields,'');
$pg = ($_POST['btn_search'] )? 1 : $_GET['page'];
$DG= $data->dataGridCustomerPayment('',$sql,'fk_customer','card_id',$data->ResultsPerPage,$pg,'view','customer_service_add.php','menu',$link,'edit',$link,'delete',$link);
 #print_r ($DG);
#$data->listData();

#################################################  legend paging ######################################
$InfoArray = $data->InfoArray();

   $page_info= "Displaying page " . $InfoArray["CURRENT_PAGE"] . " of " . $InfoArray["TOTAL_PAGES"] . "<BR>";
   $result_info =  "Displaying results " . $InfoArray["START_OFFSET"] . " - " . $InfoArray["END_OFFSET"] . " of " . $InfoArray["TOTAL_RESULTS"] . "<BR>";

   /* Print our first link */
   if($InfoArray["CURRENT_PAGE"]!= 1) {
      $paging_no = "<a href='?id=".$_GET[id]."&page=1'><img src='image/ar_left.png' border='0' /></a> ";
   } else {
      $paging_no = "<img src='image/ar_left.png' border='0' /> ";
   }

   /* Print out our prev link */
   if($InfoArray["PREV_PAGE"]) {
      $paging_no .= "<a href='?id=".$_GET[id]."&page=1" . $InfoArray["PREV_PAGE"] . "'<img src='image/ar_prev.png' border='0' /></a> | ";
   } else {
      $paging_no .= "<img src='image/ar_prev.png' border='0'/> | ";
   }

   /* Example of how to print our number links! */
   for($i=0; $i<count($InfoArray["PAGE_NUMBERS"]); $i++) {
      if($InfoArray["CURRENT_PAGE"] == $InfoArray["PAGE_NUMBERS"][$i]) {
         $paging_no .= $InfoArray["PAGE_NUMBERS"][$i] . " | ";
      } else {
         $paging_no .= "<a href='?id=".$_GET[id]."&page=" . $InfoArray["PAGE_NUMBERS"][$i] . "'>" . $InfoArray["PAGE_NUMBERS"][$i] . "</a> | ";
      }
   }

   /* Print out our next link */
   if($InfoArray["NEXT_PAGE"]) {
      $paging_no .= " <a href='?id=".$_GET[id]."&page=" . $InfoArray["NEXT_PAGE"] . "'><img src='image/ar_next.png'  border='0' /></a>";
   } else {
      $paging_no .= "<img src='image/ar_next.png'  border='0' />";
   }

   /* Print our last link */
   if($InfoArray["CURRENT_PAGE"]!= $InfoArray["TOTAL_PAGES"]) {
      $paging_no .= " <a href='?id=".$_GET[id]."&page=" . $InfoArray["TOTAL_PAGES"] . "'><img src='image/ar_right.png'  border='0' /></a>";
   } else {
      $paging_no .= " <img src='image/ar_right.png'  border='0' /> ";
   }

###############################################################################################

$tmpl->addRows('loopData',$DG);

$tmpl->addVar('page','add',$addLink);

$tmpl->addVar('legend', 'page',$page_info);
$tmpl->addVar('legend', 'result',$result_info);
$tmpl->addVar('paging', 'paging_no',$paging_no);
$tmpl->addVar('page', 'search',$searchCB);

//$tmpl->addVar('page','cek',$cekLink);
$tmpl->displayParsedTemplate('page');
?>